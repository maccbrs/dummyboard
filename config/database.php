
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysqlive'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => database_path('database.sqlite'),
            'prefix'   => '',
        ],

        // 'mysql' => [
        //     'driver'    => 'mysql',
        //     'host'      => env('DB_HOST', 'localhost'),
        //     'database'  => env('DB_DATABASE', 'forge'),
        //     'username'  => env('DB_USERNAME', 'forge'),
        //     'password'  => env('DB_PASSWORD', ''),
        //     'charset'   => 'utf8',
        //     'collation' => 'utf8_unicode_ci',
        //     'prefix'    => '',
        //     'strict'    => false,
        // ],
        'vicidial' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.132',
            'database'  => 'asterisk',
            'username'  => 'cron',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
         'vicidial2' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.132',
            'database'  => 'asterisk',
            'username'  => 'cron',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],     

         'vicidial17' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.17',
            'database'  => 'asterisk',
            'username'  => 'cron',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],   

        'mysql3' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.28',
            'database'  => 'laravel',
            'username'  => 'crusader',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ], 
        'mysqlive' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.28',
            'database'  => 'laravel',
            'username'  => 'crusader',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],  
        'mysql2' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'dummyb',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ], 
        '192.168.200.77' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.77',
            'port' => '3306',
            'database'  => 'asterisk',
            'username' => 'cron',
            'password' => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ], 
	/*	    'mysql' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'laravel_interface',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false, */
		
        'mysql' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.28',
            'database'  => 'laravel',
            'username'  => 'crusader',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false, 
			
        ],                               
        'pgsql' => [
            'driver'   => 'pgsql',
            'host'     => env('DB_HOST', 'localhost'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ],

        'sqlsrv' => [
            'driver'   => 'sqlsrv',
            'host'     => env('DB_HOST', 'localhost'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],

        'gardenia' => [ 
            'driver' => 'mysql',
            'port' => '3306',
            'host' => '54.67.2.80',
            'database' => 'gardenia',
            'username' => 'marlon.bernal',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
		'rhyolite' => [
            'driver' => 'mysql',
            'host' => '192.168.200.123',
            'port' => '3306',
            'database' => 'rhyolite',
            'username' => 'marlon',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],


		'jasper' => [
            'driver' => 'mysql',
            'host' => '192.168.201.77',
            'port' => '3306',
            'database' => 'client_portal',
            'username' => 'marc',
            'password' => 'm@rc@db',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,

        ],
		'nexus' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.200.133\CICMSSQLSERVER',
            'database' => 'I3_IC',
            'port' => '1433',
            'username' => 'sa',
            'password' => 'M@gell@n$oln!@#$',
            'charset' => 'utf8',
            'prefix' => '',
        ],  

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host'     => '127.0.0.1',
            'port'     => 6379,
            'database' => 0,
        ],

    ],

];
