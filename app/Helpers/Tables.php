<?php namespace App\Helpers; 

use DateTime; 
use DateTimeZone;
use Config;
class Tables{

   public static function report_data_tbl($data){
  //pre($data);
          $contents = [];
          $keys_temp = [];

          foreach ($data as $dv) {

             $temp_data = mb_clean_campaign_data($dv['contents']);
             $temp_data['date'] = $dv['created_at'];
             $contents[] = $temp_data;
             $keys_temp = array_merge($keys_temp,array_keys($temp_data));
          }

          $keys = mb_dksort(array_unique($keys_temp),'date');

          $value = [];
          foreach ($contents as $ck => $cv) {
              $tempvalue = [];
              foreach ($keys as $key) {
                  $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
              }
              $value[] = $tempvalue;
          }
          $newkeys = array_keys($value);
          return view('email._table',compact('value','keys'));
   }



   public function email_data_tbl($data,$opt = []){ 
    
      $not_included = ['test_email','lead_id','phone_number','user','lob','campaign_id','request_status'];

      foreach ($data as $k => $v) {

          if($k == 'CREDIT_CARD_NUMBER#'){

            $v =  str_repeat('*', strlen($v) - 6) . substr($v, -6);
           
          }

          if(!in_array($k, $not_included)){

              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;

          }
      }
      
      $value_temp['Date'] = (isset($opt->timezone)?$this->convert_tz2(date("Y-m-d H:i:s"),$opt->timezone):date("Y-m-d H:i:s"));

      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');
      
      return view('email._table',compact('value','keys'));

   }

   public function email_data_tbl_horizontal($data,$opt = []){ 

      $not_included = ['test_email','lead_id','phone_number','user','lob','campaign_id','request_status'];
      
      foreach ($data as $k => $v) {

          if($k == 'CREDIT_CARD_NUMBER#'){

            $v =  str_repeat('*', strlen($v) - 6) . substr($v, -6);
           
          }

          if(!in_array($k, $not_included)){

              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;

          }
      }      
      
      $value_temp['Date'] = (isset($opt->timezone)?$this->convert_tz2(date("Y-m-d H:i:s"),$opt->timezone):date("Y-m-d H:i:s"));
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');

      return view('email._tablehorizontal',compact('value','keys'));

   }

      public function email_data_tbl2($data){

      $not_included = ['test_email','lead_id','phone_number','user','lob','campaign_id','request_status'];

      foreach ($data as $k => $v) {
          if(!in_array($k, $not_included)){
              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;
          }
      }

      $value_temp['Date'] = date('Y-m-d H:i:s' , strtotime('-6 hour', strtotime(date("Y-m-d H:i:s"))));
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');
      
  
      return view('email._table',compact('value','keys'));

   }

    public function email_data_tbl3($data){

      $not_included = ['test_email','lead_id','phone_number','user','lob','campaign_id','request_status'];

      foreach ($data as $k => $v) {
          if(!in_array($k, $not_included)){
              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;
          }
      }

      $value_temp['Date'] = date('Y-m-d H:i:s' , strtotime('-12 hour', strtotime(date("Y-m-d H:i:s"))));
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');

      
      return view('email._table',compact('value','keys'));

   }


   public static function dispo_data_tbl($data){

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1){
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1['status'];    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1['count'];
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; }     
      $record1 .= "</table>";
      return $record1;

   }

   public static function html_logs($data){

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1){
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->status;    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->count;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; }     
      $record1 .= "</table>";

      return $record1;

   }


   public static function html_logs2($data){

      $record1 = "<p>Eod Call Logs</p>";
      $record1 .= "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1){
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->status;    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->count;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; }     
      $record1 .= "</table>";
      return $record1;

   }



   public static function html_data($data){

        $contents = [];
        $keys_temp = [];

        foreach ($data as $dv) {

           $temp_data = mb_clean_campaign_data($dv->contents);
           $temp_data['date'] = $dv->created_at;
           $contents[] = $temp_data;
           $keys_temp = array_merge($keys_temp,array_keys($temp_data));
        }

        $keys = mb_dksort(array_unique($keys_temp),'date');

        $value = [];
        foreach ($contents as $ck => $cv) {
            $tempvalue = [];
            foreach ($keys as $key) {
                $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
            }
            $value[] = $tempvalue;
        }
        $newkeys = array_keys($value);
        return view('email._table',compact('value','keys'));

   }

   public static function html_data2($data){

        $contents = [];
        $keys_temp = [];

        foreach ($data as $dv) {
          
           $temp_data = mb_clean_campaign_data($dv->contents);
           $temp_data['date'] = $dv->created_at;
           $contents[] = $temp_data;
           $keys_temp = array_merge($keys_temp,array_keys($temp_data));
  
        }

        $keys = mb_dksort(array_unique($keys_temp),'date');

        $value = [];
        foreach ($contents as $ck => $cv) {

            $tempvalue = [];
            foreach ($keys as $key) {
                $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
            }
            $value[] = $tempvalue;
        }
        $newkeys = array_keys($value);
        return view('email._table2',compact('value','keys'));

   }

   public static function cleaner($data){

      return mb_clean_campaign_data($data); 

  }

  public static function convert_tz($date_time, $from_tz, $to_tz)
  {
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    //pre($time_object,false);
    $time_object->setTimezone(new DateTimeZone($to_tz));
    //pre($time_object);
    return $time_object->format('Y-m-d H:i:s');
  }

  public function convert_tz2($date_time,$to_tz)
  {
      $time_object = new DateTime($date_time, new DateTimeZone(Config::get('app.timezone')));
      $time_object->setTimezone(new DateTimeZone($to_tz));
      return $time_object->format('Y-m-d H:i:s');
  }   


  public static function time_range($opt){

    $x = json_decode($opt,true);
    $fromTz = 'Asia/Manila';
    $tz = (isset($x['timezone'])?$x['timezone']:'Asia/Manila');
    $from_object = new DateTime((isset($x['from'])?$x['from']:'16:59:59'), new DateTimeZone($fromTz));
    $from_object->modify('-1 day');
    $orig_from = $from_object->format('Y-m-d H:i:s');
    $from_object->setTimezone(new DateTimeZone($tz));
    $to_object = new DateTime((isset($x['to'])?$x['to']:'17:00:00'), new DateTimeZone($fromTz));
    $orig_to = $to_object->format('Y-m-d H:i:s');
    $to_object->setTimezone(new DateTimeZone($tz));    
    return [
      'fr_tz' => $fromTz,
      'to_tz' => $tz,
      'ofr' => $orig_from,
      'fr' => $from_object->format('Y-m-d H:i:s'),
      'oto' => $orig_to,
      'to' => $to_object->format('Y-m-d H:i:s')
    ];

  }

}

?>   
