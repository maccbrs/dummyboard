<?php namespace App\Helpers; 

class Tables{

   public static function report_data_tbl($data){
  //pre($data);
          $contents = [];
          $keys_temp = [];

          foreach ($data as $dv) {

             $temp_data = mb_clean_campaign_data($dv['contents']);
             $temp_data['date'] = $dv['created_at'];
             $contents[] = $temp_data;
             $keys_temp = array_merge($keys_temp,array_keys($temp_data));
          }

          $keys = mb_dksort(array_unique($keys_temp),'date');

          $value = [];
          foreach ($contents as $ck => $cv) {
              $tempvalue = [];
              foreach ($keys as $key) {
                  $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
              }
              $value[] = $tempvalue;
          }
          $newkeys = array_keys($value);
          return view('email._table',compact('value','keys'));
   }



   public function email_data_tbl($data){

      $not_included = ['test_email','lead_id','phone_number','user','lob','campaign_id','request_status'];

      foreach ($data as $k => $v) {
          if(!in_array($k, $not_included)){
              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;
          }
      }

      $value_temp['Date'] = date("Y-m-d H:i:s");
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');
      return view('email._table',compact('value','keys'));

   }


   public static function dispo_data_tbl($data){

      $record1 = "<table style='margin-left: 30px' border='1' cellspacing='0'>";  
      $record1 .= "<tr><th bgcolor='#800000'align='Center'><font color='white'>DISPO Code</th><th bgcolor='#800000'align='Center'><font color='white'>Count</th></tr>";
           foreach ($data as $row1){
              $record1 .= "<tr><td>"; 
              $record1 .= $row1['status'];    
              $record1 .= "</td><td>";
              $record1 .= $row1['count'];
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            }     
      $record1 .= "</table>";
      return $record1;

   }


}

?>
