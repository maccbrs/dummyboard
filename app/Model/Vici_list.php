<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vici_list extends Model
{

	protected $connection = 'vicidial';
    protected $table = 'vicidial_list';
    protected $fillable = ['email'];  

}