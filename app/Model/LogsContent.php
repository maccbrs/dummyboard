<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogsContent extends Model
{
    protected $table = 'logs_contents';
}
