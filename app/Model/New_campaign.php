<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class New_campaign extends Model
{
    protected $table = 'new_campaign';
    protected $fillable = ['campaign_id', 'lob']; 

}