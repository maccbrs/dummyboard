<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientPortalContent extends Model
{
	protected $connection = 'gardenia';
    protected $table = 'content';
    protected $fillable = ['user','lead_id', 'campaign_id','session_id','content','lists','updated','status','dispo']; 

} 