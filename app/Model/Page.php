<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['status','campaign_id', 'title', 'template','contents','board_id','options','custom_head']; 


    public function board(){
        return $this->belongsTo('App\Model\Board');
    }
}