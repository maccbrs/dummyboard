<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Emailer extends Model
{
    protected $table = 'emailers';
    protected $fillable = ['from','to','cc','reply_to','content','bcc','name','subject']; 


}