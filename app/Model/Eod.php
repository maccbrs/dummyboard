<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Eod extends Model
{
    protected $table = 'eod_reports';
    protected $fillable = ['from','to','cc','content','bcc','name','subject','status','date']; 


}