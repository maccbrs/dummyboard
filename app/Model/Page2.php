<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page2 extends Model
{
    protected $table = 'published_pages';

    protected $fillable = ['status','id','campaign_id', 'title', 'template','contents','board_id','options','custom_head']; 



    public function board(){
        return $this->belongsTo('App\Model\Board2','id');
    }
}