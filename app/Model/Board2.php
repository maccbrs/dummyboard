<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Board2 extends Model
{
    protected $table = 'published_boards';
    protected $fillable = ['status','id','campaign_id', 'lob', 'to','cc','from','primary_page','test_email','campaign']; 
    

    public function pages(){
        return $this->hasMany('App\Model\Page2','board_id'); 
    }      
}