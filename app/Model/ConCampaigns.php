<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConCampaigns extends Model
{
    protected $table = 'con_user_campaign';
    protected $fillable = ['users_id','campaign_id', 'reports','status','email'];  

    public function board(){
        return $this->belongsTo('App\Model\Board','campaign_id','campaign_id');
    }

}