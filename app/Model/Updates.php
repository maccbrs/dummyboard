<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Updates extends Model
{
    protected $table = 'updates';
    protected $fillable = ['status','count', 'contents','campaign_id']; 


    public function board(){
        return $this->belongsTo('App\Model\Board','campaign_id','campaign_id');
    }
 
    public function mb_update($cid,$cont,$count){
    	if(!$this->where('campaign_id',$cid)->update(['contents' => $cont,'count' => $count])){
            $data = array(
                'campaign_id' => $cid,
                'count' => 1,
                'contents' => $cont
            );
            $this->create($data);
        }
    }

    public function getby_campaign($cid){
    	return $this->where('campaign_id',$cid)->first();
    }
}