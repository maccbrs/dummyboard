<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        // 

        parent::boot($router);
        $router->bind('blog', function($value) {
            return App\Blog::where('slug', $value)->first();
        });

        $router->bind('board', function($value) {
            return \App\Model\Board::where('campaign_id', $value)
            ->where('status',1)
            ->with(['pages' => 

                function ($query) { $query->where('status',1);
                    },

                    'assigned'])->first(); 

        });

        $router->bind('page', function($value) {
            return \App\Model\Page::where('id', $value)->where('status',1)->first();
        });

        $router->bind('user', function($value) {
            return \App\User::where('id', $value)->with(['campaigns'])->first();

        }); 

        $router->bind('board2', function($value) {
            return \App\Model\Board2::where('campaign_id', $value)->where('status',1)->with(['pages'])->first();
        });

        $router->bind('page2', function($value) {
            return \App\Model\Page2::where('id', $value)->where('status',1)->first();
        }); 

        $router->bind('campaign_data', function($value) {
            return \App\Model\CampaignData::where('campaign_id', $value)->where('active',1)->get();
        });  

        $router->bind('campaign_logs', function($value) {
            return \App\Model\Logs::where('campaign_id', $value)->get();
        });
        $router->bind('user_logs', function($value) {
            return \App\Model\Logs::where('users_id', $value)->get();
        });

        $router->bind('emailer_id', function($value) {
            return \App\Model\Emailer::where('id', $value)->first(); 
        });          

        $router->bind('eod_date', function($value) {
            return \App\Model\Eod::where('date', $value)->get(); 
        });  
        $router->bind('eod_id', function($value) {
            return \App\Model\Eod::where('id', $value)->first(); 
        }); 
    } 

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
