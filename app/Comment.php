<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'comments';
    protected $fillable = ['title', 'content', 'blog_id','user_id'];


    public function blog(){
        return $this->belongsTo('App\Blog');
    }
}