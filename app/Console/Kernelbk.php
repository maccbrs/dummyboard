<?php
namespace App\Console;
use Mail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Model\Board;
use App\Model\CampaignData;
use App\Helpers\Tables;
use App\Model\Logs;
use App\Model\VICI;
use DB;

class Kernel extends ConsoleKernel
{
    // public function __construct(Tables $help_tbl,Board $board,Logs $logs){
    //     $this->help_tbl = $help_tbl;
    //     $this->board = $board;
    //     $this->logs = $logs;
    // }
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)

    {
        $schedule->call(function () {

            $test = false;
            $done = [];

            $connectors = DB::table('vici_to_templar')->orderBy('id', 'desc')->get();
            $current_date = date('Y-m-d');
            $current_time = date('H:i:s');
            foreach ($connectors as $connector) {

                $html_data = $html_data = '';
                $logs = [];
                $data = [];
                $in = [];
                $templar_socket = [];

                if(!in_array($connector->templar_socket, $done)){

                    $dates = Tables::timerange($connector->options);
                    $dates['time'] = $current_time;
                    $conn = 'vicidial';
                    switch ($connector->bound) {
                        case 'out':
                            $table = 'vicidial_log';
                            break;
                        default:
                            $table = 'vicidial_closer_log';
                            break;
                    }

                    $board = Board::whereIn('campaign_id',json_decode($connector->templar_socket,true))->where('status',1)->first()->toArray();
                    $vici = DB::connection($conn)->table($table);

                    if($board && in_array('daily', mb_parse_report_options($board['options']))){

                        $opt = json_decode($board['options']);
                        $in = json_decode($connector->vici_plug,true);

                            if(!empty($in)){
                                $logs = $vici->select('status',DB::raw('count(*) as count'))
                                        ->whereBetween('call_date', [$dates['fr'],$dates['to']])
                                        ->whereIn('campaign_id',$in)
                                        ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                                        ->groupBy('status')
                                        ->orderBy('status', 'asc')
                                        ->get();                
                            }
                            if(!empty($logs)){ 
                                
                                pre($logs,false);
                                $html_logs = Tables::html_logs($logs);  

                                //board input data
                                $data = DB::table('campaign_data')
                                        ->whereIn('campaign_id',json_decode($connector->templar_socket,true))
                                        ->where('contents', 'like', '%"request_status":"live"%')
                                        ->whereBetween('created_at',[$dates['fr'],$dates['to']])
                                        ->get();
                                if(!empty($data)){
                                    $html_data = Tables::html_data($data);
                                } 

                                if($test){

                                //use this to test it with you own email and will not send to client
                                    $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
                                    $board['to'] = ['marlonbbernal@mailinator.com', 'howell.calabia@magellan-solutions.com'];
                                    $board['from'] = 'reporter@magellan-solutions.com';
                                    $board['cc'] = false;
                                    $board['bcc'] = false;
                                    
                                }else{

                                    $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
                                    $board['to'] = (!empty($board['to'])?emailer_parser2($board['to']):false);
                                    $board['from'] = ($board['from'] != ''?$board['from']:'reporter@magellan-solutions.com');
                                    $board['cc'] = (!empty($board['cc'])?emailer_parser2($board['cc']):false);
                                    $board['bcc'] = (!empty($board['bcc'])?emailer_parser2($board['bcc']):false);
                                }

                                if(!$board['to']){
                                        $board['to'] = 'NOC@magellan-solutions.com';
                                        Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                           $m->from($board['from'], 'Magellan Reporting Service');
                                           $m->to($board['to'], $board['lob'])->subject('No Recipient eod!'); 
                                        }); 
                                }else{
                                    Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                       $m->from($board['from'], 'Magellan Reporting Service');
                                       $m->to($board['to'], $board['lob'])->subject($board['subject']); 
                                       if($board['cc'] && !$board['bcc']){
                                         $m->to($board['to'], $board['lob'])->cc($board['cc'])->subject($board['subject']); 
                                       }else if(!$board['cc'] && $board['bcc']){
                                         $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->subject($board['subject']); 
                                       }else if($board['cc'] && $board['bcc']){
                                         $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
                                       }
                                       
                                    }); 
                                    $status = 1;                       
                                }

                                if(!$test){
                                    $cc = ($board['cc']?json_encode($board['cc']):'');
                                    $to = ($board['to']?json_encode($board['to']):'');
                                    $bcc = ($board['bcc']?json_encode($board['bcc']):'');
                                    DB::table('eod_reports')->insert([
                                        'to' => $to,
                                        'from' => $board['from'],
                                        'cc' => $cc,
                                        'bcc' => $bcc,
                                        'subject' => $board['subject'],
                                        'date' => $current_date,
                                        'status' => $status,
                                        'campaign' => $board['campaign'],
                                        'options' => json_encode($dates),
                                        'content' => $html_logs.'<br>'.$html_data
                                    ]);
                                }

                            }else{
                                
                                if(!$test){
                                    DB::table('eod_reports')->insert([
                                        'date' => $current_date,
                                        'status' => 0,
                                        'campaign' => $board['campaign']
                                    ]);
                                } 

                            }

                    }

                    $done[] = $connector->templar_socket;
                }

            }                     

        })->dailyAt('13:30');     //12:30       
    }
}