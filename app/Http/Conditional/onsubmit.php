<?php 

switch ($cid) {

  case '56d7a464f2618':

      $board->to = (!empty($board->to)?$board->to:['noc@magellan-solutions.com']);
    if($request->input('offernumber_description') != ''){
          Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
             $m->from($board->from);
             $m->to($board->to)->subject($board->subject); 
             if(!empty($board->cc) && empty($board->bcc)){
               $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
             }else if(empty($board->cc) && !empty($board->bcc)){
               $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
             }else if(!empty($board->cc) && !empty($board->bcc)){
               $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
             }
          });

    }else{

      $board->subject = 'Survey Completed ';
      Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
             $m->from($board->from);
             $m->to($board->to)->subject($board->subject); 
             if(!empty($board->cc) && empty($board->bcc)){
               $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
             }else if(empty($board->cc) && !empty($board->bcc)){
               $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
             }else if(!empty($board->cc) && !empty($board->bcc)){
               $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
             }
          }); 
    }

    break;

    case "5748837f750c1":


      $email_tbl = $this->help_tbl->email_data_tbl2($input,$opt);

      $fivestarsubject = $input['Product'] . "/" .$input['Property_Name'] . "/" .$input['Priority'];
                        
            if($input['Priority'] == "High"){
              $holder = array();
              
              array_push($holder,"support@serenata.com");
              $board->cc = $holder;
                    
            } 

            $board->subject = $fivestarsubject;

            Mail::send('email.report2', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
               $m->from($board->from);
               $m->to($board->to)->subject($board->subject); 

               if(empty($requestsubject)){
                    
                    $options = json_decode($board->options, true); 
                    $subject = $options["subject"];
                    $board->subject = $subject; 
                    $m->to($board->to)->subject($subject);  

                }

               if(!empty($board->cc) && empty($board->bcc)){
                 $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
               }else if(empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
               }else if(!empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
               }
                
            });

          $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';
                flash()->success($msg);
              return redirect()->back();  
    break;


    case "57d1ee5243123":


      $email_tbl = $this->help_tbl->email_data_tbl3($input,$opt);

            Mail::send('email.report2', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
               $m->from($board->from);
               $m->to($board->to)->subject($board->subject); 

               if(empty($requestsubject)){
                    
                    $options = json_decode($board->options, true); 
                    $subject = $options["subject"];
                    $board->subject = $subject; 
                    $m->to($board->to)->subject($subject);  

                }

               if(!empty($board->cc) && empty($board->bcc)){
                 $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
               }else if(empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
               }else if(!empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
               }
                
            });
        
            $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';

            flash()->success($msg);
            return redirect()->back(); 

    break;

    case "OmniDRC":

       $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);

            Mail::send('email.report_drc', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
               $m->from($board->from);
               $m->to($board->to)->subject($board->subject); 

               if(empty($requestsubject)){
                    
                    $options = json_decode($board->options, true); 
                    $subject = $options["subject"];
                    $board->subject = $subject; 
                    $m->to($board->to)->subject($subject);  

                }

               if(!empty($board->cc) && empty($board->bcc)){
                 $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
               }else if(empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
               }else if(!empty($board->cc) && !empty($board->bcc)){
                 $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
               }
                
            });
        
            $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';

            flash()->success($msg);
            return redirect()->back(); 
    break;

    case '5776d34f949fb':

      if($request->input('type') == 'technical assistance' ):

        $board->to = ['support@muviboxes.com'];

      endif;

      Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
         $m->from($board->from);
         $m->to($board->to)->subject($board->subject); 
         if(!empty($board->cc) && empty($board->bcc)){
           $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
         }else if(empty($board->cc) && !empty($board->bcc)){
           $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
         }else if(!empty($board->cc) && !empty($board->bcc)){
           $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
         }
      });

    break;

    case "OmniDentures":

        $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);


        Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {

            if ($requestsubject == 'SECURE SMILE TEETH - ORDER PLACED' || $requestsubject == 'SECURE SMILE TEETH - ORDER FAILED'){
                $board['to'] = ['jasonlvansickle@yahoo.com', 'sales@securesmileteeth.com'];
                $board['from'] = 'operations@magellan-solutions.com';
                $board['cc'] = ['webdev@magellan-solutions.com', 'operations@magellan-solutions.com', 'maryan.anora@magellan-solutions.com',
                'reports@magellan-solutions.com', 'julie.galinato@magellan-solutions.com'];
                $board['bcc'] = false;

            }else{
                $board['to'] = ['teamomnitrix@magellan-solutions.com', ];
                $board['from'] = 'operations@magellan-solutions.com';
                $board['cc'] = ['webdev@magellan-solutions.com', 'operations@magellan-solutions.com', 'maryan.anora@magellan-solutions.com',
                    'reports@magellan-solutions.com', 'julie.galinato@magellan-solutions.com'];
                $board['bcc'] = false;
            }
            $m->from($board->from);
            $m->to($board->to)->subject($board->subject);

            if(empty($requestsubject)){

                $options = json_decode($board->options, true);
                $subject = $options["subject"];
                $board->subject = $subject;
                $m->to($board->to)->subject($subject);

            }

            if(!empty($board->cc) && empty($board->bcc)){
                $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject);
            }else if(empty($board->cc) && !empty($board->bcc)){
                $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject);
            }else if(!empty($board->cc) && !empty($board->bcc)){
                $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject);
            }

        });

        $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';

        flash()->success($msg);
        return redirect()->back();

    default:
  break;
}

?> 