<?php 

  $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);

  $content = json_decode($data['contents'], TRUE);
                  
  if(empty($input['order_id'])){

    $subject = $content['lob'] . ' - Customer Service';

  }else{


    $subject = $content['lob'] . ' - Sale';

    $board->to = ['operations@magellan-solutions.com'];

  }

  $board->subject = $subject;


  
  Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
     $m->from($board->from);
     $m->to($board->to)->subject($board->subject); 

     if(empty($requestsubject)){
          
          $options = json_decode($board->options, true); 
          $subject = $options["subject"];
          $board->subject = $subject; 
          $m->to($board->to)->subject($subject);  

      }

     if(!empty($board->cc) && empty($board->bcc)){
       $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
     }else if(empty($board->cc) && !empty($board->bcc)){
       $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
     }else if(!empty($board->cc) && !empty($board->bcc)){
       $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
     }
      
  });

  $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';

  flash()->success($msg);
  return redirect()->back();

?>