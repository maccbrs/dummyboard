<?php 
	
	if($board[0]['campaign_id'] == "57336b5e72b6a"){

		if(!empty($in)){
            $logs = $vici->select('status',DB::raw('count(*) as count'))
                    ->whereBetween('call_date', [$dates['yesterday'], $dates['today']])
                    ->whereIn('campaign_id',$in)
                    ->whereIn('status',['DNC'])
                    ->groupBy('status')
                    ->orderBy('status', 'asc')
                    ->get();                
        }
	
      if(!empty($logs)){ 

	        $html_logs = Tables::html_logs($logs);  

	        //board input data
	        $data = DB::table('campaign_data')
	                ->whereIn('campaign_id',$templar_socket)
	                ->where('contents', 'like', '%"request_status":"live"%')
	                ->whereIn('status',['DNC'])
	                ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
	                ->get();
	        if(!empty($data)){
	            $html_data = Tables::html_data($data);
	        } 

	        if($test){

	        //use this to test it with you own email and will not send to client
	            $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
	            $board['to'] = ['marlonbbernal@mailinator.com', 'howell.calabia@magellan-solutions.com'];
	            $board['from'] = 'reporter@magellan-solutions.com';
	            $board['cc'] = false;
	            $board['bcc'] = false;
	            
	        }else{

	            $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
	            $board['to'] = (!empty($board['to'])?emailer_parser2($board['to']):false);
	            $board['from'] = ($board['from'] != ''?$board['from']:'reporter@magellan-solutions.com');
	            $board['cc'] = (!empty($board['cc'])?emailer_parser2($board['cc']):false);
	            $board['bcc'] = (!empty($board['bcc'])?emailer_parser2($board['bcc']):false);

	        }

	        if(!$board['to']){
	                $board['to'] = 'NOC@magellan-solutions.com';
	                Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
	                   $m->from($board['from'], 'Magellan Reporting Service');
	                   $m->to($board['to'], $board['lob'])->subject('No Recipient eod!'); 
	                }); 
	        }else{
	            Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
	               $m->from($board['from'], 'Magellan Reporting Service');
	               $m->to($board['to'], $board['lob'])->subject($board['subject']); 
	               if($board['cc'] && !$board['bcc']){
	                 $m->to($board['to'], $board['lob'])->cc($board['cc'])->subject($board['subject']); 
	               }else if(!$board['cc'] && $board['bcc']){
	                 $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->subject($board['subject']); 
	               }else if($board['cc'] && $board['bcc']){
	                 $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
	               }
	               
	            }); 
	            $status = 1;                       
	        }

	        if(!$test){
	            $cc = ($board['cc']?json_encode($board['cc']):'');
	            $to = ($board['to']?json_encode($board['to']):'');
	            $bcc = ($board['bcc']?json_encode($board['bcc']):'');
	            DB::table('eod_reports')->insert([
	                'to' => $to,
	                'from' => $board['from'],
	                'cc' => $cc,
	                'bcc' => $bcc,
	                'subject' => $board['subject'],
	                'date' => date('Y-m-d '),
	                'status' => $status,
	                'campaign' => $board['campaign'],
	                'content' => $html_logs.'<br>'.$html_data
	            ]);
	        }

	    }else{
	       
	        if(!$test){
	            DB::table('eod_reports')->insert([
	                'date' => date('Y-m-d '),
	                'status' => 0,
	                'campaign' => $board['campaign']
	            ]);
	        }   
	    }

	}


?> 