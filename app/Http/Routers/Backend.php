<?php 

$router->group(['namespace' => 'Backend'],function(){

	$this->get('logout',['as' => 'logout','uses' => 'testController@destroy']);

	$this->group(['middleware' => ['auth', 'auth.dash'],'prefix' => 'dashboard'],function(){	

			$this->get('/',['as' => 'dashboard','uses' => 'boardController@index']);

			$this->group(['prefix' => 'user'],function(){
				$this->get('/',['as' => 'users.index','uses' => 'userController@index']);
				$this->get('/add',['as' => 'users.add','uses' => 'userController@add']);
				$this->post('/save',['as' => 'users.create','uses' => 'userController@create']);
				$this->get('/edit/{user}',['as' => 'users.update','uses' => 'userController@update']);
				$this->post('/edit/{user}',['as' => 'users.save','uses' => 'userController@save']);
				$this->get('/delete/{user}',['as' => 'users.delete','uses' => 'userController@delete']);
				$this->get('/settings/reporting',['as' => 'users.settings.reporting','uses' => 'userController@reporting']);
				$this->post('/settings/reporting',['as' => 'users.settings.reporting.post','uses' => 'userController@reporting_post']);
				$this->post('/resetPassword',['as' => 'users.resetPassword','uses' => 'userController@resetPassword']);
			});

			$this->group(['prefix' => 'boards'],function(){
				$this->get('/',['as' => 'board.index','uses' => 'boardController@index']); 
				$this->get('add',['as' => 'board.add','uses' => 'boardController@add']);
				$this->post('save',['as' => 'board.add.save','uses' => 'boardController@save']);
				$this->get('edit/{board}',['as' => 'board.edit','uses' => 'boardController@edit']);
				$this->post('edit-save/{board}',['as' => 'board.edit.save','uses' => 'boardController@edit_post']);
				$this->get('delete/{board}',['as' => 'board.delete','uses' => 'boardController@delete']); 
				$this->get('test',['as' => 'board.test','uses' => 'boardController@test']);
				$this->group(['prefix' => 'mail'],function(){
					$this->get('/{board}',['as' => 'backend.admin.board.mail','uses' => 'boardController@mail']);
					$this->post('/save/{board}',['as' => 'backend.admin.board.mail.save','uses' => 'boardController@mail_save']);
				});

			});   

			$this->group(['prefix' => 'page'],function(){
				$this->get('{board}/add',['as' => 'page.add','uses' => 'pageController@add']);
				$this->post('create',['as' => 'page.add.save','uses' => 'pageController@create']); 
				$this->get('edit/{page}',['as' => 'page.edit','uses' => 'pageController@edit']);
				$this->post('edit/{page}',['as' => 'page.edit.save','uses' => 'pageController@update']);
				$this->post('edit2/{page}',['as' => 'page.edit2.save','uses' => 'pageController@update2']);
				$this->get('delete/{page}',['as' => 'page.edit','uses' => 'pageController@delete']);
				$this->get('test',['as' => 'page.test','uses' => 'pageController@test']);
 			});

			$this->get('/publish',['as' => 'publish.index','uses' => 'dashboardController@publish']);
			$this->get('/publish/{board}/save',['as' => 'publish','uses' => 'dashboardController@publish_save']);

			$this->group(['prefix' => 'campaign'],function(){
				$this->get('/',['as' => 'campaign.index','uses' => 'campaignController@index']);
				$this->get('/add',['as' => 'campaign.add','uses' => 'campaignController@add']);
				$this->post('/add',['as' => 'campaign.add.create','uses' => 'campaignController@create']);
				$this->get('/delete/{id}',['as' => 'campaign.delete','uses' => 'campaignController@delete']);
			});

			$this->group(['prefix' => 'logs'],function(){
				$this->get('/',['as' => 'logs.index','uses' => 'logsController@index']);
				$this->get('/user/{user_logs}',['as' => 'logs.user','uses' => 'logsController@user']);
				$this->get('/campaign/{campaign_logs}',['as' => 'logs.campaign','uses' => 'logsController@campaign']);  
			});  

			$this->group(['prefix' => 'emailer'],function(){
				$this->get('/',['as' => 'emailer.index','uses' => 'emailerController@index']);
				$this->get('/add',['as' => 'emailer.add','uses' => 'emailerController@add']);
				$this->get('/email_blast',['as' => 'emailer.email_blast','uses' => 'emailerController@email_blast']);
				$this->post('/create',['as' => 'emailer.create','uses' => 'emailerController@create']);
				$this->get('/edit/{emailer_id}',['as' => 'emailer.edit','uses' => 'emailerController@edit']);
				$this->post('/edit/{emailer_id}',['as' => 'emailer.update','uses' => 'emailerController@update']);
				$this->get('/delete/{emailer_id}',['as' => 'emailer.delete','uses' => 'emailerController@delete']);
				$this->post('/email_blast_send',['as' => 'emailer.email_blast_send','uses' => 'emailerController@email_blast_send']);
				$this->post('/email_blast_load',['as' => 'emailer.email_blast_load','uses' => 'emailerController@email_blast_load']);
				

			}); 

			$this->group(['prefix' => 'tool'],function(){
				$this->get('/',['as' => 'tool.index','uses' => 'toolController@index']);
				$this->get('/1010',['as' => 'tool.crc_1010','uses' => 'toolController@crc_1010']);
				$this->get('/change_data',['as' => 'tool.change_data','uses' => 'toolController@change_data']);
				$this->post('/change_data',['as' => 'tool.change_data','uses' => 'toolController@update_data']);
				$this->post('/update_content/{id}',['as' => 'tool.update_content','uses' => 'toolController@update_content']);
				$this->post('/blockage',['as' => 'tool.blockage','uses' => 'toolController@blockage']);
				$this->post('/blockage_1010',['as' => 'tool.blockage1010','uses' => 'toolController@blockage1010']);
				$this->get('/edit',['as' => 'tool.edit','uses' => 'toolController@edit']);
				$this->post('/edit',['as' => 'tool.update','uses' => 'toolController@update']);
				$this->post('/edit_1010',['as' => 'tool.update1010','uses' => 'toolController@update1010']);

			}); 

			$this->group(['prefix' => 'vct'],function(){
				$this->get('/',['as' => 'vct.index','uses' => 'vctController@index']);
				$this->get('/select-server',['as' => 'vct.select-server','uses' => 'vctController@select_server']);
				$this->get('/create',['as' => 'vct.create','uses' => 'vctController@create']);
				$this->post('/create',['as' => 'vct.save','uses' => 'vctController@save']);
				$this->get('/{vct_id}',['as' => 'vct.show','uses' => 'vctController@show']);
				$this->get('/{vct_id}/delete',['as' => 'vct.destroy','uses' => 'vctController@destroy']);
			}); 

			$this->group(['prefix' => 'reporter'],function(){
				$this->get('/',['as' => 'reporter.index','uses' => 'reporterController@index']);
				$this->get('/blockage',['as' => 'reporter.blockage','uses' => 'reporterController@blockage']);
				$this->post('/blockage',['as' => 'reporter.blockage','uses' => 'reporterController@blockage']);
				$this->get('/create_blockage',['as' => 'reporter.blockages.create','uses' => 'reporterController@create_blockage']);
			}); 

			$this->group(['prefix' => 'data_researcher'],function(){
				$this->get('/',['as' => 'data_researcher.index','uses' => 'researcherController@index']);
				$this->get('/update',['as' => 'data_researcher.edit','uses' => 'researcherController@edit']);
                $this->get('/claim/{lead_id?}',['as' => 'data_researcher.claim','uses' => 'researcherController@claim']);


				$this->post('/assignment',['as' => 'data_researcher.assignment','uses' => 'researcherController@assignment']);
				$this->post('/update',['as' => 'data_researcher.update','uses' => 'researcherController@update']);
			}); 

			$this->group(['prefix' => 'connectors'],function(){
				$this->get('/blockages',['as' => 'connectors.blockages.index','uses' => 'connectorsController@blockages_index']);
				$this->get('/blockages/create',['as' => 'connectors.blockages.create','uses' => 'connectorsController@blockages_create']);
				$this->post('/blockages/save',['as' => 'connectors.blockages.save','uses' => 'connectorsController@blockages_save']);
			}); 

			$this->group(['prefix' => 'bookworm'],function(){
				$this->get('/blockages',['as' => 'bookworm.blockages.index','uses' => 'bookwormController@index']);
				
			}); 

			$this->group(['prefix' => 'eod'],function(){
				$this->get('/',['as' => 'eod.index','uses' => 'eodController@index']);
				$this->get('/date/{eod_date}',['as' => 'eod.date','uses' => 'eodController@date']);
				$this->get('/show/{eod_id}',['as' => 'eod.show','uses' => 'eodController@show']);
			}); 

			$this->group(['prefix' => 'eod-generator'],function(){
				$this->get('/',['as' => 'eod-generator.index','uses' => 'eodgeneratorController@index']);
				$this->post('/show',['as' => 'eod-generator.show','uses' => 'eodgeneratorController@show']);
				$this->post('/send',['as' => 'eod-generator.send','uses' => 'eodgeneratorController@send']);
			}); 
			
			$this->group(['prefix' => 'upload'],function(){
				$this->get('/',['as' => 'upload.index','uses' => 'uploadController@index']);
				//$this->get('/add',['as' => 'upload.add','uses' => 'uploadController@upload_pdf']);
				$this->post('/create/save',['as' => 'upload.create.save','uses' => 'uploadController@upload_pdf']);
				$this->get('/create',['as' => 'upload.create','uses' => 'uploadController@create']);
				$this->post('/delete',['as' => 'upload.delete','uses' => 'uploadController@delete']);
			}); 



	}); 

});  

