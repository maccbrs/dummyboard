<?php  


$this->group(['namespace' => 'tulip','middleware' => "auth",'prefix' => 'tulip'],function(){

	$this->get('/',['as' => 'tulip.index','uses' => 'tulipController@index']);
	$this->get('/generate/{id}','tulipController@generate');

});