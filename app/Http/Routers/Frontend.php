<?php 



$router->group(['namespace' => 'Frontend'],function(){
	$this->get('/',['as' => "home", 'uses' => "testController@index"]);
	$this->get('register',['as' => "register", 'uses' => "testController@register"]);
	$this->get('tabularboard',['as' => "tabularboard", 'uses' => "tabularboardController@display"]);
	$this->post('register',['as' => "register.post", 'uses' => "testController@create"]);
	$this->get('login',['as' => "login", 'uses' => "testController@login"]);
	$this->post('login',['as' => "login.post", 'uses' => "testController@login_post"]);
	$this->get('cleotest',['as' => "cleotest", 'uses' => "testController@cleoTest"]);
	$this->get('iq_link', 'templatesController@iq_link');
	$this->get('iq_sched', 'templatesController@iq_sched');
	$this->post('iq_link_post', 'templatesController@iq_link_post');
	$this->post('iq_sched_post', 'templatesController@iq_sched_post');

	$this->group(['prefix' => 'preview'],function(){
	    $this->get('/{board}/{page}',['as' => "preview.board.page", 'uses' => "templatesController@index"]);
	    $this->get('/{board}',['as' => "preview.board", 'uses' => "templatesController@board"]);
	});
	

	$this->group(['prefix' => 'live'],function(){ 
		$this->get('/{board2}/{page2}',['as' => "board.page", 'uses' => "templatesController@index"]);
	    $this->get('/{board2}',['as' => "board", 'uses' => "templatesController@board"]);
	});	


	$this->group(['prefix' => 'train'],function(){ 
		$this->get('/{board2}/{page2}',['as' => "board.page", 'uses' => "trainController@index"]);
	    $this->get('/{board2}',['as' => "board", 'uses' => "trainController@board"]);
	    $this->get('/',['as' => "board", 'uses' => "trainController@train"]);
	});

	$this->group(['prefix' => 'emailer'],function(){ 
		$this->get('/send/{emailer_id}',['as' => 'emailer.send','uses' => 'emailerController@send']);
		$this->post('/send/{emailer_id}',['as' => 'emailer.sent','uses' => 'emailerController@sent']);
		
		
		$this->get('eod/send/{emailerid}',['as' => 'emailer.eod.send','uses' => 'emailerController@eod_send']);
		//$this->post('eod/send/{emailer_id}',['as' => 'emailer.eod.sent','uses' => 'emailerController@sent']);
	});
	

	

});   
$this->get('test',['uses' => 'testController@test']);
 $this->get('/testtemplate',['as' => "testmail.index", 'uses' => "testController@test_template"]);