<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



include_once app_path().'/Http/Routers/Backend.php';
include_once app_path().'/Http/Routers/Frontend.php';
include_once app_path().'/Http/Routers/Tulip.php';

$router->group(['prefix' => 'data'],function(){
	$this->post('save/{board}',['as' => 'data.save','uses' => 'dataController@store']);
	$this->post('save_outbound/{board}',['as' => 'data.save_outbound','uses' => 'dataController@store_outbound']);
	$this->get('/manualSend/{id}',['as' => 'emailer.manual','uses' => 'manualSendAutoMailer@manual']);
});




$router->get('cron_daily',['uses' => 'dataController@cron_daily']);
$router->get('test',['uses' => 'testController@test']);
$router->get('info',['uses' => 'testController@info']);


$router->post('/generate', 'dataController@eCapital');
$router->post('/custom-form', 'dataController@customForm');

	
