<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class testController extends Controller
{
    
	public function __construct(Guard $auth){
		$this->auth = $auth;
	}

    public function index()
    {
       echo 'admin index';
    }

    public function dashboard()
    {
       echo 'admin dashboard';
    }

  	public function destroy(){
  		$this->auth->logout();
  		return redirect()->route('home');
  	}
    
}