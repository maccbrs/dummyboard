<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\ClientPortalContent;
use App\User;
use Auth;


class researcherController extends Controller
{
	public function index(){

		$user = new User;

		$researchers = $user->select('name','id')->where('user_type','researcher')->get(); 

		return view('backend.admin.researcher.index',compact('connectors','researchers'));

	}

	public function edit(){
	    /*
	     * pool = all unedited leads, status = 1
	     * complete = leads submitted as completed status = 3
	     * incomplete = leads submitted as incomplete status = 2
	     * */

		$user_id = Auth::user()->id;

		$options = Auth::user()->options;
		$user_type = Auth::user()->user_type;

		if($user_type == 'admin'){

			$assigned_dispo = ['FU'];

		}else{

			$assigned_dispo = json_decode($options); 

		}

		$client_portal = new ClientPortalContent;

		$pool = $client_portal->whereIn('dispo',$assigned_dispo)
            ->whereIn('status', [1, ])
            ->orderBy('id','desc')
            ->paginate(25);


		foreach ($pool as $key => $value) {
            $pool[$key]['content2'] = json_decode($value['content'], true);
		}

        $incomplete = $client_portal->whereIn('dispo',$assigned_dispo)
            ->whereIn('updated',[$user_id , ])
            ->whereIn('status', [2, ])
            ->orderBy('id','desc')
            ->get();

        foreach ($incomplete as $key => $value) {
            $incomplete[$key]['content2'] = json_decode($value['content']);
        }

        $complete = $client_portal->whereIn('dispo',$assigned_dispo)
            ->whereIn('updated',[$user_id , ])
            ->whereIn('status',[3, ])
            ->orderBy('id','desc')
            ->paginate(25);

        foreach ($complete as $key => $value) {
            $complete[$key]['content2'] = json_decode($value['content']);
        }

        return view('backend.admin.researcher.edit',compact('pool', 'complete', 'incomplete', 'user_id'));

	}

	public function assignment(Request $r){

		$user = new User;

		$data['options'] = json_encode($r->input('dispo'));
		$data['user_type'] = $r->input('user_type'); 

		$user->where('id','=',$r->input('id'))->update($data);

		return redirect()->back();  

	}

	public function update(Request $r){
		$user_id = Auth::user()->id;

		$client_portal = new ClientPortalContent;

		$data['content'] = json_encode($r->input('content'));
		$data['updated'] = $r->input('updated');
        $data['status'] = 3;

		$data = $client_portal
			->where('id',$r->input('id'))
			->whereIn('updated',[$user_id, ])
			->update($data);

		$msg = 'Data has been Updated';
		flash()->success($msg);

		return redirect()->back();
	}

    public function claim($lead_id){


        /*
         * This is called via AJAX as soon as an agent clicks the Claim button
         * to identify if someone is already working on a lead
         * */

        $user_id = Auth::user()->id;
        $client_portal = new ClientPortalContent;

        $isClaimed = $client_portal->whereIn('lead_id', [$lead_id,])->first();
        $isClaimed = $isClaimed['updated'] != 1 ? true : false;
        if ($isClaimed)
            return "claimed";
        else{
            $data['updated'] = $user_id;
            $data['status'] = 2;
            $client_portal->where('lead_id', '=', $lead_id)
                ->update($data);
            return "not claimed";
        }


    }

}