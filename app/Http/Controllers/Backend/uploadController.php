<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\CampaignData;
use App\Helpers\Tables; 

class uploadController extends Controller
{

	public function __construct(Tables $tbl){
		$this->help_tbl = $tbl;
	}

	public function index(){
		
		return view('backend.admin.upload.index');
	}

	
	
	public function upload_pdf(Request $request){
			
    // Handle the User Upload of avatar
    	if($request->hasFile('pdf_file')){
    		$avatar = $request->file('pdf_file');
			$filename = $avatar->getClientOriginalName();
    		//$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		/*save( public_path('uploads/pdf/' . $filename));
    			Image::make($avatar)->resize(300,300)->save( public_path('uploads/avatars/' . $filename)); */
				
	    $avatar->move(public_path('pdf/') , $filename);
		$mode = "upload" ; 
    	$msg = "File Successfully Uploaded" ;
		}

		return view('backend.admin.upload.index' , compact('msg' , 'mode'));
		
    }

		
	public function create(){
			
		return view('backend.admin.upload.add');
	}

	
	public function delete(Request $request){
		
		
		 
			
	
		$filename = $request->filename; 
		
		chdir(public_path(  'pdf' . chr(92)));
			unlink($filename);
		//$deletefile = /*echo*/ public_path(  'pdf' . chr(92)  . $filename) ; 
		$mode = "delete" ; 
		$msg = "File has Deleted" ;
		return view('backend.admin.upload.index' , compact('msg','mode'));
		
		
		
	}

}