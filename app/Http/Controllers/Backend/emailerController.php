<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Emailer;
use App\Model\Emails;
use App\User;
use App\Model\Vici_list;
use Mail;
use Auth;

class emailerController extends Controller
{

    public function __construct(Request $request,Guard $guard,Emailer $emailer){
        $this->request = $request;
        $this->guard = $guard;
        $this->emailer = $emailer;
    }

	public function index(){
		
		$emails = $this->emailer->all()->toArray();

		return view('backend.admin.emailer.index',compact('emails'));
	}

	public function add(){ 

		return view('backend.admin.emailer.add');
	}

	public function create(){
		
		$to_arr = explode(',', $this->request->input('to'));
		$from_arr = explode(',', $this->request->input('from'));
		$cc_arr = explode(',', $this->request->input('cc'));
		$bcc_arr = explode(',', $this->request->input('bcc'));
		$replyTo_arr = explode(',', $this->request->input('reply_to'));

		$data = [];

		if(!empty($to_arr)) foreach ($to_arr as $v) { if(filter_var($v, FILTER_VALIDATE_EMAIL)) $data['to'][] = $v; };
		if(!empty($from_arr)) foreach ($from_arr as $v2) { if(filter_var($v2, FILTER_VALIDATE_EMAIL)) $data['from'][] = $v2; };
		if(!empty($cc_arr)) foreach ($cc_arr as $v3) { if(filter_var($v3, FILTER_VALIDATE_EMAIL)) $data['cc'][] = $v3; };
		if(!empty($bcc_arr)) foreach ($bcc_arr as $v4) { if(filter_var($v4, FILTER_VALIDATE_EMAIL)) $data['bcc'][] = $v4; };
		if(!empty($replyTo_arr)) foreach ($replyTo_arr as $v5 { if(filter_var($v5, FILTER_VALIDATE_EMAIL)) $data['bcc'][] = $v5; };

		$address['to'] = (isset($data['to'])? json_encode($data['to']): '');
		$address['from'] = (isset($data['from'])? json_encode($data['from']): '');
		$address['cc'] = (isset($data['cc'])? json_encode($data['cc']): '');
		$address['bcc'] = (isset($data['bcc'])? json_encode($data['bcc']): '');
		$address['name'] = $this->request->input('name');
		$address['subject'] = $this->request->input('subject');
		$address['reply_to'] = $this->request->input('reply_to');
		$address['content'] = $this->request->input('content');

		$this->emailer->create($address);
        flash()->success('new email entry created!');
        return redirect()->route('emailer.add'); 

	}

	public function edit($emailer){
		$emails = $emailer->toArray();
		return view('backend.admin.emailer.edit',compact('emails'));
	}	

	public function update($email){
		$email->to = json_encode(explode(',', $this->request->input('to')));
		$email->from = json_encode(explode(',', $this->request->input('from'))); 
		$email->cc = json_encode(explode(',', $this->request->input('cc')));
		$email->bcc = json_encode(explode(',', $this->request->input('bcc')));
		$email->name = $this->request->input('name');
		$email->subject = $this->request->input('subject');
		$email->content = $this->request->input('content');
		$email->save();
        flash()->success('email updated!');
        return redirect()->route('emailer.edit',['emailer_id' => $email->id]); 
	}

	public function delete($email){
		$email->delete();
		flash()->success('email deleted!');
		return redirect()->back();
	}

	public function send($emailer){
		$emails = $emailer->toArray();
		return view('backend.admin.emailer.send',compact('emails'));
	}

	public function email_blast(){

		$Vici_list = new Vici_list;

		$list_id =  null; 

		$Vici_list_email =  $Vici_list->select('email')->where('list_id','=',$list_id)->limit(300)->get();

		$bcc = "";

		foreach ($Vici_list_email as $key => $value) {
			
			$bcc = $bcc . $value['email'] . ', ';

		}
	
		return view('backend.admin.emailer.email_blast',compact('Vici_list_email','bcc'));

	}

	public function email_blast_load(Request $request){

		$Vici_list = new Vici_list;
		$Email = new Emails;
		$lead_ids = array();
		$bcc_list = array();

		$email_dummy =  $Email->select('lead_id')->get();

		foreach ($email_dummy as $key2 => $value2) {
			array_push($lead_ids, $value2['lead_id']);
		}

		$Vici_list_email =  $Vici_list
			->select('email', 'lead_id')
			->where('list_id','=',$request->input('list_id'))
			->whereNotIn('lead_id',$lead_ids)
			->limit(300)->get();

		$bcc  = null;

		$dummy_email =  $Email
			->select('email', 'lead_id')
			->where('status','=',1)
			->limit(5)->get();

		foreach ($dummy_email as $key3 => $value3) {
			
			array_push($bcc_list, $value3['email']);
		}

		foreach ($Vici_list_email as $key => $value) {
			
			if(!empty($value['email'])){

				$bcc = $bcc . $value['email'] . ', ';
				$email_data['status']= 1;
				$email_data['email']= $value['email'];
				$email_data['lead_id']= $value['lead_id'];

				$Email->create($email_data);
			}

			$msg = 'Emails has been Loaded from Vici Database!';

		}

		return $bcc_list;
	
	}

	public function email_blast_send(Request $request){

		$email['subject'] = !empty($request->input('subject')) ? $request->input('subject') : " " ;
		$to = $request->input('to');
		$email['to'] = array_filter(explode(",",$to));

		$cc = $request->input('cc');
		$email['cc'] = array_filter(explode(",",$cc));

		$bcc = $request->input('bcc');
		$email['bcc'] = array_filter(explode(",",$bcc));

		$email['from'] = $request->input('from');

		$user_id = Auth::user()->id;

		if($user_id == 78){
            $data['from'] =  "danilo.hipol@magellan-solutions";
            $data['fullname'] =  "Danilo Hipol";
        }

        if($user_id == 79){
            $data['from'] =  "Hershey.Ramos@magellan-solutions.com";
            $data['fullname'] =  "Hershey Reyes";
        }


        if($user_id == 4){
            $data['from'] =  "danilo.hipol@magellan-solutions";
            $data['fullname'] =  "Danilo Hipol";
        }

        $allowed_user = [4,78,79];

        $emailer = new Emailer;
        $email_dummy = new Emails;

        $emailer_bcc = $emailer->where('subject','=','Logi Analytics Interactive Visual Gallery')->get();

        $array_collection = array();
        $values_arr = array();

        foreach ($emailer_bcc as $key => $value) {
        	
        	$values_arr = json_decode($value['bcc']);
        		array_push($array_collection,$value );
     		
        }

        $array_collection = array_unique ($array_collection);

        $email['bcc_official'] = array();

        foreach ($email['bcc'] as $key3 => $value3) {
        	if (in_array($value3,$array_collection)){

        		
	        }else{
	        	array_push($email['bcc_official'],trim($value3));

	        }
        }


        if(in_array($user_id, $allowed_user)){

        	$status['status'] = 0;
        	$email_dummy->whereIn('email',$email['bcc_official'])->update($status);

	        Mail::send('email.iq_link_email', ['content' => $data ], function ($m) use ($email) {
	           $m->from('test@magellan-solutions.com', 'Magellan Reporting Service');
	           $m->to($email['to'])
	           ->cc($email['cc'])
	           ->bcc($email['bcc_official'])
	           ->subject($email['subject']); 
	        });

	        $email['name'] = $data['fullname'];
	        $email['status'] = 1;
	        $email['to'] = json_encode($email['to']);
	        $email['cc'] = json_encode($email['cc']);
	        $email['bcc'] = json_encode($email['bcc_official']);

	        $emailer->create($email);

	        $msg = 'Email has been Sent!';

	    }else{

			$msg = 'You are not allowed to use this Feature';
	    }
		
        flash()->success($msg);
        return redirect()->back();  

	}


}