<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Board;
use App\Model\Page;
use App\Model\Updates;
use App\Model\Logs;
use App\User;
use App\Model\Lists;
use App\Model\Vct;
use App\Model\New_campaign as Campaign;
use App\Model\ConCampaigns as CC;

class boardController extends Controller 
{

    public function __construct(Guard $auth,Updates $updates,Board $boardObj,Campaign $campaign,Page $pageObj,Request $request,CC $cc,Logs $logs,Lists $lists, Vct $vici){
        $this->boardObj = $boardObj;
        $this->campaign = $campaign;
        $this->pageObj = $pageObj;
        $this->request = $request;
        $this->auth = $auth;
        $this->updates = $updates;
        $this->name = $auth->user()->name;
        $this->concamp = $cc; 
        $this->logger = $logs; 
        $this->lists = $lists;
        $this->vici = $vici;

    }

    public function index(){

        if($this->auth->user()->user_type == 'user') return redirect()->route('reporter.index');
        
        $boards = [];

        if($this->auth->user()->user_type == 'admin'){
            $boards = $this->boardObj->where('status',1)->get()->toArray();
        }else{
            $boards_raw = $this->concamp->where('users_id',$this->auth->user()->id)->with(['board'])->get()->toArray();
            
            foreach ($boards_raw as $bk => $bv) {
               if($bv['board']['status'] == 1){
                 $boards[] = $bv['board'];
               }
              
            }
        }

        $vici = $this->vici->get()->toArray();

        $viciToTemplar[] = ""; 

        $ctr = 0; 

        foreach ($vici as $key => $value) {

            $holder = json_decode($value["templar_socket"], true);

            if(count($holder) != 1){

                foreach ($holder as $holderkey => $holdervalue) {

                    $viciToTemplar[$ctr] =  $holdervalue;  
                    $ctr ++; 
                }

            }else{

                $viciToTemplar[$ctr] =  $holder[0];

            }

            $ctr ++; 

        }

        foreach ($boards as $boardkey => $boardvalue) {

            foreach ($viciToTemplar as $vicikey => $vicivalue) {
               
                if($boardvalue['campaign_id'] == $vicivalue){
                   
                  $boards[$boardkey]["connector"] = "connected";

                }

            }
        }   

        return view('backend.admin.board.lists',compact('boards', 'viciToTemplar')); 

    }


    public function add(){
        $campaigns = $this->campaign->all();
        return view('backend.admin.board.add',compact('campaigns')); 
    }


    public function edit($board){ 
   
        $opt = mb_convert_options($board->options);
        $emails = [];

        if(!empty($board->assigned)){
            foreach ($board->assigned as $val) { 
               $emails[] = $val['email'];
            }          
        }
        $reports = (isset($opt['reports'])?$opt['reports']:array());
        $btns = (isset($opt['btns'])?$opt['btns']:array());


//pre($btns);
        return view('backend.admin.board.edit',compact('board','reports','emails','btns')); 

    }
 
    public function save(){ 

        $this->validate($this->request, [
            'campaign' => 'required',
            'lob' => 'required',
            'to' => 'required|max:255|min:5'
        ]);
        
        $post = $this->request->all();
        $post['campaign_id'] = uniqid(); 

        if($this->boardObj->create($post)){  
            $this->_assign_campaigns($this->auth->user()->id,$this->auth->user()->email,$post['campaign_id']);
            //updates
            $ups = mb_update_contents($post['lob'].' added by '.$this->name.'.');
            $ups['campaign_id'] = $post['campaign_id'];
            $this->updates->create($ups);
            $this->logger->create(['campaign_id' => $post['campaign'],'users_id' => $this->auth->user()->id,'contents' => json_encode(['added the board.'])]);
            flash()->success('new board added!');
            return redirect()->route('board.index'); 
        }       

    }  


    public function mail($board){

        $emails = [];

        return view('backend.admin.board.mail',compact('board','emails')); 

    }

    public function mail_save($board){

        $data = $this->request->all(); 
        
        if($board->options != ''){
            $options = json_decode($board->options);
        }else{
            $options = json_decode('{}');
        }

        $options->subject = (empty($data['subject'])?'Magellan Reporting':$data['subject']); //die;
        $board->options = json_encode($options); //die;
        $board->to = json_encode(validate_email($data['to'])); //die;
        $board->from = (filter_var(trim($data['from']), FILTER_VALIDATE_EMAIL)?$data['from']:'reports@magellan-solutions.com'); //die;
        $board->cc = json_encode(validate_email($data['cc'])); //die;
        $board->bcc = json_encode(validate_email($data['bcc'])); //die;
        $board->save();
        flash()->success('successfully updated!'); 
        return redirect()->back();
        
    }


    public function edit_post($board){ 

        $input = $this->request->only(['to','cc']);
        $board->to = $this->request->input('to'); 
        $board->cc = json_encode($this->request->input('cc'));
        $board->primary_page = $this->request->input('primary_page');

        $opt = mb_convert_options($board->options);
        $opt['reports'] = $this->request->input('opt');
        $opt['btns'] = $this->request->input('btn');
        $board->options = json_encode($opt);
        $board->lob = $this->request->input('lob');
        //pre($board); 
        $board->save();
 
        //updates
        $updatesCont = $this->updates->getby_campaign($board->campaign_id);
        $ups = mb_update_contents('board updated by '.$this->name.'.',$updatesCont->contents);
        $ups['campaign_id'] = $board->campaign_id;
        $this->updates->mb_update($board->campaign_id,$ups['contents'],$ups['count']);

         $this->logger->create(['campaign_id' => $board->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['updated the board.'])]);
        //$updates->create($ups);
               
        flash()->success('successfully updated!');
        return redirect('dashboard/boards/edit/'.$board->campaign_id);
    }

    public function delete($board){

        if($this->auth->user()->user_type == 'admin'){
            $board->status = 0;
            $this->logger->create(['campaign_id' => $board->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['deleted the board.'])]);
            $board->save();
            flash()->success('board successfully deleted!');
        }else{
            flash()->error('you dont have permission to delete this board');
        }
        return redirect()->back();        
    }

     private function _assign_campaigns($uid,$email,$input_camps){
        
        $reports = json_encode($this->lists->reports());
        if(!empty($input_camps)){
            if(is_array($input_camps)){
                $this->concamp->where('users_id',$uid )->delete();
                foreach ($input_camps as $ick => $icv) {
                    if(is_array($icv)){
                        $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $ick,'reports' => json_encode($icv)]);
                    }else{
                        $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $icv,'reports' => $reports]);
                    }
                } 
            }else{

               if($input_camps != '') $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $input_camps,'reports' => $reports]);
           
            }           
        }
    }   

    private function _update_campaigns($uid,$email,$input_camps){

        $this->concamp->where('users_id',$uid )->update(['reports' => '']);
        if(!empty($input_camps)){
            foreach ($input_camps as $ick => $icv) {
                $this->concamp
                    ->where('campaign_id',$ick)
                    ->where('users_id',$uid)
                    ->update(['reports' => json_encode($icv)]);
            }            
        }

    }    


} 


