<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use App\User;

class logsController extends Controller
{
    
  	public function __construct(Guard $auth,Logs $logs,User $user){
  		$this->auth = $auth;
      $this->logs = $logs;
      $this->user = $user;

  	}

    public function index()
    {
      $logs = $this->logs->where('status',1)->limit(100)->get()->toArray();

      switch ($this->auth->user()->user_type){
         case 'admin':
           $logs = $this->logs->where('status',1)->take(5)->get()->toArray();
           break;
         case 'operation':
         case 'trainer':
           $user = $this->user->where('id',$this->auth->user()->id)->with(['campaigns'])->first()->toArray();

           if(!empty($user['campaigns'])){
              foreach ($user['campaigns'] as $vcamp) {
                $campaigns[] = $vcamp['campaign_id'];
              }

              if(isset($campaigns) && !empty($campaigns)) $logs = $this->logs->where('status',1)->whereIn('campaign_id',$campaigns)->get()->toArray();
           }

           break;         
       }

      if(isset($logs) && !empty($logs)){

        foreach ($logs as $logk => $logv) {
          $u[] = $logv['users_id'];
        }
        $user_a =$this->user->whereIn('id',$u)->get()->toArray();
        foreach ($user_a as $uv) {
          $user_b[$uv['id']] = $uv['name']; 
        }
        foreach ($logs as $logv) {
          foreach ($logv as $k1 => $v1) {
            if($k1 == 'users_id'){
              $logv2[$k1] = $user_b[$v1];
            }elseif($k1 == 'contents'){
              $logv2[$k1] = '- '. implode('<br>', json_decode($v1));
            }else{
              $logv2[$k1] = $v1;
            }
          }
          $logs_final[] = $logv2;
        }
      };

      if(!isset($logs_final)) $logs_final=[];
      return view('backend.admin.logs.index',['logs' => $logs_final]);
    
    }

    public function user($logs)
    {

      $log_arr = $logs->toArray();

      if(!empty($log_arr)){
        
        foreach ($log_arr as $logk => $logv) {
          $u[] = $logv['users_id'];
        } 
        $user_a =$this->user->whereIn('id',$u)->get()->toArray();     
        foreach ($user_a as $uv) {
          $user_b[$uv['id']] = $uv['name']; 
        }
        foreach ($log_arr as $logv) {
          foreach ($logv as $k1 => $v1) {
            if($k1 == 'users_id'){
              $logv2[$k1] = $user_b[$v1];
            }elseif($k1 == 'contents'){
              $logv2[$k1] = '- '. implode('<br>', json_decode($v1));
            }else{
              $logv2[$k1] = $v1;
            }
          }
          $logs_final[] = $logv2;
        }

      }

      if(!isset($logs_final)) $logs_final=[];
       return view('backend.admin.logs.user',['logs' => $logs_final]);
    }

  	public function campaign($logs){

      $log_arr = $logs->toArray();

      if(!empty($log_arr)){

        foreach ($log_arr as $logk => $logv) {
          $u[] = $logv['users_id'];
        } 
        $user_a =$this->user->whereIn('id',$u)->get()->toArray();     
        foreach ($user_a as $uv) {
          $user_b[$uv['id']] = $uv['name']; 
        }
        foreach ($log_arr as $logv) {
          foreach ($logv as $k1 => $v1) {
            if($k1 == 'users_id'){
              $logv2[$k1] = $user_b[$v1];
            }elseif($k1 == 'contents'){
              $logv2[$k1] = '- '. implode('<br>', json_decode($v1));
            }else{
              $logv2[$k1] = $v1;
            }
          }
          $logs_final[] = $logv2;
        }

      }

      if(!isset($logs_final)) $logs_final=[];
      return view('backend.admin.logs.campaign',['logs' => $logs_final]);

  	}
    
}  