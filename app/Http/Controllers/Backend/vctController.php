<?php namespace App\Http\Controllers\Backend;


use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\VICI;
use App\Model\Board;
use App\Model\Vct;

class vctController extends Controller
{


    public function __construct(VICI $vici,Board $board,Vct $vct, Request $request){

        $this->vici = $vici;
        $this->board = $board;
        $this->r = $request;
        $this->vct = $vct;

    }

	public function index(){

		$connectors = $this->vct->get();
		return view('backend.admin.vct.index',compact('connectors','templar'));
	}

	public function show($i){ 

		$vct = $this->vct->find($i);
        if($vct->toArray()){
            $templar = $this->board->whereIn('campaign_id',json_decode($vct->templar_socket,true))->select('campaign')->get();
            foreach ($templar as $v) {
                $arr[] = $v->campaign;
            }
           $templar_socket = implode(',', $arr);
           $vici_plug = implode(',', json_decode($vct->vici_plug,true));
           return view('backend.admin.vct.show',compact('templar_socket','vici_plug'));
        }
        
	}

	public function create(){
        $this->validate($this->r, [
            'server' => 'required',
            'bound' => 'required'
        ]);
        $server = $this->r->input('server');
        $bound = $this->r->input('bound');
		$vici = $this->vici->allgroups();
		$templar = $this->board->templar_socket(); 

		return view('backend.admin.vct.add',compact('vici','templar','server','bound'));
	}	

	public function save(){
        $this->validate($this->r, [
            'vici_plug' => 'required',
            'name' => 'required',
            'templar_socket' => 'required'
        ]);

        if(!$this->vct->exist($this->r->input('vici_plug'),$this->r->input('templar_socket'),$this->r->input('server'),$this->r->input('bound'))){

            $data = [
             'vici_plug' => json_encode($this->r->input('vici_plug')),
             'templar_socket' => json_encode($this->r->input('templar_socket')),
             'name' => $this->r->input('name')
            ];
        	$this->vct->create($data);
        	flash()->success('successfully created!');
        	return redirect()->route('vct.index');         	
        }else{
        	flash()->error('connector already exist!');
        	return redirect()->back(); 
        }
        	

	} 

	public function destroy($id){
		$this->vct->destroy($id);
    	flash()->success('successfully deleted!');
    	return redirect()->back(); 		
	}

    public function select_server(){
        return view('backend.admin.vct.select-server',compact('connectors','templar'));
    }    

} 