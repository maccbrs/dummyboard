<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\CampaignData;
use App\Helpers\Tables; 
use App\Model\Vct;
use App\Model\Board;

class reporterController extends Controller
{

	public function __construct(Tables $tbl,Vct $vct){
		$this->help_tbl = $tbl;
		$this->vct = $vct;
	}

	public function index(){

		$connectors = $this->vct->get();

		return view('backend.admin.reporter.index',compact('connectors'));

	}

	public function blockage(Request $r,CampaignData $cd){

		$connectors = $this->vct->get();

		$camp= json_decode($r['camp']); 

	
	    $not_included = ['test_email','lead_id','phone_number','user','campaign_id','request_status','email_subject', 'Address','campaign_id','contents','status','active','Agent_Name','caller_id','zip_code','notes','customer_name','email','Bedroom_Size','Moving_To_Zipcode','Moving_From_Zipcode','Plan_Date_to_Mov'];

	    $included = ['Submitted_Web_Form','Disposition','caller_id','lob','date'];


		$data =	$cd->whereIn('campaign_id',['OmniContractor','OmniContractor_2','OmniContractor_3','OmniContractor_4'])->whereBetween('created_at', [$r->input('from'),$r->input('to')])->get()->toArray();
		//pre($data); 
	    $data2 = [];
	    foreach ($data as $dk => $dv) {
	    	$contents = json_decode($dv['contents']);
	    	$contents->date = $dv['created_at'];
	    	$data2[] = $contents;
	    }

	    $data3 = [];
	    $value_temp = [];
	    $keys = [];

	    foreach ($data2 as $k => $v) {
	      	foreach ($v as $k2 => $v2) {
	          	if(in_array($k2, $included)){
	          	  $key = ucfirst(strtolower(str_replace('_', ' ', $k2)));
	              $value_temp[$key] = $v2;
	              if(!in_array($key, $keys)) $keys[] = $key;
	         	}
	    	} 

		    if(!empty($value_temp)){
		    		$data3[] = $value_temp;
		    		$value_temp = [];

		    }

	    }



	    sort($keys);
	    $value = $data3;

		$email_tbl = view('email._reporter',compact('value','keys'));
		return view('backend.admin.reporter.index',compact('email_tbl','connectors','data'));

	}



}