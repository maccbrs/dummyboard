<?php namespace App\Http\Controllers\Backend;


use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\VICI;
use App\Model\Blockages;
use App\Model\Board;
use App\Model\Vct; 
use App\Model\CampaignData;


class connectorsController extends Controller
{

	public function __construct(Blockages $blk,Board $board,Request $r){
		$this->blk = $blk;
		$this->board = $board;
		$this->request = $r;
	}

	public function blockages_index(){
		$campaigns = $this->blk->get()->toArray();
		return view('backend.admin.connectors.blockages.index',compact('campaigns'));
	}

	public function blockages_create(){
		$boards = $this->board->where('status',1)->get();
		return view('backend.admin.connectors.blockages.add',compact('boards'));
	}

	public function blockages_save(){

		$all = $this->request->all();
		$post['name'] = $all['name'];
		$post['campaigns'] = json_encode($all['campaigns']);
		$this->blk->create($post);
		flash()->success('successfully created!');
		return redirect()->route('connectors.blockages.index');
		
	}
 
}