<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Eod;
use App\Model\Vct;
use App\Model\Board;
use App\Helpers\Tables;
use DB;
use Mail;

class eodgeneratorController extends Controller
{

	public function index(Eod $eod,Vct $vct){

		$lists = $vct
				->select('name','id')
				->get();
		
		return view('backend.admin.eodgenerator.index',compact('lists'));

	}

	public function show(Request $request,Vct $vct){

        $this->validate($request, [
            'campaign' => 'required',
            'from' => 'required',
            'to' => 'required'
        ]); 

        $test = false;
        $done = [];

        $html_data = '';

        $html_logs = '';
        $from = $request->input('from');
        $to = $request->input('to');

       $camp = $vct->where('id',$request->input('campaign'))->first()->toArray();

       $dates = mb_two_dates();
      
        switch ($camp['server']) {
            case 'server 1':
                $conn = 'vicidial';
                $from = date("Y-m-d H:i",strtotime("+8 hours",strtotime($from)));
                $to = date("Y-m-d H:i",strtotime("+8 hours",strtotime($to)));
               break;
                 
            default:
                $conn = 'vicidial2';
                break;
                
        }

        switch ($camp['bound']) {
            case 'out':
                $table = 'vicidial_log';
            break;

                    default:
                $table = 'vicidial_closer_log';
                break;
        }
      //  pre($to); 
        //pre(json_decode($camp['templar_socket'],true));

        $boards = Board::whereIn('campaign_id',json_decode($camp['templar_socket'],true))->where('status',1)->get()->toArray();
       $vici = DB::connection($conn)->table($table);
       $in = [];
       $in = json_decode($camp['vici_plug'],true);

       if(!empty($in)){
           $logs = $vici->select('status',DB::raw('count(*) as count'))
                   ->whereBetween('call_date', [$from, $to])
                   ->whereIn('campaign_id',$in)
                   ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                   ->groupBy('status')
                   ->orderBy('status', 'asc')
                   ->get(); 
            if(!empty($logs)){
               $html_logs = Tables::html_logs2($logs);   
            }                          

       }
  
       
       $data = DB::table('campaign_data')
               ->whereIn('campaign_id',json_decode($camp['templar_socket'],true))
               ->where('contents', 'like', '%"request_status":"live"%')
               ->whereBetween('created_at', [$from,$to])
               ->get();
              // pre($data);
       if(!empty($data)){
           $html_data = Tables::html_data2($data);
       } 
            


		return view('backend.admin.eodgenerator.show',compact('html_logs','html_data'));
	}

    public function send(Request $request){
        
        $this->validate($request, [
            'from' => 'required',
            'to' => 'required'
        ]); 

        $html_data = $request->input('data');
        $html_logs = $request->input('logs');
        $board['subject'] = ($request->input('subject')?$request->input('subject'):'reports@magellan-solutions.com');
        $board['from'] = ($request->input('from') != ''?$request->input('from'):false);
        $board['to'] = ($request->input('to') != ''?array_map('trim',explode(',', $request->input('to'))):false);
        $board['cc'] = ($request->input('cc') != ''?array_map('trim',explode(',', $request->input('cc'))):false);
        $board['bcc'] = ($request->input('bcc') != ''?array_map('trim',explode(',', $request->input('bcc'))):false);

        Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 

           $m->from($board['from'], $board['subject']);
           $m->to($board['to'])->subject($board['subject']); 
           if($board['cc'] && !$board['bcc']){
             $m->to($board['to'])->cc($board['cc'])->subject($board['subject']); 
           }else if(!$board['cc'] && $board['bcc']){
             $m->to($board['to'])->bcc($board['bcc'])->subject($board['subject']); 
           }else if($board['cc'] && $board['bcc']){
             $m->to($board['to'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
           }
           
        }); 

        flash()->success('Eod Reports successfully sent!');
        return redirect()->route('eod-generator.index');

    }

}  