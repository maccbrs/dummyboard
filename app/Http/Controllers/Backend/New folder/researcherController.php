<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\ClientPortalContent;
use App\User;
use Auth;

class researcherController extends Controller
{

	public function index(){

		$user = new User;

		$researchers = $user->select('name','id')->where('user_type','researcher')->get(); 

		return view('backend.admin.researcher.index',compact('connectors','researchers'));

	}

	public function edit(){

		$user_id = Auth::user()->id;

		$options = Auth::user()->options;
		$user_type = Auth::user()->user_type;

		if($user_type == 'admin'){

			$assigned_dispo = ['FU'];

		}else{

			$assigned_dispo = json_decode($options); 

		}

		$client_portal = new ClientPortalContent;

		$data = $client_portal->whereIn('dispo',$assigned_dispo)->whereIn('updated',[$user_id , '1'])->orderBy('id','desc')->get();

		foreach ($data as $key => $value) {

			$data[$key]['content2'] = json_decode($value['content']);

		}

		return view('backend.admin.researcher.edit',compact('data','user_id'));

	}

	public function assignment(Request $r){

		$user = new User;

		$data['options'] = json_encode($r->input('dispo'));
		$data['user_type'] = $r->input('user_type'); 

		$user->where('id','=',$r->input('id'))->update($data);

		return redirect()->back();  

	}

	public function update(Request $r){

		$user_id = Auth::user()->id;

		$client_portal = new ClientPortalContent;

		$data['content'] = json_encode($r->input('content'));
		$data['updated'] = $r->input('updated');

		$data = $client_portal
			->where('id',$r->input('id'))
			->whereIn('updated',[1,$user_id])
			->update($data);

		$msg = 'Data has been Updated';
		flash()->success($msg);

		return redirect()->back();  

	}

}