<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Model\Board;
use App\Model\Updates;
use App\Model\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use Route;


class pageController extends Controller
{

    public function __construct(Logs $logs,Guard $auth){
        $this->auth = $auth;
        $this->logger = $logs;
    }

	public function add($board){

		return view('backend.admin.page.add',compact('board'));
	}

	public function create(Request $request,Page $page,Updates $updates){
		 
        $this->validate($request, [
            'campaign_id' => 'required',
            'title' => 'required|max:100',
            'template' => 'required'
        ]); 

        $input = $request->only(['campaign_id','title','template','board_id']);
        
        if($page->create($input)){

            $updatesCont = $updates->getby_campaign($input['campaign_id']);
            $contents_a = (isset($updatesCont->contents)?$updatesCont->contents:'');
            $ups = mb_update_contents($input['title'].' page created',$contents_a);
            $ups['campaign_id'] = $input['campaign_id'];

            $updates->mb_update($input['campaign_id'],$ups['contents'],$ups['count']);
            $this->logger->create(['campaign_id' => $input['campaign_id'],'users_id' => $this->auth->user()->id,'contents' => json_encode(['created '.$input['title'].' page.'])]);

            flash()->success('page successfully created!');
            return redirect('dashboard/boards/edit/'.$input['campaign_id']); 
        }

	}

    public function edit($page){

        return view('backend.admin.page.edit',compact('page'));

    } 

    public function update(Request $request,$page,Updates $updates){ 
        
        $pagecontent = array();

        if($page->contents != ''){
            $pagecontent_obj = json_decode($page->contents);
            foreach ($pagecontent_obj as $k=> $v) {

                $pagecontent[$k] = $v;
               
            }
        }

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $name = $file->getClientOriginalName(); 
            $pagecontent['logo'] = $name;
            $file->move('uploads',$name);
        }

        $pagecontent['content-r'] = $request->input('content-r');
        $pagecontent['content-l'] = $request->input('content-l');
        $pagecontent['content-2'] = $request->input('content-2');
        $pagecontent['content-3'] = $request->input('content-3');
        $pagecontent['content-4'] = $request->input('content-4');


        if($page->template == 'customform'):
            $pagecontent['form-main'] = $request->input('form-main');
            $pagecontent['email-subject'] = $request->input('email-subject');
            $pagecontent['email-from'] = $request->input('email-from');
            $pagecontent['email-to'] = $request->input('email-to');
            $pagecontent['email-cc'] = $request->input('email-cc');
        endif;
        

        $page->contents = json_encode($pagecontent);
		$page->custom_head = $request->input('custom_head');

        if($request->input('title') != '') $page->title = $request->input('title');
        
        if($request->input('email_subject') != ''){
            switch ($page->options) {
                 case '':
                 case '[]':
                 case '[""]':
                 $options = json_decode('{}');
                     break;
                default:
                 $options = json_decode($page->options);
                    break;
            } 

            $options->email_subject = $request->input('email_subject');
            $page->options = json_encode($options);
        }

        $page->save();

        //updates
        $updatesCont = $updates->getby_campaign($page->campaign_id);
        $contents_a = (isset($updatesCont->contents)?$updatesCont->contents:'');
        $ups = mb_update_contents($page->title.' page updated',$contents_a);
        $ups['campaign_id'] = $page->campaign_id;

        $updates->mb_update($page->campaign_id,$ups['contents'],$ups['count']);
        $this->logger->create(['campaign_id' => $page->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['updated '.$page->title.' page.'])]);

        flash()->success('successfully updated!');

        return redirect('dashboard/page/edit/'.$page->id);

    }  


    public function update2(Request $request,$page,Updates $updates){ 

        $data = json_decode($request->all,true);
        $client = new \GuzzleHttp\Client;
        $client->get('http://192.168.200.111/agc/api.php?source=test&user='.$data['user'].'&pass='.$data['pass'].'&agent_user='.$data['user'].'&function=external_hangup&value=1');
        $client->get('http://192.168.200.111/agc/api.php?source=test&user='.$data['user'].'&pass='.$data['pass'].'&agent_user='.$data['user'].'&function=external_status&value=TEST');
        echo $res->getStatusCode(); // 200

        die;

    }



    public function delete($page){

        $page->status = 0;
        $page->save();
        $this->logger->create(['campaign_id' => $page->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['delete '.$page->title.' page.'])]);
        flash()->success('page successfully deleted!');
        return redirect()->back();

    }  

    public function test(){



    }


}