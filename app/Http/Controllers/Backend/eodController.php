<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Eod;
use DB;

class eodController extends Controller
{

	public function index(Eod $eod){

		$lists = $eod->select(DB::raw('count(*) as count, date'))->groupBy('date')->orderBy('date','desc')->paginate(13);
		return view('backend.admin.eod.index',compact('lists'));

	}

	public function date($lists){
		return view('backend.admin.eod.date',compact('lists'));
	}

	public function show($data){
		 
		return view('backend.admin.eod.show',compact('data'));
	}

}