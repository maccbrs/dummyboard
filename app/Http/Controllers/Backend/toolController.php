<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Tables; 
use App\Model\Blockages;
use App\Model\Logs;
use App\Model\LogsContent; 
use App\Model\CampaignData;
use DB;
use Auth;


class toolController extends Controller
{

	public function __construct(Tables $tbl,Blockages $blk){
		$this->help_tbl = $tbl;
		$this->blk = $blk;
	}

	public function index(){

		$dispo = DB::connection('vicidial')
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status')
			->get();

		$campaigns = $this->blk->get()->toArray(); 
		return view('backend.admin.tool.index',compact('campaigns','dispo'));
	}

	public function crc_1010(){

		$dispo = DB::connection('192.168.200.77')
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status')
			->get();

		$campaigns = $this->blk->get()->toArray(); 
		return view('backend.admin.tool.crc1010',compact('campaigns','dispo'));
	}

	public function change_data(){

		$campaignlist = DB::connection('mysqlive')
			->table('published_boards')
			->select('campaign_id','campaign')
			->groupBy('campaign_id')
			->orderBy('campaign')
			->get();

		return view('backend.admin.tool.change_data',compact('campaignlist','dispo'));
	}

	public function update_data(Request $r){

		$campaignlist = DB::connection('mysqlive')
			->table('published_boards')
			->select('campaign_id','campaign')
			->groupBy('campaign_id')
			->orderBy('campaign')
			->get();

		$campaign_data = DB::connection('mysqlive')
			->table('campaign_data')
			->select('id','contents','campaign_id','created_at')
			->wherein('campaign_id',[$r->input("campaign")])
			->whereBetween('created_at',[$r->input("from"),$r->input("to")])
			->get();

		$to = $r->input('to');
		$from = $r->input('from');

		$campaign_content = array(); 
 		$campaign_holder = array(); 
 		$head_holder = array(); 

		foreach ($campaign_data as $key => $value) {

			$content = (array)json_decode($value->contents);
			$campaign_holder = (array)$campaign_data[$key];
			$campaign_content[] = array_merge($campaign_holder ,$content);

		}

		$campaign_name = $r->input('campaign');

		$content_head = array_unique($head_holder); 

		return view('backend.admin.tool.change_data',compact('campaignlist','dispo','campaign_content','campaign_name','to','from'));
	}


	public function update_content(Request $r,Guard $auth){

		$campaignlist = DB::connection('mysqlive')
			->table('published_boards')
			->select('campaign_id','campaign')
			->groupBy('campaign_id')
			->orderBy('campaign')
			->get();

		$campaign_data = DB::connection('mysqlive')
			->table('campaign_data')
			->select('id','contents','campaign_id','created_at')
			->wherein('campaign_id',[$r->input("campaign_id")])
			->whereBetween('created_at',[$r->input("from"),$r->input("to")])
			->get();

		

		$campaign_content = array(); 
 		$campaign_holder = array(); 
 		$head_holder = array(); 

		foreach ($campaign_data as $key => $value) {

			$content = (array)json_decode($value->contents);
			$campaign_holder = (array)$campaign_data[$key];
			$campaign_content[] = array_merge($campaign_holder ,$content);

		}

		if(!empty($r->input())){

			$userId = Auth::id();

			$logs_content = new LogsContent;
			$logs_content['prev_content'] = $r->input('prev_content');
			$logs_content['campaign_id'] = $r->input('campaign_id');
			$logs_content['user_id'] = $userId;
			$logs_content['status'] = 1;
			$logs_content->save();

			if(!empty($r->input('id'))){

				DB::table('campaign_data')
					->where('id',$r->input('id'))
					->update(array('contents' => json_encode($r->input('content'))));

				$msg = 'The Process has been saved to Logs and the content has been updated.';

			}else{

				 $msg = 'Check your input. Process cancelled';
			}
		
		}

		$campaign_name = $r->input('campaign_id');
		$to = $r->input('to');
		$from = $r->input('from');

		flash()->success($msg);

		return view('backend.admin.tool.change_data',compact('campaignlist','dispo','campaign_name','campaign_content','to','from'));
	}


	public function blockage(Request $r){

		$email_tbl = "";
		$campaigns = "";

		$startdate = date("Y-m-d H:i:s", strtotime($r->calldate)); 

		$endTime = date("Y-m-d H:i:s", strtotime($startdate) + (60));

		$conn = 'vicidial';
                
    	$table = 'vicidial_log';

    	$table2 = 'vicidial_closer_log';
	  
        $vici = DB::connection($conn)->table($table);

        $vici2 = DB::connection($conn)->table($table2);

        if(!empty($r->phonenumber)){

	        $logs = (!empty($r->disposition)? $vici->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici->where('phone_number', $r->phonenumber));
	        $logs = $logs->get(); 
 
	        $logs_closer = (!empty($r->disposition)? $vici2->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici2->where('phone_number', $r->phonenumber));
	        $logs_closer = $logs_closer->get();
	       	
	    }else{

	        $logs = (!empty($r->disposition)? $vici->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici->whereBetween('call_date', [$startdate, $endTime]));
	        $logs = $logs->get(); 

	        $logs_closer = (!empty($r->disposition)? $vici2->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici2->whereBetween('call_date', [$startdate, $endTime]));
	        $logs_closer = $logs_closer->get();

	    }

	        if(empty($logs) && empty($logs2) && empty($logs_closer) && empty($logs_closer2)){

	        	flash()->error('No Calls Found');

	    	}else{

	    		flash()->success('Call Log Successfully Found');

	    	}

	    	$repository = $r->repository;

	    	if(!empty($r->phonenumber)){

	    		$details =  $r->phonenumber;
	    		$label = "Phone Number : ";

	    	}else{

	    		$details =  $startdate . " - " . $endTime; 
	    		$label = "Date Range : ";

	    	}

			$email_tbl = view('email._crc',compact('logs','logs_closer','logs2','logs_closer2', 'conn', 'repository', 'details', 'phonenumber', 'label','conn2'));

			$campaigns = $this->blk->get()->toArray(); 

		$phonenumber = $r->phonenumber; 
		$calldate = $r->call_date;
		$server = $r->repository;

		$dispo = DB::connection('vicidial')
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status')
			->get();

		return view('backend.admin.tool.index',compact('email_tbl','campaigns','server','calldate', 'phonenumber','dispo'));

	}

	public function blockage1010(Request $r){

		$email_tbl = "";
		$campaigns = "";

		$startdate = date("Y-m-d H:i:s", strtotime($r->calldate)); 

		$endTime = date("Y-m-d H:i:s", strtotime($startdate) + (60));

		$conn = '192.168.200.77';
                
    	$table = 'vicidial_log';

    	$table2 = 'vicidial_closer_log';
	  
        $vici = DB::connection($conn)->table($table);

        $vici2 = DB::connection($conn)->table($table2);

        if(!empty($r->phonenumber)){

	        $logs = (!empty($r->disposition)? $vici->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici->where('phone_number', $r->phonenumber));
	        $logs = $logs->get(); 
 
	        $logs_closer = (!empty($r->disposition)? $vici2->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici2->where('phone_number', $r->phonenumber));
	        $logs_closer = $logs_closer->get();
	       	
	    }else{

	        $logs = (!empty($r->disposition)? $vici->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici->whereBetween('call_date', [$startdate, $endTime]));
	        $logs = $logs->get(); 

	        $logs_closer = (!empty($r->disposition)? $vici2->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici2->whereBetween('call_date', [$startdate, $endTime]));
	        $logs_closer = $logs_closer->get();

	    }

	        if(empty($logs) && empty($logs2) && empty($logs_closer) && empty($logs_closer2)){

	        	flash()->error('No Calls Found');

	    	}else{

	    		flash()->success('Call Log Successfully Found');

	    	}

	    	$repository = $r->repository;

	    	if(!empty($r->phonenumber)){

	    		$details =  $r->phonenumber;
	    		$label = "Phone Number : ";

	    	}else{

	    		$details =  $startdate . " - " . $endTime; 
	    		$label = "Date Range : ";

	    	}

			$email_tbl = view('email._crc',compact('logs','logs_closer','logs2','logs_closer2', 'conn', 'repository', 'details', 'phonenumber', 'label','conn2'));

			$campaigns = $this->blk->get()->toArray(); 

		$phonenumber = $r->phonenumber; 
		$calldate = $r->call_date;
		$server = $r->repository;

		$dispo = DB::connection('192.168.200.77')
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status')
			->get();

		return view('backend.admin.tool.crc1010',compact('email_tbl','campaigns','server','calldate', 'phonenumber','dispo'));

	}

	public function update(Request $request ){

		$table = $request['table'];

	    $conn = $request['server'];

		$vici = DB::connection($conn)->table($table);

		$vici->where('uniqueid',$request['uniqueid'])->update(['status' => $request['disposition']]);

		flash()->success('Disposition has been succesfully updated!');

        return redirect()->route('tool.index');

	}

	public function update1010(Request $request ){


		$table = $request['table'];

	    $conn = '192.168.200.77';

		$vici = DB::connection($conn)->table($table);

		$vici->where('uniqueid',$request['uniqueid'])->update(['status' => $request['disposition']]);

		flash()->success('Disposition has been succesfully updated!');

        return redirect()->route('tool.index');

	}
}