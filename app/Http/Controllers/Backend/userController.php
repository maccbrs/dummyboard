<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Board;
use App\Model\Page;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\ConCampaigns as CC;
use App\Model\Lists;
use App\Model\Logs;

class userController extends Controller
{ 

    public function __construct(Guard $auth,Request $request,Board $board,CC $cc,Lists $lists,User $user,Logs $logs){
        $this->request = $request;
        $this->auth = $auth;
        //$this->name = $auth->user()->name;
        $this->options = $auth->user()->options;
        $this->board = $board;
        $this->concamp = $cc;
        $this->lists = $lists;
        $this->user = $user; 
        $this->logger = $logs;
    }

	public function index(User $user){
		$users = $user->all();
		return view('backend.admin.users.lists',compact('users')); 
	}

	public function add(){
		return view('backend.admin.users.add'); 
	}

	public function create(Request $request,User $users){
		
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|max:255|unique:users,email',
            'password' => 'required|max:50',
            'repeat_password' => 'required|same:password'
        ]);	

        $input = $request->only(['email','name','user_type']);

        $input['password'] = bcrypt($request->input('password'));

        if($users->create($input)){
            $this->logger->create(['campaign_id' => '','users_id' => $this->auth->user()->id,'contents' => json_encode(['created user '.$input['name'].'.'])]);
            flash()->success('user successfully added!');
            return redirect('dashboard/user');
        }

	}

	public function update($user,Board $boardObj){

        $boards = $boardObj->all();
		return view('backend.admin.users.edit',compact('user','boards')); 

	}

	public function save(Request $request,$user){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|max:255',
            'user_type' => 'required'
        ]);	

        $opt = array();
        
        if($user->options != ''){
            $opt_arr = json_decode($user->options);
            if($opt_arr){
                foreach ($opt_arr as $k => $v) {
                   $opt[$k] = $v;
                }
            }
        }

        //assign campaigns
        $this->_assign_campaigns($user->id,$user->email,$request->input('campaign'));

        $user->options = json_encode($opt);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->user_type = $request->input('user_type');

        if($user->save()){
            $this->logger->create(['campaign_id' => '','users_id' => $this->auth->user()->id,'contents' => json_encode(['updated user '.$user->name.'.'])]);
            flash()->success('user successfully updated!');
            return redirect('dashboard/user');
        } 

	}


    public function delete($user){
        if($user->delete()){
            $this->logger->create(['campaign_id' => '','users_id' => $this->auth->user()->id,'contents' => json_encode(['updated user .'])]);
            flash()->success('user successfully deleted!');
            return redirect('dashboard/user');
        }
    }	

    public function reporting(){
//echo $this->auth->user()->id;die;
        $options = [];
        $sets = [];
        $options = $this->_campaigns();
        $board_name = $this->board->whereIn('campaign_id',array_keys($options))->lists('campaign','campaign_id')->toArray();
        $sets = $this->_sets($options);
        // pre($sets);
        return view('backend.admin.users.settings_report',compact('options','sets','board_name')); 

    }

    public function reporting_post(){

        $this->_update_campaigns($this->auth->user()->id,$this->auth->user()->email,$this->request->input('opt'));
        flash()->success('successfully updated!');
        return redirect()->back();

    }

    private function _assign_campaigns($uid,$email,$input_camps){
        $this->concamp->where('users_id',$uid )->delete();
        $reports = json_encode($this->lists->reports());
        if(!empty($input_camps)){
            foreach ($input_camps as $ick => $icv) {
                if(is_array($icv)){
                    $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $ick,'reports' => json_encode($icv)]);
                }else{
                    $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $icv,'reports' => $reports]);
                }
            }            
        }
    }

    private function _update_campaigns($uid,$email,$input_camps){

        $this->concamp->where('users_id',$uid )->update(['reports' => '']);
        if(!empty($input_camps)){
            foreach ($input_camps as $ick => $icv) {
                $this->concamp
                    ->where('campaign_id',$ick)
                    ->where('users_id',$uid)
                    ->update(['reports' => json_encode($icv)]);
            }            
        }

    }

    private function _campaigns(){
       $c_raw = $this->concamp->where('users_id',$this->auth->user()->id)->get()->toArray();
       $data = [];
       foreach ($c_raw as $crv) {
           $data[$crv['campaign_id']] = json_decode($crv['reports']);
       }
       return $data;        
    }

    public function _sets($options){
        $sets = [];
        if(!empty($options)){
            $sets_raw = $this->board->select('options','campaign_id')->whereIn('campaign_id', array_keys($options))->get()->toArray();
            foreach ($sets_raw as  $srv) {
                $sets[$srv['campaign_id']] = json_decode($srv['options']);
            }
        }  
        return $sets;      
    }

    public function _setsb($options){
        $sets = [];
        if(!empty($options)){
            $sets_raw = $this->board->select('options','campaign_id')->whereIn('campaign_id', array_keys($options))->get()->toArray();
            foreach ($sets_raw as  $srv) {
                $sets[$srv['campaign_id']] = json_decode($srv['options']);
            }
        }  
        return $sets;      
    }    

    public function resetPassword(Request $request){

        $user = User::where('id', $request->id)->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->back();
        
    }		
}