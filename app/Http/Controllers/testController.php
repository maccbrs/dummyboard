<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Updates;
use App\Model\Board;
use App\Model\VICI;
use App\Helpers\Tables;
use App\Model\CampaignData;
use App\Model\Logs;


class testController extends Controller
{

    public function __construct(Guard $auth,Board $board,Updates $updates,VICI $vici){
        $this->auth = $auth;
        $this->board = $board;
        $this->updates = $updates;
        $this->vici = $vici;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   pre('sss');
        return view('test');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function test_mail(){

        $user = array(
            'name' => 'marlon bernal',
            'email' => 'marlon.bernal@magellan-solutions.com',
            'message' => 'Hi!'
            );


        Mail::send('templates.email', ['user' => $user], function ($m) use ($user) {
            $m->from('reporter@magellan-solutions.com', 'Magellan Reporting Service');

            $m->to($user['email'], $user['name'])->subject('Your Reminder!');
        });

    }

    public function test_one($dataObj,Tables $tables){
        
        $c = $tables->report_data_tbl($dataObj->toArray());
        // $contents = [];
        // $keys_temp = [];
        // foreach ($data as $dv) {
        //    $temp_data = mb_clean_campaign_data($dv['contents']);
        //    $contents[] = $temp_data;
        //    $keys_temp = array_merge($keys_temp,array_keys($temp_data));
        // }

        // $keys = array_unique($keys_temp);

        // $value = [];
        // foreach ($contents as $ck => $cv) {
        //     $tempvalue = [];
        //     foreach ($keys as $key) {
        //         $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
        //     }
        //     $value[] = $tempvalue;
        // }

        // $test = view('email._table',compact('value'));
        echo $c;die;
        // $keys = array_keys($contents);
        //pre($value);
        //pre(array_unique($keys));
    } 


    public function test_template(){

        $x = $this->vici->ingroups();
        pre($x);
         //$users = DB::connection('mysql2')->select('select * from dummy_pages');
        // pre($users);
        //return view('templates.first.index');
    }

    public function test(){


         $user = "kit";

            Mail::send('email.report', ['user' => $user], function ($m) use ($user) {
                $m->from('howell.calabia@magellan-solutions.com', 'Your Application');

                $m->to('marlon@mailinator.com', 'kit')->subject('Your Reminder!');
            });
                   


    }


    public function test2(){

            $boards = Board::where('status',1)->get();
            $dates = mb_two_dates();

            foreach ($boards as $board) {
                $logs = [];
                //$cc = $this->_assigned_cc($board->assigned,$board->cc,'daily');
                $cc = [];
                if(filter_var($board->to, FILTER_VALIDATE_EMAIL)){
                    if(in_array('daily', mb_parse_report_options($board->options))){
                        $c_data = CampaignData::where('campaign_id',$board->campaign_id)
                        ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
                        ->get()->toArray();
                        $email_tbl = Tables::report_data_tbl($c_data);

                        // Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board,$cc) {
                        //    if($cc != '' || empty($cc)){
                        //        $m->to($board->to, $board->to)->cc($cc)->subject('Magellan Reporting Service'); 
                        //    }else{
                        //        $m->to($board->to, $board->to)->subject('Magellan Reporting Service test cron'); 
                        //    }
                        // }); 

                        $logs[] = 'Daily report sent to '.$board->lob. 'with email address of '.$board->to;

                    }else{ $logs[] = $board->lob.' daily report disabled';}  

                }else{
                    $logs[] = 'Daily report failed to send to'.$board->lob. 'with email address of '.$board->to;
                }
                if(!empty($logs)){
                    Logs::create(['campaign_id' => $board->campaign_id, 'contents'=>json_encode($logs)]);
                }            
            } 

    }

    public function test3(){

            $boards = Board::where('status',1)->get();
            $dates = mb_two_dates();

            foreach ($boards as $board) {

                    $vct = DB::table('vici_to_templar')->where('templar_socket',$board->campaign_id)->get();
                    $in = [];
                    foreach ($vct as $val) {
                        $in[] = $val->vici_plug;
                    }                    

                    if(in_array('daily', mb_parse_report_options($board['options']))){
                        $c_data = CampaignData::where('campaign_id',$board['campaign_id'])
                        ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
                        ->get()->toArray();
                        if(!empty($c_data)){
                            $email_tbl = Tables::report_data_tbl($c_data);

                            $board->subject = (isset($opt->subject)?$opt->subject:'Magellan Reporting Service');
                            $board->to = (!empty($board->to)?emailer_parser2($board->to):'');
                            $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
                            $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):'');
                            $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):'');

                            if(!empty($in)){
                                pre($in,false);
                                $logs = VICI::vici_call_logs($dates['today'],$dates['yesterday'],$in);
                                $dispo = Tables::dispo_data_tbl($logs);                        
                            }

                            $content = $email_tbl;
                            if($board->to != ''){
                                Mail::send('email.report', ['content' => $email_tbl,'dispo' => $dispo, 'board' => $board], function ($m) use ($board) { 

                                   $m->from($board->from, $board->subject);
                                   $m->to($board->to, $board->lob)->subject($board->subject); 

                                   if(!empty($board->cc) && empty($board->bcc)){
                                     $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                                   }else if(empty($board->cc) && !empty($board->bcc)){
                                     $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                                   }else if(!empty($board->cc) && !empty($board->bcc)){
                                     $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                                   }
                                   
                                }); 
                            }else{
                                $board->to = 'NOC@magellan-solutions.com';
                                Mail::send('email.report', ['content' => $email_tbl,'dispo' => $dispo, 'board' => $board], function ($m) use ($board) { 

                                   $m->from($board->from, $board->subject);
                                   $m->to($board->to, $board->lob)->subject('No Recipient eod!'); 
                                   
                                });                                 
                            } 
                        }
                    }
   
            } 


        }



}
