<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CampaignData;
use App\Model\ClientPortalContent;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App\Helpers\Tables;
use App\Model\Board;
use App\Model\Logs; 
use App\Model\VICI;
use App\Model\Catcher;
use DateTime; 
use DateTimeZone;
use Config;

class dataController extends Controller
{

    public function __construct(Tables $help_tbl,Board $board,Logs $logs,VICI $vici){ 

        $this->help_tbl = $help_tbl;
        $this->board = $board;
        $this->logs = $logs;
        $this->vici = $vici;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        //
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Catcher $catch,CampaignData $cdata, Request $request,$board)
    { 
        
   
        $test=false;
        $cid = $board->campaign_id;
        $lob = $board->lob;
        $all = json_encode($request->input('all'));
        $catch->insert(['content' => $all]);
        $leadid = $request->input('hidden_lead_id');
        $phone = $request->input('hidden_phone_number');
        $user = $request->input('hidden_user');
        if($request->input('start_time') != '' && $request->input('end_time') != ''){

            $date = date('Y-m-d');
           // $starttime = date('Y-m-d ', strtotime($date)).date('H:i:s', strtotime($request->input('start_time')));
            $starttime = $request->input('start_time');
            //$endtime = date('Y-m-d H:i:s');
           // $endtime = date('Y-m-d ', strtotime($date)).date('H:i:s', strtotime($request->input('end_time')));
            $endtime = $request->input('end_time');
            $request->merge(['start_time' => $starttime]);
            $request->merge(['end_time' => $endtime]);
          

        }

        if($request->input('start_time')  != ''){
             $date = date('Y-m-d');
             $starttime = $request->input('start_time');
        }
        $input = $request->except(['_method','_token','hidden_phone_number','hidden_lead_id','hidden_user','email_subject','all','pageid','email_to']);
       
        $input['lead_id'] = $leadid;
        $input['phone_number'] = $phone; 
        $input['user'] = $user;
        $input['lob'] = $lob;
        $input['campaign_id'] = $cid;
     
        $data = array(
           'user' => $user,
           'lead_id' => $leadid,
           'phone_number' => $phone,
           'campaign_id' =>  $board->campaign_id,  
           'lob' => $board->lob,
           'contents' => json_encode($input),
           'pageid' => $request->input('pageid')
        );         

        $ivr = array("IVRMagellan", "MESReservation", "CSPMagellan", "MESOrdertaking", "57212dcfdec45", "MESInbound");

        $bookwormAU = array("571e4d7153706", "571e4e0b94b44", "57b51a7f5ae78");

        $bookwormUS = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b","57101c7d2183d","57101d50b098e","57101d9db2f39","57101cfd6a2e3","5714fdb2667d1","571a63ea42803","571a5ac93a0f4");
       
        //check if test
        $url_arr = explode('/', str_replace(url('/').'/', '', $request->server('HTTP_REFERER')));
        if (isset($url_arr[0])&& $url_arr[0] == 'preview') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'train') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'live') { $data['active'] = 3; $test = false;}

        $fire_hazard = array('5b3e50f9a1849','5b3e7384d21e4','5b3e7669d2c4b','5b3e789bd638d');

        if($board->campaign_id != '5b58dc9911d9e'){

        $checker = $cdata
            ->where('contents','=',$data['contents'])
            ->where('campaign_id','=',$data['campaign_id'])
            ->where('lob','=',$data['lob'])
            ->first();

        if(!empty($checker)){
            $msg = 'Duplicate Data Detected, Inform your TL/IT about this case';
            flash()->error($msg);
            return redirect()->back();
        }

        }

        if($board->campaign_id == '5b58dc9911d9e'){
            $save = $cdata->create($data);
            return redirect()->back();  
          
        }else{


        if($cdata->create($data)){ 

            $reports = mb_parse_report_options($board->options);
           
            $opt = json_decode($board->options);

            $from = $board->from;

            $msg = 'data saved successfully!';

            if($board->campaign_id == '5b24160404730' || $board->campaign_id == '5bac72b936c5b'){
                $email_tbl = $this->help_tbl->email_data_tbl_horizontal($input,$opt);
            }
            
            else{
                 $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);
            }
                                
                
            if($test){

        
                if(in_array($cid, $bookwormAU)){
                    $content = json_decode($data['contents'], TRUE);

                    if(!empty($content['Call_Type'])){
                    
                        if($content['Call_Type'] != "Customer Service"){
                            
                            $board->to = json_encode(['operations@magellan-solutions.com','reports@magellan-solutions.com','tristanoliver.stamaria@magellan-solutions.com',
                                'webdev@magellan-solutions.com','hadji.anastacio@magellan-solutions.com',
                                'gerardo.resano@magellan-solutions.com','graham.matias@magellan-solutions.com','catherine.pasag@magellan-solutions.com','rodrigo.arbasto@magellan-solutions.com'
                                ],true);
                        } 
                    }
                    
                }
                
                if($request->input('test_email') == ''){

                    $msg = 'data saved successfully, email not sent, no recipient!';

                }else{

                    $board->to = $request->input('test_email');
                    Mail::send('email.report', ['content' => $email_tbl, 'board_data' => $board], function ($m) use ($board) {
                       $m->from('test@magellan-solutions.com', 'Magellan Reporting Service');
                       $m->to($board->to, $board->lob)->subject('Test Reporting Service'); 
                    });
                    $msg = 'data saved successfully, email sent to '.$request->input('test_email').'!';

                }

            }else{ 

                if($request->input('email_address') != ""){

                    $board->reply_to = $request->input('email_address');

                }else{

                    $board->reply_to = (!empty($from)?$from:'operationstest@magellan-solutions.com' );
                }

                if($request->input('email_to') != ""){

                    $board->to = $request->input('email_to');

                }else{

                    $board->to = (!empty($board->to)?emailer_parser2($board->to):['noc@magellan-solutions.com']);
                }
                  
                $requestreplyto = $request->input('email_address'); 
                 
                if($request->input('email_subject') != ""){

                    if($board->campaign_id == '5bb2c5a9aa548' || $board->campaign_id == '5bb2c5e06d149'){

                       // if($data['pageid'] == '1684' || $data['pageid'] == '1686'){
                            $board->subject = $request->input('email_subject').' ('.$request->input('Customer_Name').')';
                        // }else{
                        //     $board->subject = $request->input('email_subject');
                        // }

                    }else{
                        $board->subject = $request->input('email_subject');

                    }

                }else{

                    $board->subject = (!empty($opt->subject)?$opt->subject:'Magellan Reporting Service' );
                }

                $requestsubject = $request->input('email_subject');


                $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
                $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):[]);
                $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):[]);

                if(in_array('onsubmit', $reports)){
                
                    Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board, $requestsubject, $requestreplyto) {
                       $m->from($board->from);
                       $m->to($board->to)->subject($board->subject)->replyTo($board->reply_to); 

                       if(empty($requestsubject)){
                            
                            $options = json_decode($board->options, true); 
                            $subject = $options["subject"];
                            $board->subject = $subject; 
                            $m->to($board->to)->subject($subject);  

                        }

                       if(empty($requestreplyto)){

                        if($board->reply_to == 'teamonitrix@magellan-solutions.com')
                            {
                                $m->to($board->to)->subject($board->subject);  
                            }
                        else
                            {
                                // $subject = "teamonitrix@magellan-solutions.com";
                                // $board->reply_to = $subject; 
                                $m->to($board->to)->subject($board->subject)->replyTo($board->reply_to); 
                            }

                        }

                       if(!empty($board->cc) && empty($board->bcc)){
                         $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                       }else if(empty($board->cc) && !empty($board->bcc)){
                         $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                       }else if(!empty($board->cc) && !empty($board->bcc)){
                         $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                       }

                        
                    });
                
                    if($request->input('email_to') != ""){
                        $msg = 'data saved successfully and a copy sent to '.$board->to.'!';
                    }else{
                    $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';
                    }


                }elseif(in_array($cid, $bookwormUS)){

                    include_once app_path().'/Http/Conditional/bookwormUS.php';  

                }else{

                    include_once app_path().'/Http/Conditional/onsubmit.php';

                } 

            }

            flash()->success($msg);
            return redirect()->back();  
            } 

        } // endif        
    }

    public function store_outbound(Catcher $catch,ClientPortalContent $cdata, Request $request,$board)
    {  
        $test = false;
        $cid = $board->campaign_id;
        $lob = $board->lob;
        $input = $request->except(['_method','_token','hidden_phone_number','hidden_lead_id','hidden_user','email_subject','all','lead_id','campaign_id','session_id','request_status','active', 'selectModalDispo', 'selectMainDispo','pageid']);
        $leadid = $request->input('lead_id');
        $campaign_id = $request->input('campaign_id');
        $user = $request->input('user');
        $session_id = $request->input('session_id');
        $disposition = $request->input('disposition');
        $opt = json_decode($board->options);
        $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);
        $if_exist = $cdata->select('id','lead_id')->where('lead_id','=',$leadid)->first(); 

        $data = null;

        $paperchase = array('58bcbd721afb8','58b3ac5f13f83','58b0531ee315b'); 
        $paperchase_dispo = array('LQ','FU','WN','DN'); 

        if($cid == '58dd847471ea0'){

            $input['Date'] = date('Y-m-d H:i:s',strtotime(date("Y-m-d H:i:s"))+ 3600 ); 

            $email_temp = '_presbia'; 
            $email_tbl = $input;

        }
        else{

            $input['Date'] = date("Y-m-d H:i:s");
            $email_temp = 'report'; 

        }

        $url_arr = explode('/', str_replace(url('/').'/', '', $request->server('HTTP_REFERER')));
        if (isset($url_arr[0])&& $url_arr[0] == 'preview') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'train') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'live') { $data['active'] = 3; $test = false;}

        $data = array(
            'user' => $user,
            'lead_id' => $leadid,
            'campaign_id' => $campaign_id,
            'user' =>  $user,
            'session_id' => $session_id,
            'content' => json_encode($input),
            'lists' => '{}',
            'dispo' =>  $disposition,
            'pageid' => $request->input('pageid')
        );

        if($test){

            if($request->input('test_email') == ''){

                $msg = 'data saved successfully, email not sent, no recipient!';

            }else{

                $board->to = $request->input('test_email');
                Mail::send('email.'.$email_temp, ['content' => $email_tbl], function ($m) use ($board) {
                   $m->from('test@magellan-solutions.com', 'Magellan Reporting Service');
                   $m->to($board->to, $board->lob)->subject('test Reporting Service'); 
                });
                $msg = 'data saved successfully, email sent to '.$request->input('test_email').'!';
            }

        }else{

            if(!empty($if_exist['id'])){
      
                $cdata->where('lead_id','=',$leadid)->where('campaign_id','=',$campaign_id)->update($data);
            
            }else{
  
                if($cdata->create($data)){ 

                }
            }

            $reports = mb_parse_report_options($board->options);

            if($request->input('email_subject') != ""){

                $board->subject = $request->input('email_subject');

            }else{

                $board->subject = (!empty($opt->subject)?$opt->subject:'Magellan Reporting Service' );
            }

            $requestsubject = $request->input('email_subject');


            $board->to = (!empty($board->to)?emailer_parser2($board->to):['noc@magellan-solutions.com']);
            $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
            $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):[]);
            $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):[]);

            $msg = 'Data has been Saved'.$request->input('test_email').'!';

            if(in_array('onsubmit', $reports)){

                if(in_array($cid, $paperchase)){

                    if(!in_array($input['disposition'], $paperchase_dispo)){

                        $board->to = array('webdev_report@magellan-solutions.com');

                    }

                }

                Mail::send('email.'.$email_temp, ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
                   $m->from($board->from);
                   $m->to($board->to)->subject($board->subject); 

                    if(empty($requestsubject)){
                        
                        $options = json_decode($board->options, true); 
                        $subject = $options["subject"];
                        $board->subject = $subject; 
                        $m->to($board->to)->subject($subject);  

                    }

                   if(!empty($board->cc) && empty($board->bcc)){
                     $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                   }else if(empty($board->cc) && !empty($board->bcc)){
                     $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                   }else if(!empty($board->cc) && !empty($board->bcc)){
                     $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                   }
                    
                });
        
                $msg = 'data saved successfully, email sent to '.$request->input('test_email').'!';
                
                }
            }
        
        flash()->success($msg);
        return redirect()->back();   
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cron_daily(CampaignData $cdata){


        $boards = $this->board->where('status',1)->with('assigned')->get();
        $dates = mb_two_dates();

        foreach ($boards as $board) {

            $logs = [];
            $opt = json_decode($board->options);

            if(filter_var($board->to, FILTER_VALIDATE_EMAIL)){

                if(in_array('daily', mb_parse_report_options($board->options))){

                    $c_data = $cdata->where('campaign_id',$board->campaign_id)
                    ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
                    ->get()->toArray();

                    
                    if(!empty($c_data)){

                        $email_tbl = $this->help_tbl->report_data_tbl($c_data);
                        $board->subject = (isset($opt->subject)?$opt->subject:'Magellan Reporting Service');
                        $board->to = (!empty($board->to)?emailer_parser2($board->to):'');
                        $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
                        $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):'');
                        $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):'');


                        Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {

                           $m->from($board->from, $board->subject);
                           $m->to($board->to, $board->lob)->subject($board->subject); 

                           if(!empty($board->cc) && empty($board->bcc)){
                             $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                           }else if(empty($board->cc) && !empty($board->bcc)){
                             $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                           }else if(!empty($board->cc) && !empty($board->bcc)){
                             $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                           }
                            
                        });

                        $logs[] = 'Daily report sent to '.$board->lob. 'with email address of '.implode(',', $board->to);                        
                    }else{
                        $logs[] = 'Daily report empty for'.$board->lob. 'with email address of '.implode(',', $board->to);
                    }
                    

                }else{ $logs[] = $board->lob.' daily report disabled';}  

            }else{
                $logs[] = 'Daily report failed to send to'.$board->lob. 'with email address of '.implode(',', $board->to);
            }
            if(!empty($logs)){
                $this->logs->create(['campaign_id' => $board->campaign_id, 'contents'=>json_encode($logs)]);
            }            
        }

    }

    private function _assigned_cc($boardObj,$cc_raw,$type = 'onsubmit'){
        
        $assigned = [];
        $cc1 = [];
        $assigned_raw = $boardObj->toArray();
        $cc_raw = json_decode($cc_raw);
        if($assigned_raw && $cc_raw){
            foreach ($assigned_raw as $ak => $av) {
                if(!empty($av['email']) && !empty($av['reports'])){
                    if(in_array($av['email'],$cc_raw) && in_array($type,json_decode($av['reports']))){
                        $cc1[] = $av['email'];
                    }
                }
            }
        }  
        return  $cc1;
    }  

    public function test(){
            Mail::send('email.test',['content'=>'test'], function ($message) {
                $message->from('us@example.com', 'Laravel');
        $recipients = [
            'stuff123@mailinator.com',
            '567stuff@mailinator.com',
        ];
                $message->to('marlon.bernal@magellan-solutions.com')->cc($recipients);
            });
    }  

    public function eCapital(CampaignData $cdata,Request $request){
        
        $lob = $request->input( 'lob' );
        $user = $request->input( 'user' );
        $lead_id = $request->input( 'lead_id' );
        $campaign_id = $request->input( 'campaign_id' );
        $phone = $request->input( 'phone' );
        $options = $request->input( 'options' );
        $test_email = $request->input( 'test_email' );
        $from = $request->input( 'from' );
        $to = $request->input( 'to' );
        $cc = $request->input( 'cc' );
        $bcc = $request->input( 'bcc' );
 
        $test = [$lob, $user, $lead_id, $campaign_id, $campaign_id ];
        $input = $request->except(['_method','_token','hidden_phone_number','hidden_lead_id','hidden_user','email_subject','all','lob','user', 'lead_id', 'campaign_id','content','options', 'from', 'cc','bcc', 'to','oid', 'retURL']);

        $data = array(
           'user' => $user,
           'lead_id' => $lead_id,
           'phone_number' => $phone,
           'campaign_id' =>  $campaign_id, 
           'lob' => $lob,
           'contents' => json_encode($input)
        );

        //check if test
        $url_arr = explode('/', str_replace(url('/').'/', '', $request->server('HTTP_REFERER')));
        if (isset($url_arr[0])&& $url_arr[0] == 'preview') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'train') { $data['active'] = 3; $test = true;}
        
        if($cdata->create($data)){ 
            
            $reports = mb_parse_report_options($options);
            $opt = json_decode($options);
            $msg = 'data saved successfully!';
            $email_tbl = $this->help_tbl->email_data_tbl($input);

                $board["to"]  = (!empty($to )?emailer_parser2($to):['noc@magellan-solutions.com']);
                $board["from"] = (!empty($from )?$from:'reporter@magellan-solutions.com');
                $board["cc"] = (!empty($cc)?emailer_parser2($cc):[]);
                $board["bcc"] = (!empty($bcc)?emailer_parser2($bcc):[]);
                $board["subject"] = "eCapital";
            
            if($test_email == ''){

                Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
                   $m->from($board["from"], 'Magellan Reporting Service');
                   $m->to($board["to"], 'eCapital')->subject('e-Capital Report'); 

                   if(!empty($board["cc"]) && empty($board["cc"])){
                         $m->to($board["to"], $board["lob"])->cc($board["to"])->subject("e-Capital Report"); 
                       }else if(empty($board["cc"]) && !empty($board["bcc"])){
                         $m->to($board["to"], $board["lob"])->bcc($board["bcc"])->subject("e-Capital Report"); 
                       }else if(!empty($board["cc"]) && !empty($board["bcc"])){
                         $m->to($board["to"], $board["lob"])->bcc($board["bcc"])->cc($board["cc"])->subject("e-Capital Report"); 
                   }

                });
                $msg = 'data saved successfully, email sent to '.$test_email.'!';



            }else{
                
                Mail::send('email.report', ['content' => $email_tbl], function ($m)  use ($board,$test_email){
                   $m->from($board["from"], 'Magellan Reporting Service');
                   $m->to($test_email, 'eCapital')->subject('test Reporting Service'); 
                });
                $msg = 'data saved successfully, email sent to '.$test_email.'!';

            }


            flash()->success($msg);
            return redirect()->back();  
        }        
    }

    public function customForm(Request $r){

       $inputs = $r->except(['pageid','_method','request_status','_token','all','email_subject','email_from','email_to','email_cc','attached']);
       $file = $r->all();
       $subject = ($r->email_subject?$r->email_subject:'Magellan Form Submit');
       $from = ($this->validEmail($r->email_from)?$r->email_from:'noc@magellan-solutions.com');
       $to = ($this->parseEmails($r->email_to)?$this->parseEmails($r->email_to):'noc@magellan-solutions.com');
       $cc = ($this->parseEmails($r->email_cc)?$this->parseEmails($r->email_cc):'noc@magellan-solutions.com');
       $email_tbl = $this->help_tbl->email_data_tbl($inputs);
        Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($subject,$from,$to,$cc,$file) {
           $m->from($from);
           $m->to($to, $subject)->cc($cc)->subject($subject);
           if(isset($file['attached'])):
            $m->attach($file['attached']->getRealPath(), array(
                'as' => 'attached.' . $file['attached']->getClientOriginalExtension(), 
                'mime' => $file['attached']->getMimeType())
            ); 
           endif; 
        });
        echo 'successfully sent!'.'<br>';  
        echo 'from: '.$from.'</br>';
        echo 'to: '; $this->printEmails($to);
        echo 'cc: '; $this->printEmails($cc);  


    }

    private function parseEmails($emails){

        $returnEmails = false;
        if($emails):
            $arr = explode(',', $emails);
            foreach($arr as $b):
                $ve = $this->validEmail($b);
                if($ve):
                    $returnEmails[] = $ve;
                endif;
            endforeach;
        endif;

        return $returnEmails;
    } 


    private function validEmail($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)):
          return $email;
        else:
          return false;
        endif;       
    }

    private function printEmails($arr){
        if(is_array($arr)):
            $a = implode(',', $arr);
        endif;
        echo $a.'</br>';
    }

}
 

