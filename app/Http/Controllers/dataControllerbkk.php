<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CampaignData;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App\Helpers\Tables;
use App\Model\Board;
use App\Model\Logs; 
use App\Model\VICI;
use App\Model\Catcher;
use DateTime; 
use DateTimeZone;
use Config;

class dataController extends Controller
{

    public function __construct(Tables $help_tbl,Board $board,Logs $logs,VICI $vici){
        $this->help_tbl = $help_tbl;
        $this->board = $board;
        $this->logs = $logs;
        $this->vici = $vici;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        //
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Catcher $catch,CampaignData $cdata, Request $request,$board)
    { 
        $test=false;
        $cid = $board->campaign_id;
        $lob = $board->lob;
        $all = json_encode($request->input('all'));
        $catch->insert(['content' => $all]);
        $leadid = $request->input('hidden_lead_id');
        $phone = $request->input('hidden_phone_number');
        $user = $request->input('hidden_user');
        $input = $request->except(['_method','_token','hidden_phone_number','hidden_lead_id','hidden_user','email_subject','all']);
       
        $input['lead_id'] = $leadid;
        $input['phone_number'] = $phone; 
        $input['user'] = $user;
        $input['lob'] = $lob;
        $input['campaign_id'] = $cid;
        //pre($input);
        $data = array(
           'user' => $user,
           'lead_id' => $leadid,
           'phone_number' => $phone,
           'campaign_id' =>  $board->campaign_id, 
           'lob' => $board->lob,
           'contents' => json_encode($input)
        );         

        $ivr = array("IVRMagellan", "MESReservation", "CSPMagellan", "MESOrdertaking", "57212dcfdec45", "MESInbound");
       
        //check if test
        $url_arr = explode('/', str_replace(url('/').'/', '', $request->server('HTTP_REFERER')));
        if (isset($url_arr[0])&& $url_arr[0] == 'preview') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'train') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'live') { $data['active'] = 3; $test = false;}

        if($cdata->create($data)){ //$cdata->create($data)

            $reports = mb_parse_report_options($board->options);
           
            $opt = json_decode($board->options);
            $msg = 'data saved successfully!';
            
            if($cid == "5748837f750c1"){ 
                $email_tbl = $this->help_tbl->email_data_tbl2($input,$opt);
            }else{
                $email_tbl = $this->help_tbl->email_data_tbl($input,$opt);
            }
            if($test){

                if($request->input('test_email') == ''){

                    $msg = 'data saved successfully, email not sent, no recipient!';
                }else{

                    $board->to = $request->input('test_email');
                    Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
                       $m->from('test@magellan-solutions.com', 'Magellan Reporting Service');
                       $m->to($board->to, $board->lob)->subject('test Reporting Service'); 
                    });
                    $msg = 'data saved successfully, email sent to '.$request->input('test_email').'!';

                }

            }else{ 

                if(in_array($cid, $ivr)){
               
                    $content = json_decode($data['contents'], TRUE);
                    $website_name = $content['which_website_visited'];

                    $board->subject = $website_name ;

                }else{

                //    pre($request->input('email_subject')); 

                    if(!empty($request->input('email_subject'))){

                        $board->subject = $request->input('email_subject');

                    }else{

                        $board->subject = (isset($opt->subject)?$opt->subject:'Magellan Reporting Service');
                    }


                }

                $requestsubject = $request->input('email_subject');;

                $board->to = (!empty($board->to)?emailer_parser2($board->to):['noc@magellan-solutions.com']);
                $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
                $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):[]);
                $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):[]);

                if(in_array('onsubmit', $reports)){

                    Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board, $requestsubject) {
                       $m->from($board->from);
                       $m->to($board->to)->subject($board->subject); 

                       if(empty($requestsubject)){
                            
                                $options = json_decode($board->options, true); 
                                $subject = $options["subject"];
                                $m->to($board->to)->subject($subject);  

                        }


                       if(!empty($board->cc) && empty($board->bcc)){
                         $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                       }else if(empty($board->cc) && !empty($board->bcc)){
                         $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                       }else if(!empty($board->cc) && !empty($board->bcc)){
                         $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                       }
                        
                    });

                    $msg = 'data saved successfully and a copy sent to '.implode(',',$board->to).'!';
                }else{

                    include_once app_path().'/Http/Conditional/onsubmit.php';

                } 

            }

            flash()->success($msg);
            return redirect()->back();  
        }        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cron_daily(CampaignData $cdata){

        // Mail::send('welcome', ['user' =>'x'], function ($m){
        //     $m->from('hello@app.com', 'Your Application');
        //     $m->to('marlon123@mailinator.com', 'marlon')->subject('Your Reminder!');
        // });

        $boards = $this->board->where('status',1)->with('assigned')->get();
        $dates = mb_two_dates();

        foreach ($boards as $board) {

            $logs = [];
            $opt = json_decode($board->options);

            if(filter_var($board->to, FILTER_VALIDATE_EMAIL)){

                if(in_array('daily', mb_parse_report_options($board->options))){

                    $c_data = $cdata->where('campaign_id',$board->campaign_id)
                    ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
                    ->get()->toArray();

                    
                    if(!empty($c_data)){

                        $email_tbl = $this->help_tbl->report_data_tbl($c_data);
                        $board->subject = (isset($opt->subject)?$opt->subject:'Magellan Reporting Service');
                        $board->to = (!empty($board->to)?emailer_parser2($board->to):'');
                        $board->from = ($board->to != ''?$board->from:'reporter@magellan-solutions.com');
                        $board->cc = (!empty($board->cc)?emailer_parser2($board->cc):'');
                        $board->bcc = (!empty($board->bcc)?emailer_parser2($board->bcc):'');


                        Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {

                           $m->from($board->from, $board->subject);
                           $m->to($board->to, $board->lob)->subject($board->subject); 

                           if(!empty($board->cc) && empty($board->bcc)){
                             $m->to($board->to, $board->lob)->cc($board->cc)->subject($board->subject); 
                           }else if(empty($board->cc) && !empty($board->bcc)){
                             $m->to($board->to, $board->lob)->bcc($board->bcc)->subject($board->subject); 
                           }else if(!empty($board->cc) && !empty($board->bcc)){
                             $m->to($board->to, $board->lob)->bcc($board->bcc)->cc($board->cc)->subject($board->subject); 
                           }
                            
                        });

                        $logs[] = 'Daily report sent to '.$board->lob. 'with email address of '.implode(',', $board->to);                        
                    }else{
                        $logs[] = 'Daily report empty for'.$board->lob. 'with email address of '.implode(',', $board->to);
                    }
                    

                }else{ $logs[] = $board->lob.' daily report disabled';}  

            }else{
                $logs[] = 'Daily report failed to send to'.$board->lob. 'with email address of '.implode(',', $board->to);
            }
            if(!empty($logs)){
                $this->logs->create(['campaign_id' => $board->campaign_id, 'contents'=>json_encode($logs)]);
            }            
        }

    }

    private function _assigned_cc($boardObj,$cc_raw,$type = 'onsubmit'){
        
        $assigned = [];
        $cc1 = [];
        $assigned_raw = $boardObj->toArray();
        $cc_raw = json_decode($cc_raw);
        if($assigned_raw && $cc_raw){
            foreach ($assigned_raw as $ak => $av) {
                if(!empty($av['email']) && !empty($av['reports'])){
                    if(in_array($av['email'],$cc_raw) && in_array($type,json_decode($av['reports']))){
                        $cc1[] = $av['email'];
                    }
                }
            }
        }  
        return  $cc1;
    }  

    public function test(){
            Mail::send('email.test',['content'=>'test'], function ($message) {
                $message->from('us@example.com', 'Laravel');
        $recipients = [
            'stuff123@mailinator.com',
            '567stuff@mailinator.com',
        ];
                $message->to('marlon.bernal@magellan-solutions.com')->cc($recipients);
            });
    }  

    public function eCapital(CampaignData $cdata,Request $request){
        
        $lob = $request->input( 'lob' );
        $user = $request->input( 'user' );
        $lead_id = $request->input( 'lead_id' );
        $campaign_id = $request->input( 'campaign_id' );
        $phone = $request->input( 'phone' );
        $options = $request->input( 'options' );
        $test_email = $request->input( 'test_email' );
        $from = $request->input( 'from' );
        $to = $request->input( 'to' );
        $cc = $request->input( 'cc' );
        $bcc = $request->input( 'bcc' );
 
        $test = [$lob, $user, $lead_id, $campaign_id, $campaign_id ];
        $input = $request->except(['_method','_token','hidden_phone_number','hidden_lead_id','hidden_user','email_subject','all','lob','user', 'lead_id', 'campaign_id','content','options', 'from', 'cc','bcc', 'to','oid', 'retURL']);

        $data = array(
           'user' => $user,
           'lead_id' => $lead_id,
           'phone_number' => $phone,
           'campaign_id' =>  $campaign_id, 
           'lob' => $lob,
           'contents' => json_encode($input)
        );

        //check if test
        $url_arr = explode('/', str_replace(url('/').'/', '', $request->server('HTTP_REFERER')));
        if (isset($url_arr[0])&& $url_arr[0] == 'preview') { $data['active'] = 3; $test = true;}
        if (isset($url_arr[0])&& $url_arr[0] == 'train') { $data['active'] = 3; $test = true;}
        
        if($cdata->create($data)){ 
            
            $reports = mb_parse_report_options($options);
            $opt = json_decode($options);
            $msg = 'data saved successfully!';
            $email_tbl = $this->help_tbl->email_data_tbl($input);

                $board["to"]  = (!empty($to )?emailer_parser2($to):['noc@magellan-solutions.com']);
                $board["from"] = (!empty($from )?$from:'reporter@magellan-solutions.com');
                $board["cc"] = (!empty($cc)?emailer_parser2($cc):[]);
                $board["bcc"] = (!empty($bcc)?emailer_parser2($bcc):[]);
                $board["subject"] = "eCapital";
            
            if($test_email == ''){

                Mail::send('email.report', ['content' => $email_tbl], function ($m) use ($board) {
                   $m->from($board["from"], 'Magellan Reporting Service');
                   $m->to($board["to"], 'eCapital')->subject('e-Capital Report'); 

                   if(!empty($board["cc"]) && empty($board["cc"])){
                         $m->to($board["to"], $board["lob"])->cc($board["to"])->subject("e-Capital Report"); 
                       }else if(empty($board["cc"]) && !empty($board["bcc"])){
                         $m->to($board["to"], $board["lob"])->bcc($board["bcc"])->subject("e-Capital Report"); 
                       }else if(!empty($board["cc"]) && !empty($board["bcc"])){
                         $m->to($board["to"], $board["lob"])->bcc($board["bcc"])->cc($board["cc"])->subject("e-Capital Report"); 
                   }

                });
                $msg = 'data saved successfully, email sent to '.$test_email.'!';



            }else{
                
                Mail::send('email.report', ['content' => $email_tbl], function ($m)  use ($board,$test_email){
                   $m->from($board["from"], 'Magellan Reporting Service');
                   $m->to($test_email, 'eCapital')->subject('test Reporting Service'); 
                });
                $msg = 'data saved successfully, email sent to '.$test_email.'!';

            }


            flash()->success($msg);
            return redirect()->back();  
        }        
    }

  

}
 

