<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Auth;
use App\Test;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use App\Model\Board2; 

use App\Model\Board;
use App\Model\CampaignData;
use App\Helpers\Tables;
use App\Model\Logs;
use App\Model\VICI;
use Mail;
use DB;
class tabularboardController extends Controller
{
    
    public function index(Board2 $boardObj)
    {
        $boards = $boardObj->all();
        return view('frontend.test',compact('boards'));
    }

    public function display(Request $request)
    {	
        $help = new \App\Http\Controllers\Frontend\helper;
        //$request->id;
		$campaign = $request->id;
		if (($request->id == '') ||  ($request->id == NULL) )
		{
		//echo $tempvariable;
		$campaigndatas = CampaignData::where('campaign_id', '=', '59a45c28d0d42')->where('contents','not like','%test%')->orderBy('created_at', 'desc')->get();
		
		
		}
        elseif($campaign == '5879861681241'){
            //phpinfo();
            ini_set('memory_limit', '-1');
            $campaigndatas = CampaignData::where('campaign_id', '=', $campaign)->where('contents','not like','%"request_status":"test"%')
            ->where('created_at', '>=', '2018-01-01')
            ->where('created_at', '<=', '2019-12-31')
            ->orderBy('created_at', 'desc')
            ->get();
            
        }elseif($campaign == '5b58dc9911d9e'){
             ini_set('memory_limit', '-1');
             $campaigndatas = CampaignData::where('campaign_id', '=', $campaign)->where('contents','not like','%test%')
             ->where('created_at', '>=', '2019-01-01')
             ->orderBy('created_at', 'desc')
             ->get();
            // foreach($campaigndatas as $campaign){
            //    dd($campaign->contents);
            // }
        }
		else{
		//	echo "test";
        
		$campaigndatas = CampaignData::where('campaign_id', '=', $campaign)->where('contents','not like','%"request_status":"test"%')->orderBy('created_at', 'desc')->limit(2000)->get();

		}
       
        $custom = array('59b96bfe41291','5879861681241', '5b58dc9911d9e');

        //pre($campaign);
        return view('frontend.tabularboard',compact('campaign','campaigndatas','tpcastboard','contents','help','custom')) ;
    }

   /* public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);

        $user = new User;
        $input = $request->only(['name','email','password']);
        $input['password'] = bcrypt($input['password']);
        if($user->create($input)){
            return redirect()->route('login');
        }
    }

    public function login()
    {
        if (Auth::check()) {return redirect()->route('dashboard');}
        return view('frontend.login');
    } 

    public function login_post(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect()->route('dashboard');}

        if (Auth::guest())
        {
            return $this->getFailedLoginMessage();          
        }
    } 

    protected function getFailedLoginMessage()
    {
        return redirect()->back()->with('alert', 'These credentials do not match our records');
        //return 'These credentials do not match our records.';

    }

    private function _assigned_cc($boardObj,$cc_raw,$type = 'onsubmit'){

        $assigned = [];
        $cc1 = [];
        $assigned_raw = $boardObj->toArray();
        $cc_raw = json_decode($cc_raw);
        if($assigned_raw && $cc_raw){
            foreach ($assigned_raw as $ak => $av) {
                if(!empty($av['email']) && !empty($av['reports'])){
                    if(in_array($av['email'],$cc_raw) && in_array($type,json_decode($av['reports']))){
                        $cc1[] = $av['email'];
                    }
                }
            }
        }  
        return  $cc1;
    } 





    public function test(){


                $boards = Board::where('status',1)->get()->toArray();

                foreach ($boards as $board) {
                    
                        //pre(mb_parse_report_options($board['options']));
                        $html_logs = $html_data = '';
                        $status = 0;
                        $vici = DB::connection('vicidial')->table('vicidial_closer_log');
                        $templar = DB::table('vici_to_templar');
                        $dates = mb_two_dates();
                        $vct = $templar->select('vici_plug')->where('templar_socket',$board['campaign_id'])->get();
                        if(!empty($vct)){
                            $in = [];
                            foreach ($vct as $val) {
                                $in[] = $val->vici_plug;
                            }
                            //dispo data from inbound
                            $logs = $vici->select('status',DB::raw('count(*) as count'))
                                    ->whereBetween('call_date', [$dates['yesterday'], $dates['today']])
                                    ->whereIn('campaign_id',$in)
                                    ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                                    ->groupBy('status')
                                    ->orderBy('status', 'asc')
                                    ->get(); 

                            //if empty try to check in outbound table
                            if(empty($logs)){
                            $logs = DB::connection('vicidial')->table('vicidial_log')
                                    ->select('status',DB::raw('count(*) as count'))
                                    ->whereBetween('call_date', [$dates['yesterday'], $dates['today']])
                                    ->whereIn('campaign_id',$in)
                                    ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                                    ->groupBy('status')
                                    ->orderBy('status', 'asc')
                                    ->get(); 
                            }

                            if(!empty($logs)){
                                $html_logs = Tables::html_logs($logs);
                            }
                            //board input data
                            $data = DB::table('campaign_data')
                                    ->where('campaign_id',$board['campaign_id'])
                                    ->where('contents', 'like', '%"request_status":"live"%')
                                    ->whereBetween('created_at', [$dates['yesterday'],$dates['today']])
                                    ->get();
                            if(!empty($data)){
                                $html_data = Tables::html_data($data);
                            } 
                            //emailer
                            if(in_array('daily', mb_parse_report_options($board['options']))){
                                if(!empty($logs)){
                                    $board['subject'] = (isset($opt['subject'])?$opt['subject']:'Magellan Reporting Service');
                                    $board['to'] = ['marlonbbernal@mailinator.com'];
                                    $board['from'] = 'reports@mailinator.com';
                                    $board['cc'] = false;
                                    $board['bcc'] = false;                    
                                    
                                    if(!$board['to']){
                                            $board['to'] = 'NOC@magellan-solutions.com';
                                            Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                               $m->from($board['from'], $board['subject']);
                                               $m->to($board['to'], $board['lob'])->subject('No Recipient eod!'); 
                                            }); 
                                    }else{
                                        Mail::send('email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                           $m->from($board['from'], $board['subject']);
                                           $m->to($board['to'], $board['lob'])->subject($board['subject']); 
                                           if($board['cc'] && !$board['bcc']){
                                             $m->to($board['to'], $board['lob'])->cc($board['cc'])->subject($board['subject']); 
                                           }else if(!$board['cc'] && $board['bcc']){
                                             $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->subject($board['subject']); 
                                           }else if($board['cc'] && $board['bcc']){
                                             $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
                                           }
                                           
                                        }); 
                                        $status = 1;                       
                                    }

                                }
                             
                            }
                        }

                }


    }

*/












}

