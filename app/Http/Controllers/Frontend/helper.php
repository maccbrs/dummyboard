<?php namespace App\Http\Controllers\Frontend;
use Auth;



class Helper 
{


	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function sortByDate($obj){
		
		$arr = [];
		foreach($obj as $k => $v):
			$arr[$v->created] = $v;
		endforeach;
		$this->pre($arr);
		
	}	

	public function access($str,$x){

		if($str == '') return false;
		$arr = json_decode($str,true);
		if(in_array($x, $arr)){
			return true;
		}else{
			return false;
		}

	}
	
	public function fetchrstatus($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['request_status'];
		endif;
	}

	public function fetchpnumber($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['PHONE_NUMBER'])?$obj['PHONE_NUMBER']:'N/A';
		endif;
	}

	public function fetchdate($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['DATE(MM/DD/YYY)'])?$obj['DATE(MM/DD/YYY)']:'N/A';
		endif;
	}

	public function fetchsaddress($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['SHIPPING_ADDRESS'])?$obj['SHIPPING_ADDRESS']:'N/A';
		endif;
	}

	public function fetchsmethod($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['SHIP_METHOD'])?$obj['SHIP_METHOD']:'N/A';
		endif;
	}

	public function fetchqp($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['QUANTITY/PRODUCT'])?$obj['QUANTITY/PRODUCT']:'N/A';
		endif;
	}

	public function fetchfal($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['FIRST_AND_LAST_NAME'])?$obj['FIRST_AND_LAST_NAME']:'N/A';
		endif;
	}

	public function fetchdid($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['DID_NUMBER'])?$obj['DID_NUMBER']:'N/A';
		endif;
	}

	public function fetchprice($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['PRICE'])?$obj['PRICE']:'N/A';
		endif;
	}

	public function fetchbaddress($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['BILLINGADDRESS'])?$obj['BILLINGADDRESS']:'N/A';
		endif;
	}

	public function fetchrname($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['REPRESENTATIVES_NAME'])?$obj['REPRESENTATIVES_NAME']:'N/A';
		endif;
	}

	public function fetchcity($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['CITY'])?$obj['CITY']:'N/A';
		endif;
	}

	public function fetchstate($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['STATE'])?$obj['STATE']:'N/A';
		endif;
	}

	public function fetchzip($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['ZIPCODE'])?$obj['ZIPCODE']:'N/A';
		endif;
	}

	public function fetchcountry($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['COUNTRY'])?$obj['COUNTRY']:'N/A';
		endif;
	}

	public function fetchfname($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['First_Name'])?$obj['First_Name']:'N/A';
		endif;
	}

	public function fetchlname($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Last_Name'];
		endif;
	}

	public function fetchemail($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['EMAIL_ADDRESS'])?$obj['EMAIL_ADDRESS']:'N/A';
		endif;
	}
	
	public function fetchsn($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Serial_Number'];
		endif;
	}
	public function fetchdevicetype($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['Type_of_Device'])?$obj['Type_of_Device']:'N/A';
		endif;
	}
	public function fetchcountry2($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['Country'])?$obj['Country']:'N/A';
		endif;
	}

	public function fetchidesc($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Issue_Description'];
		endif;
	}

	public function fetchidesco($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Issue_Description_Others'];

		endif;
	}

	public function fetchnotes($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Notes'];
		endif;
	}

	public function fetchstatus($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['Status'];
		endif;
	}

	public function fetchagent($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['agent'];
		endif;
	}

	public function fetchagentname($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['AGENT_NAME'];
		endif;
	}

	public function fetchstarttime($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['start_time'];
		endif;
	}

	public function fetchform($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['FORM_TO_BE_SUBMITTED'];
		endif;
	}

	public function fetchvideo($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['VIDEO_ID'];
		endif;
	}

	public function fetchissuetype($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['ISSUE_TYPE'];
		endif;
	}

	public function fetchissuespecifics($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['ISSUE_SPECIFICS'];
		endif;
	}

	public function fetchemailforVr($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return !empty($obj['Email_Address'])?$obj['Email_Address']:'N/A';
		endif;
	}
}  