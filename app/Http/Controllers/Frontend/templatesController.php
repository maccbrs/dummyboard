<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Model\Board;
use App\Model\Page;
use App\Model\Emailer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Board2; 
use Mail;

class templatesController extends Controller
{

    public function __construct(Request $r){

    	$base = route('home');
    	$this->viewer = $base.'/live';
    	$this->link = 'live';
        if ($r->is('preview/*')){ $this->viewer = $base.'/preview'; $this->link = 'preview'; }
        if ($r->is('train/*')){ $this->viewer = $base.'/train'; $this->link = 'train'; }
        
    }

	public function index($board,$page){


		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$bookwormBoards = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1" ,"571e4e0b94b44","57b51a7f5ae78","571e4d7153706","571a63ea42803","571a5ac93a0f4");

		$bookwormBoardsUS = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1");

		$bookwormBoardsAUS = array("571e4d7153706", "571e4e0b94b44", "57b51a7f5ae78");

		$bookwormBoardsTOTBP = array("571a63ea42803", "571a5ac93a0f4");

        $omnitrixBoards = array('56df51288fd0f', '56df58b4368d3', '571a655821190', '5879861681241', '589c888bd6bad', 'OmniContractor', 'OmniContractor_2', 'OmniContractor_3', '56f441df7015d', 'OmniMoving', 'OmniMoving2', 'PriscriptoDID1', 'MESInbound', 'MESOrdertaking', 'MESReservation', 'OmniMSUSA', 'OmniDentures', '56ce35f3b46e7', '56ce189a9a98c', 'OmniACEI', 'OmniGiftgate', 'OmniTightSqeeze', 'OmniVanity', 'Painless', '5748837f750c1', '56fd65a2750dd', '577ff257db927', '577c144e8bde6', '577bd91f4282f', '5776d34f949fb', '579b7f7cb9ba7', '57a4d29a4dc64', '57c9a31a702ab', '57e16ad2987ea', 'CSPMagellan', 'IVRMagellan', '576d8b816c896', '582e52343167d', '57f80f9d69859', 'OmniAtlantic', 'OmniNevada', 'OmniORTHO', 'OmniOSPI', 'OmniRegional2', 'OmniDolorALX', '5851948ebafb1', '57bde6200614d', 'Strainee', '58e2bf7a97d4c', '58f00b63b8d2f', '56ff303456bab', '571e4c72a2370', '580677f27a21f', '57f80ac474b6d', '57b392c0b3556', '57bf26633a5b9', '57d1ee5243123', '5817a1cccac78', '5841c3b6b8ecd', 'OmniDRC', 'omniParty', '59a45c28d0d42');


		$view = 'templates.'.$page->template.'.index';
		//echo $view;die;


		return view($view,compact('test','board','page','pages','url','btns','link','bookwormBoards','bookwormBoardsUS','bookwormBoardsAUS','bookwormBoardsTOTBP','omnitrixBoards'));
	}

	public function board($board){

		
		$url = $this->viewer;
		$link = $this->link;
		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$bookwormBoards = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1", "571e4d7153706", "571e4e0b94b44","57b51a7f5ae78","571e4d7153706" ,"571a63ea42803","571a5ac93a0f4");

		$bookwormBoardsUS = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1");

		$bookwormBoardsAUS = array("571e4d7153706", "571e4e0b94b44", "57b51a7f5ae78");

		$bookwormBoardsTOTBP = array("571a63ea42803", "571a5ac93a0f4");

        $omnitrixBoards = array('56df51288fd0f', '56df58b4368d3', '571a655821190', '5879861681241', '589c888bd6bad', 'OmniContractor', 'OmniContractor_2', 'OmniContractor_3', '56f441df7015d', 'OmniMoving', 'OmniMoving2', 'PriscriptoDID1', 'MESInbound', 'MESOrdertaking', 'MESReservation', 'OmniMSUSA', 'OmniDentures', '56ce35f3b46e7', '56ce189a9a98c', 'OmniACEI', 'OmniGiftgate', 'OmniTightSqeeze', 'OmniVanity', 'Painless', '5748837f750c1', '56fd65a2750dd', '577ff257db927', '577c144e8bde6', '577bd91f4282f', '5776d34f949fb', '579b7f7cb9ba7', '57a4d29a4dc64', '57c9a31a702ab', '57e16ad2987ea', 'CSPMagellan', 'IVRMagellan', '576d8b816c896', '582e52343167d', '57f80f9d69859', 'OmniAtlantic', 'OmniNevada', 'OmniORTHO', 'OmniOSPI', 'OmniRegional2', 'OmniDolorALX', '5851948ebafb1', '57bde6200614d', 'Strainee', '58e2bf7a97d4c', '58f00b63b8d2f', '56ff303456bab', '571e4c72a2370', '580677f27a21f', '57f80ac474b6d', '57b392c0b3556', '57bf26633a5b9', '57d1ee5243123', '5817a1cccac78', '5841c3b6b8ecd', 'OmniDRC', 'omniParty','59c193ef350aa','59c2cbdfb9ff8','59a45c28d0d42');

		if(empty($pages)){
			$view = 'templates.no-page';
		}else{
			$page = $pages[0];
			if($board->primary_page != ''){
				foreach ($board['pages'] as  $v) {
					if($board->primary_page == $v->id){
						$page = $v;
					}
				}
			}
			$view = 'templates.'.$page->template.'.index';
			
		}

        //pre($omnitrixBoards);
		//echo $view;die;
		return view($view,compact('board','page','pages','url','btns','link', 'bookwormBoards','bookwormBoardsUS','bookwormBoardsAUS','bookwormBoardsTOTBP','omnitrixBoards'));

	}

	public function iq_link(){

        return view('email.iq_link');      

    }

    public function iq_sched(){

        return view('email.iq_sched');     

    }

    public function iq_link_post(Request $r){

    	$data['to'] =  $r->input('to');
    	// $data['cc'] =  "kit@mailinator.com";
    	$data['cc'] =  $r->input('cc');
    	$data['from'] =  "logi_analytics@magellan-solutions.com";
    	$data['notes'] =  $r->input('notes');
    	$data['subject'] = 'Logi Analytics Interactive Visual Gallery' ;
    	$data['lname'] =  $r->input('lname');
    	$data['fname'] = $r->input('fname');
    	$data['name'] = $r->input('name');
        $data['user'] = $r->input('user');
    	$data['bcc'] =  ["webdev_report@magellan-solutions.com","danilo.hipol@magellan-solutions.com"];


        $data['cc'] = (!empty($data['cc'])?$data['cc']:[]);


        $data['fullname'] =  null;

        if($data['user'] == 'jgobantes'){
            $data['from'] =  "jose.gobantes@logianalytics.com";
            $data['fullname'] =  "Jose Vicente Gobantes";
        }

        if($data['user'] == 'mresurreccion'){
            $data['from'] =  "marielle.resurreccion@logianalytics.com";
            $data['fullname'] =  "Marielle Acy Resurreccion";
        }

        if($data['user'] == 'traochavilo'){
            $data['from'] =  "tamie.ochavillo@logianalytics.com";
            $data['fullname'] =  "Tamie Rae Anne Ochavilo";
        }

        if($data['user'] == 'ellaneta'){
            $data['from'] =  "eufemio.llaneta@logianalytics.com";
            $data['fullname'] =  "Eufemio Llaneta";
        }

        if($data['user'] == 'jjsuobiron'){
            $data['from'] =  "john.joseph@logianalytics.com";
            $data['fullname'] =  "John Joseph Suobiron";
        }

           
        if($data['user'] == 'acasas'){
            $data['from'] =  "argwin.casas@logianalytics.com";
            $data['fullname'] =  "Argwin Casas";
        }

        if($data['user'] == 'mpenaflor'){
            $data['from'] =  "marchie.penaflor@logianalytics.com";
            $data['fullname'] =  "Marchie Penaflor";
        }

        if($data['user'] == 'tvillanueva'){
            $data['from'] =  "tom.villanueva@logianalytics.com";
            $data['fullname'] =  "Tom Christian Villanueva";
        }

        if($data['user'] == 'jcuevas'){
            $data['from'] =  "jose.cuevas@logianalytics.com";  
            $data['fullname'] =  "Jose Paolo Cuevas";
        }

        if($data['user'] == 'kit'){
            $data['from'] =  "kit@mailinator.com";  
            $data['fullname'] =  "Kit";
        }

        if($data['user'] == 'eesmayor'){
            $data['from'] =  "erlyn.esmayor@logianalytics.com";  
            $data['fullname'] =  "Erlyn Esmayor";
        }

        if($data['user'] == 'dcardosa'){
            $data['from'] =  "donnell.cardosa@logianalytics.com";  
            $data['fullname'] =  "Donnell Cardosa";
        }

        if($data['user'] == 'aapili'){
            $data['from'] =  "ace.aspili@logianalytics.com";  
            $data['fullname'] =  "Ace Aspili";
        }

        if($data['user'] == 'jjsuobiron'){
            $data['from'] =  "John.joseph@logianalytics.com";  
            $data['fullname'] =  "John Joseph Suobiron";
        }

        if($data['fullname'] != null){

            Mail::send('email.iq_link_email', ['content' => $data], function ($m) use ($data) {
               $m->from($data['from']);
               $m->to($data['to'])->subject($data['subject']); 
               $m->cc($data['cc']);
               $m->bcc($data['bcc']);
            });  

            $email_save = new Emailer; 

            $emailer['name'] = !empty($data['name']) ? $data['name'] : "";
            $emailer['to'] = !empty($data['to']) ? $data['to'] : "";
            $emailer['from'] = !empty($data['from']) ? $data['from'] : "";
            $emailer['cc'] = !empty($data['cc']) ? json_encode($data['cc']) : "";
            $emailer['bcc'] = !empty($data['bcc']) ? json_encode($data['bcc']) : "";
            $emailer['subject'] = !empty($data['subject']) ? $data['subject'] : "";
            $emailer['status'] = !empty($data['status']) ? $data['status'] : "";

            $emailer['content'] = json_encode($data);

            $email_save->create($emailer);

            return view('close');  

        }

        return redirect()->back(); 

    }

    public function iq_sched_post(Request $r){

    	$data['to'] =  $r->input('to');
    	//$data['cc'] =  "kit@mailinator.com";
    	$data['cc'] =  $r->input('cc');
    	$data['from'] =  "logi_analytics@magellan-solutions.com";
    	$data['notes'] =  $r->input('notes');
    	$data['subject'] = 'Logi Analytics Schedule Email Invite' ;
    	$data['lname'] =  $r->input('lname');
    	$data['fname'] = $r->input('fname');
    	$data['user'] = $r->input('user');
    	$data['timezone'] = $r->input('timezone');
    	$data['time'] = $r->input('time');
    	$data['date'] = $r->input('date');
    	$data['name'] = $r->input('name');
        $data['phone_number'] = $r->input('phone_number');
    	$data['part_of_day'] = $r->input('part_of_day');
    	$data['bcc'] =  ["webdev_report@magellan-solutions.com","danilo.hipol@magellan-solutions.com","Sana.Narani@logianalytics.com","Shelby.Dawirs@logianalytics.com"]; 

        $email_tbl = null;
        $data['fullname'] =  null;
        $data['cc_name'] =  null;

        if($data['cc'] == 'dustin.yelinek@logianalytics.com'){

            $data['cc_name'] =  "Dustin";
        }

        if($data['cc'] == 'devon.Corker@logianalytics.com'){

            $data['cc_name'] =  "Devon";
        }

        if($data['cc'] == 'brian.zurek@logianalytics.com'){

            $data['cc_name'] =  "Brian";
        }

        if($data['cc'] == 'peter.barrett@logianalytics.com'){

            $data['cc_name'] =  "Peter";
        }

        if($data['cc'] == 'matthew.ellis@logianalytics.com'){

            $data['cc_name'] =  "Matthew";
        }

        if($data['cc'] == 'Dylan.lease@logianalytics.com'){

            $data['cc_name'] =  "Dylan";
        }

        if($data['cc'] == 'ryan.skorupan@logianalytics.com'){

            $data['cc_name'] =  "Ryan";
        }

        if($data['cc'] == 'marc.hagen@logianalytics.com'){

            $data['cc_name'] =  "Marc";
        }

        if($data['cc'] == 'harris.husain@logianalytics.com'){

            $data['cc_name'] =  "Harris";
        }

        if($data['cc'] == 'tyler.dixon@logianalytics.com'){

            $data['cc_name'] =  "Tyler";
        }



        $data['cc'] = (!empty($data['cc'])?$data['cc']:['danilo.hipol@magellan-solutions.com']);

        if($data['user'] == 'jgobantes'){
            $data['from'] =  "jose.gobantes@logianalytics.com";
            $data['fullname'] =  "Jose Vicente Gobantes";
        }

        if($data['user'] == 'mresurreccion'){
            $data['from'] =  "marielle.resurreccion@logianalytics.com";
            $data['fullname'] =  "Marielle Acy Resurreccion";
        }

        if($data['user'] == 'traochavilo'){
            $data['from'] =  "tamie.ochavillo@logianalytics.com";
            $data['fullname'] =  "Tamie Rae Anne Ochavilo";
        }
        
        if($data['user'] == 'ellaneta'){
            $data['from'] =  "eufemio.llaneta@logianalytics.com";
            $data['fullname'] =  "Eufemio Llaneta";
        }

        if($data['user'] == 'acasas'){
            $data['from'] =  "argwin.casas@logianalytics.com";
            $data['fullname'] =  "Argwin Casas";
        }

        if($data['user'] == 'mpenaflor'){
            $data['from'] =  "marchie.penaflor@logianalytics.com";
            $data['fullname'] =  "Marchie Penaflor";
        }

        if($data['user'] == 'tvillanueva'){
            $data['from'] =  "tom.villanueva@logianalytics.com";
            $data['fullname'] =  "Tom Christian Villanueva";
        }

        if($data['user'] == 'jcuevas'){
            $data['from'] =  "jose.cuevas@logianalytics.com";  
            $data['fullname'] =  "Jose Paolo Cuevas";
        }

        if($data['user'] == 'jjsuobiron'){
            $data['from'] =  "john.joseph@logianalytics.com";
            $data['fullname'] =  "John Joseph Suobiron";
        }

        if($data['user'] == 'kit'){
            $data['from'] =  "kit@mailinator.com";  
            $data['fullname'] =  "Kit";
        }

        if($data['cc'] == 'harris.husain@logianalytics.com'){

            $data['cc_name'] =  "Harris";
        }

        if($data['cc'] == 'tyler.dixon@logianalytics.com'){

            $data['cc_name'] =  "Tyler";
        }

        if($data['fullname'] != null){

            Mail::send('email.iq_sched_email', ['content' => $data], function ($m) use ($data) {
               $m->from($data['from']);
               $m->to($data['to'])->subject($data['subject']); 
               $m->cc($data['cc']);
               $m->bcc($data['bcc']);
            });   

            $email_save = new Emailer; 

            $emailer['name'] = !empty($data['name']) ? $data['name'] : "";
            $emailer['to'] = !empty($data['to']) ? $data['to'] : "";
            $emailer['from'] = !empty($data['from']) ? $data['from'] : "";
            $emailer['cc'] = !empty($data['cc']) ? json_encode($data['cc']) : "";
            $emailer['bcc'] = !empty($data['bcc']) ? json_encode($data['bcc']) : "";
            $emailer['subject'] = !empty($data['subject']) ? $data['subject'] : "";
            $emailer['status'] = !empty($data['status']) ? $data['status'] : "";

            $emailer['content'] = json_encode($data);

            $email_save->create($emailer);

            return view('close');  
        }

        return redirect()->back();  

    }

    
} 