<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Model\Board;
use App\Model\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Board2; 

class trainController extends Controller
{

   public function __construct(Request $r){

    	$base = route('home');
    	$this->viewer = $base.'/live';
    	$this->link = 'live';
        if ($r->is('preview/*')){ $this->viewer = $base.'/preview'; $this->link = 'preview'; }
        if ($r->is('train/*')){ $this->viewer = $base.'/train'; $this->link = 'train'; }
        
    }


	public function index($board,$page){

		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$omnitrixBoards = array('57f544bb414ed', '56df51288fd0f', '56df58b4368d3', '571a655821190', '5879861681241', '589c888bd6bad', 'OmniContractor', 'OmniContractor_2', 'OmniContractor_3', '56f441df7015d', 'OmniMoving', 'OmniMoving2', 'PriscriptoDID1', 'MESInbound', 'MESOrdertaking', 'MESReservation', 'OmniMSUSA', 'OmniDentures', '56ce35f3b46e7', '56ce189a9a98c', 'OmniACEI', 'OmniGiftgate', 'OmniTightSqeeze', 'OmniVanity', 'Painless', '5748837f750c1', '56fd65a2750dd', '577ff257db927', '577c144e8bde6', '577bd91f4282f', '5776d34f949fb', '579b7f7cb9ba7', '57a4d29a4dc64', '57c9a31a702ab', '57e16ad2987ea', 'CSPMagellan', 'IVRMagellan', '576d8b816c896', '582e52343167d', '57f80f9d69859', 'OmniAtlantic', 'OmniNevada', 'OmniORTHO', 'OmniOSPI', 'OmniRegional2', 'OmniDolorALX', '5851948ebafb1', '57bde6200614d', 'Strainee', '58e2bf7a97d4c', '58f00b63b8d2f', '56ff303456bab', '571e4c72a2370', '580677f27a21f', '57f80ac474b6d', '57b392c0b3556', '57bf26633a5b9', '57d1ee5243123', '5817a1cccac78', '5841c3b6b8ecd', 'OmniDRC', 'omniParty','59c193ef350aa','59c2cbdfb9ff8');

		$view = 'templates.'.$page->template.'.index';
		//echo $view;die;
		return view($view,compact('board','page','pages','url','btns','link','omnitrixBoards'));
	}

	public function board($board){

		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$omnitrixBoards = array('57f544bb414ed', '56df51288fd0f', '56df58b4368d3', '571a655821190', '5879861681241', '589c888bd6bad', 'OmniContractor', 'OmniContractor_2', 'OmniContractor_3', '56f441df7015d', 'OmniMoving', 'OmniMoving2', 'PriscriptoDID1', 'MESInbound', 'MESOrdertaking', 'MESReservation', 'OmniMSUSA', 'OmniDentures', '56ce35f3b46e7', '56ce189a9a98c', 'OmniACEI', 'OmniGiftgate', 'OmniTightSqeeze', 'OmniVanity', 'Painless', '5748837f750c1', '56fd65a2750dd', '577ff257db927', '577c144e8bde6', '577bd91f4282f', '5776d34f949fb', '579b7f7cb9ba7', '57a4d29a4dc64', '57c9a31a702ab', '57e16ad2987ea', 'CSPMagellan', 'IVRMagellan', '576d8b816c896', '582e52343167d', '57f80f9d69859', 'OmniAtlantic', 'OmniNevada', 'OmniORTHO', 'OmniOSPI', 'OmniRegional2', 'OmniDolorALX', '5851948ebafb1', '57bde6200614d', 'Strainee', '58e2bf7a97d4c', '58f00b63b8d2f', '56ff303456bab', '571e4c72a2370', '580677f27a21f', '57f80ac474b6d', '57b392c0b3556', '57bf26633a5b9', '57d1ee5243123', '5817a1cccac78', '5841c3b6b8ecd', 'OmniDRC', 'omniParty','59a45c28d0d42');

		if(empty($pages)){
			$view = 'templates.no-page';
		}else{
			$page = $pages[0];
			if($board->primary_page != ''){
				foreach ($board['pages'] as  $v) {
					if($board->primary_page == $v->id){
						$page = $v;
					}
				}
			}
			$view = 'templates.'.$page->template.'.index';
			//echo $view;die;
		}

		return view($view,compact('board','page','pages','url','btns','link','omnitrixBoards'));

	}


    public function train(Board2 $boardObj)
    {

        $boards = $boardObj->all();

        return view('frontend.train.train',compact('boards'));
    } 

}