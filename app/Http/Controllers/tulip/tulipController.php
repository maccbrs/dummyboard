<?php namespace App\Http\Controllers\Tulip;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Tables;
use DB;

class tulipController extends Controller
{

	public function index(){

		$startDate = date("Y-m-d H:i:s");
		$endDate = date("Y-m-d H:i:s" ,strtotime("-3 months", strtotime($startDate)));
 
		$calls = DB::table('campaign_data')
			->select('id', 'campaign_id', 'lob', 'contents', 'created_at', 'updated_at')
			->whereBetween('updated_at', [ $endDate, $startDate])
			->orderBy('id', 'desc')
			->get();


		$dateHolder[] = "";

		foreach ($calls as $key => $call) {

			$date = date("Y-m-d" ,strtotime($call->created_at));
			$campaignData[$call->campaign_id] = $call->lob;

			if($date != 0){

				$callHolder[$date][$key]['id'] = $call->id;
				$callHolder[$date][$key]['campaign_id'] = $call->campaign_id;
				$callHolder[$date][$key]['lob'] = $call->lob;
				
				$callHolder[$date][$key]['created_at'] = $date;
				$callHolder[$date][$key]['updated_at'] = $call->updated_at;

				if(!empty($call->contents)){

		            $html_data = Tables::cleaner($call->contents);
		            $callHolder[$date][$key]['contents'] = $html_data;
		        } 
			}
		}

		return view('tulip.home.index',compact('calls', 'callHolder', 'campaignData'));
	}

	public function generate($id){
        
        $startDate = date("Y-m-d H:i:s");
		$endDate = date("Y-m-d H:i:s" ,strtotime("-3 months", strtotime($startDate)));

        $calls = DB::table('campaign_data')
			->select('id', 'campaign_id', 'lob', 'contents', 'created_at', 'updated_at')
			->whereBetween('updated_at', [ $endDate, $startDate])
			->where('campaign_id',$id)
			->orderBy('id', 'desc')
			->get();

		$dateHolder[] = "";

		foreach ($calls as $key => $call) {

			$date = date("Y-m-d" ,strtotime($call->created_at));

			if($date != 0){

				$callHolder[$date][$key]['id'] = $call->id;
				$callHolder[$date][$key]['campaign_id'] = $call->campaign_id;
				$callHolder[$date][$key]['lob'] = $call->lob;
				
				$callHolder[$date][$key]['created_at'] = $date;
				$callHolder[$date][$key]['updated_at'] = $call->updated_at;

				if(!empty($call->contents)){

		            $html_data = Tables::cleaner($call->contents);
		            $callHolder[$date][$key]['contents'] = $html_data;
		        } 
			}
		}

        return view('tulip.home.generate',compact('calls', 'callHolder'));

    }


}