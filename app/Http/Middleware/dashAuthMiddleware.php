<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;

use Closure;

class dashAuthMiddleware
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $allowed = ['admin','operation','trainer','reportanalyst','researcher'];
        if(!in_array($this->auth->user()->user_type,$allowed)){
            return redirect()->route('tulip.index');
        }
        return $next($request);
    }
} 
