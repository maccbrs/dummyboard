<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardDataTbls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lead_id',50);
            $table->string('phone_number',50);
            $table->string('user',100);
            $table->string('campaign_id',50);
            $table->string('lob',50);
            $table->text('contents');
            $table->string('status',10);
            $table->integer('active')->default(1);  
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_data');
    }
}
