<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPublishedPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('published_pages', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('board_id');
            $table->string('campaign_id');
            $table->string('title',100);
            $table->string('template',50);
            $table->text('contents');
            $table->integer('status')->default(1); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() 
    {
        Schema::drop('published_pages');
    }
}
