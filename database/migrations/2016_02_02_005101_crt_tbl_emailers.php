<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrtTblEmailers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('to');
            $table->string('from');
            $table->string('cc');
            $table->string('bcc');
            $table->text('subject');
            $table->text('content');
            $table->integer('status')->default(1);
            $table->integer('users_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emailers');
    }
}
