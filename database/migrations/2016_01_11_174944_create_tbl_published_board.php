<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPublishedBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('published_boards', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('campaign_id')->unique();
            $table->string('primary_page'); 
            $table->text('options');
            $table->string('to');
            $table->string('from', 60);
            $table->string('cc');
            $table->string('lob', 100);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('published_boards');
    } 
}
