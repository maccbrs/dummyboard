<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUsersBoardsCon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_user_campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->string('campaign_id',50);
            $table->string('reports');
            $table->string('email');
            $table->integer('status')->default(1); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('con_user_campaign');
    }
}
