<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('campaign_id')->unique();
            $table->string('primary_page'); 
            $table->text('options');
            $table->string('to');
            $table->string('test_email');
            $table->string('from', 60);
            $table->text('cc');
            $table->string('lob', 100);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boards');
    }
}
