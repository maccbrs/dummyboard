<?php

use Illuminate\Database\Seeder;
use App\Model\New_campaign;

class create_seeder_new_campaign extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create(); 
        New_campaign::truncate();

        for($x = 0; $x < 10; $x++ ){
	        New_campaign::create([  
	            'campaign_id' => $faker->firstName(),  
	            'lob' => $faker->firstName()
	        ]);  
        }  
    }
}
