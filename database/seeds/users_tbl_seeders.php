<?php

use Illuminate\Database\Seeder;
use App\User;

class users_tbl_seeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        User::truncate();
        User::create([  
            'name' => 'admin',  
            'email' => 'marlon.bernal@magellan-solutions.com', 
            'user_type' => 'admin',  
            'password' => bcrypt('admin')
        ]);               
    }
}
