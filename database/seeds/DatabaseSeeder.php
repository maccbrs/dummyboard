<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(users_tbl_seeders::class);
        $this->call(create_seeder_new_campaign::class);
        
        Model::reguard();
    }
}
