@extends('templates.gemini.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 

      foreach ($contents as $k => $v) {

        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options); 

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

  $fname= !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $lname= !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $user= !empty($_GET["user"]) ?  $_GET["user"] : ""; 

?>


<?php

$lead_info = json_encode($_GET);
$lead = json_decode($lead_info);

$fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
?>
@section('custom_head')


<style>
 .well{background-color: <?= (isset($arr_contents['content-4'])?parse_input($arr_contents['content-4'],$url):'#e3e3e3') ?> !important;}

 .intakeForm-darken>header {
    border-color: #001633;
    background: <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'#001633') ?>!important;
    color: #fff;
    height: 70px;
    font-family: Impact, Charcoal, sans-serif !important;
}
.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'#659EC7') ?>!important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/
}

.populate {
    margin-top: 6px !important;
    background-color: <?= (isset($arr_contents['content-4'])?parse_input($arr_contents['content-4'],$url):'#e3e3e3') ?> !important;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
</style>

<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    /*$(":text").attr("required", true);*/
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");

    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>



    <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">

            <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
              <input type="hidden" name="pageid" value="{{$page->id}}">
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
              <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
              <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
              <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
              <input type="hidden" name="email_subject" value="{{$email_subject}}">
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">  
              <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
              <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">
              @if($page['campaign_id'] != '59b96bfe41291')
              <input type="hidden" name="disposition" value="">
              @endif
<!--               @if($page['campaign_id'] == '5a0481d9cfa4d')
              <input type="hidden" name="reply_to" value="cleobert.delacruz@magellan-solutions.com">
              @endif -->
              <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">       

            <div class="col-md-7">
              <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">
                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if(($page['campaign_id'] == '57f544bb414ed')  || ($page['campaign_id'] == '59b96bfe41291'))
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif
                        </div>
                        
                    </div>
                </div>                
            </div>

            

            <div class="col-md-5 " style = "max-height: 100%;position:fixed;top:10px; right:10px ;">
              @if($link == 'preview')
            
                <div class="pull-right test-email">
                    <label>test email:</label>
                    <input type="text" name="test_email">
                    <br>
                </div>
                <br>
                <br>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right test-email">
                  <label>test email:</label>
                  <input type="text" name="test_email">
                  <br>
                </div>
                <br>
                <br>
             @endif

              <div style="border: solid 1px; border-color: gainsboro; padding: 10px; max-height: 800px; overflow-y: auto; overflow-x: hidden;">

            {{ Session::get('flash_notification.message') }}

              <div class ="populate modal-gemini">

                <?= (isset($arr_contents['content-2'])?parse_input($arr_contents['content-2'],$url):'') ?>

              </div>
          
              @foreach($pages as $p)

                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    @if($p->id != $page->id && in_array($p->id,$btns))
                        
                      <a data-toggle='modal' data-target='#{{$p->id}}' class='btn modal-gemini' style="height: 30px;"><p style="font-size: 15px;">{{$p->title}}</p></a>

                      <div class="modal fade" id='{{$p->id}}' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content "> 
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" >&times;</button>
                                      <h4 class="modal-title">{{$p->title}}</h4>
                                  </div>
                                  <div class="modal-body " style = "padding-top: 0px!important;">  
                                    <div class = "populate">
                                    @if($p->template == 'customform')
                                        {!!view('templates.customform.index',['page' => $p,'url' => $url])!!}
                                    @else
                                      <?php 
                                        if(!empty($p->contents)){
                                              $extractor = json_decode($p->contents);
                                            
                                              foreach ($extractor as $k2 => $v2) {

                                                $arr_contents2[$k2] = $v2;
                                              }
                                          }
                                        ?>
                                                
                                        <?= (isset($arr_contents2['content-l'])?parse_input($arr_contents2['content-l'],$url):'') ?>
                                    @endif

                                      </div>          
                                  </div>
                              </div>
                          </div>
                      </div>
                                                  


                    @endif

              @endforeach


                @if(!empty($arr_contents['content-3']))
                  <div class="populate">
                    <?php
                      $content_dispo = $arr_contents['content-3'];
                      $dispo_a = preg_replace("/\s+/", " ",$arr_contents['content-3']);
                      $dispo_b = explode(";",$dispo_a);
                    ?>

                    <p><b>Disposition :</b>
                    <select class="form-control pull-right" required style ="width:300;" name = "disposition">
                      <option value="" disabled>Please select</option>
                        @foreach ($dispo_b as $key2 =>$value2)
                          @if(!empty($value2))
                            <?php  $trimmed2 = explode(":", trim($value2)); ?>
                              <option value="{{$trimmed2[0]}}" >{{$trimmed2[1]}}</option>
                          @endif
                        @endforeach
                    </select></p>
                  </div>
                @endif

            </div>
        </div>
    </div>
    </div>
</div>

          @endif
        </div>
      </div>
  @endif

</form>



@endsection

@if(($page['campaign_id'] != '59c2f863d3575'))
<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}



.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

/*.well{

  background-color:#e3e3e3!important;
}*/

.modal-backdrop{

  left:inherit !important;
}

.modal-gemini{

  margin-bottom: 2px!important;
}

</style>
@endif

@if(($page['campaign_id'] == '59c2f863d3575'))
<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


/*.intakeForm-darken>header {
    border-color: #001633;
    background: #001633;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}*/


.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}



/*.well{

  background-color:#cce0f9!important;
}*/

.modal-backdrop{

  left:inherit !important;
}

.modal-gemini{

  margin-bottom: 2px!important;
}

</style>
@endif