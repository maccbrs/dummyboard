<div class="modal fade" id="jobTitlesModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title">No Contact - Immediate Call Dispo</h4>
            </div>
            <div class="modal-body">
                <!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
                <p>Information Technology (IT) Job Titles</p>
                <p><strong>A - D</strong></p>
                <ul>
                    <li>Application Developer</li>
                    <li>Application Support Analyst</li>
                    <li>Applications Engineer</li>
                    <li>Associate Developer</li>
                    <li>Chief Technology Officer (CTO)&nbsp;</li>
                    <li>Chief Information Officer (CIO)&nbsp;</li>
                </ul>
                <ul>
                    <li>Computer and Information Systems Manager</li>
                    <li>Computer Systems Manager</li>
                    <li>Customer Support Administrator</li>
                    <li>Customer Support Specialist</li>
                    <li>Data Center Support Specialist</li>
                    <li>Data Quality Manager</li>
                    <li><a href="https://www.thebalance.com/database-administrator-job-description-salary-and-skills-2061775">Database Administrator</a></li>
                    <li>Desktop Support Manager</li>
                    <li>Desktop Support Specialist</li>
                    <li>Developer</li>
                    <li>Director of Technology</li>
                </ul>
                <p><strong>E - N</strong></p>
                <ul>
                    <li><a href="https://www.thebalance.com/front-end-web-developer-cover-letter-and-resume-examples-2060131">Front End Developer</a></li>
                    <li>Help Desk Specialist</li>
                </ul>
                <ul>
                    <li>Help Desk Technician</li>
                    <li>Information Technology Coordinator</li>
                    <li>Information Technology Director</li>
                    <li>Information Technology Manager</li>
                    <li>IT Support Manager</li>
                    <li>IT Support Specialist</li>
                    <li>IT Systems Administrator</li>
                    <li>Java Developer</li>
                    <li>Junior Software Engineer</li>
                    <li>Management Information Systems Director</li>
                    <li>.NET Developer</li>
                    <li>Network Architect</li>
                    <li>Network Engineer</li>
                </ul>
                <ul>
                    <li>Network Systems Administrator</li>
                </ul>
                <p><strong>P - S</strong></p>
                <ul>
                    <li><a href="https://www.thebalance.com/computer-programmer-job-description-salary-and-skills-2061823">Programmer</a></li>
                    <li>Programmer Analyst</li>
                    <li>Security Specialist</li>
                    <li>Senior Applications Engineer</li>
                    <li>Senior Database Administrator</li>
                    <li>Senior Network Architect</li>
                    <li>Senior Network Engineer</li>
                    <li>Senior Network System Administrator</li>
                    <li>Senior Programmer</li>
                    <li>Senior Programmer Analyst</li>
                    <li>Senior Security Specialist</li>
                    <li>Senior Software Engineer</li>
                    <li>Senior Support Specialist</li>
                    <li>Senior System Administrator</li>
                    <li>Senior System Analyst</li>
                    <li>Senior System Architect</li>
                    <li>Senior System Designer</li>
                    <li>Senior Systems Analyst</li>
                    <li>Senior Systems Software Engineer</li>
                    <li>Senior Web Administrator</li>
                    <li>Senior Web Developer</li>
                    <li>Software Architect</li>
                    <li><a href="https://www.thebalance.com/software-developer-job-description-salary-and-skills-2061833">Software Developer</a></li>
                    <li>Software Engineer</li>
                    <li>Software Quality Assurance Analyst</li>
                    <li><a href="https://www.thebalance.com/computer-support-specialist-526075">Support Specialist</a></li>
                    <li>Systems Administrator</li>
                    <li><a href="https://www.thebalance.com/computer-systems-analyst-526001">Systems Analyst</a></li>
                    <li>System Architect</li>
                    <li>Systems Designer</li>
                </ul>
                <ul>
                    <li>Systems Software Engineer</li>
                </ul>
                <p>​<strong>T - Z</strong></p>
                <ul>
                    <li>Technical Operations Officer</li>
                    <li>Technical Support Engineer</li>
                    <li><a href="https://www.thebalance.com/computer-support-specialist-526075">Technical Support Specialist</a></li>
                    <li>Technical Specialist</li>
                    <li>Telecommunications Specialist</li>
                    <li>Web Administrator</li>
                    <li><a href="https://www.thebalance.com/web-developer-job-description-salary-and-skills-2061841">Web Developer</a></li>
                    <li>Webmaster</li>
                </ul>
            </div>
        </div>
    </div>
</div>