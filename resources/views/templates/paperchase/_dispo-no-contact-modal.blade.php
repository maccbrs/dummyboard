

<div class="modal fade" id="dispoModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title">No Contact - Immediate Call Dispo</h4>
            </div>
            
            <div class="modal-body">
                <p><b>Disposition :</b>

                <select id="selectModalDispo"  class="form-control pull-right" style="width: 300px" name="disposition">
                    <option value="" style="display:none">Please select</option>
                    <option value="LQ">LQ - Lead Qualified</option>
                    <option value="CB">CB - Call Back</option>
                    <option value="NI">NI - Not Interested</option>
                    <option value="HU">HU - Hang Up</option>
                    <option value="NA">NA - No Answer</option>
                    <option value="AM">AM - Answering Machine</option>
                    <option value="HUP">HUP - Hang Up During Pitch</option>
                    <option value="DMNA">DMNA - Decision Maker Not Available</option>
                    <option value="WN">WN - Wrong Number</option>
                    <option value="BUSY">BUSY - Busy tone</option>
                    <option value="DNC">DNC - Do not call</option>
                     <option value="DN">DN - Disconected No</option>
                    <option value="FM">FM - Fax  Machine</option>
                    <option value="LB">LB - Language Barrier</option>
                    <option value="BC">BC - Bad Connection</option>
                    <option value="EMAIL">EMAIL - Email</option>
                    <option value="FU">FU - For Update</option>
                </select>
                </p>
            </div>

            <div class="modal-footer">
                <p>
                    <button type="submit" class="btn btn-sm">Submit</button>

                </p>
                <button type="submit" onclick="$('#selectMainDispo').attr('required', false);">Submit</button>
            </div>
        </div>
    </div>
</div>



