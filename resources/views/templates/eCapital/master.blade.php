
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/first/custom.css')}}" rel="stylesheet">
  </head>
  <body>

    @yield('content')

    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">
      
 

        $( "#phone" ).keyup(function() {
          counter = $('#phone').val().length;
          phone_num = $('#phone').val();


          if(counter > 10){

            var str = phone_num.substring(0, phone_num.length-1);
            $('#phone').val(str);

          }

        });

        $( ".capitalize" ).keyup(function() {
            
            firstLetter = $(this).val().charAt(0).toUpperCase();
            restWord = $(this).val().slice(1);
            
            capitalized = firstLetter.concat(restWord); 

            $(this).val(capitalized);
            

        });

        $( ".lastName" ).keyup(function() {
            
            firstLetter = $('.lastName').val().charAt(0).toUpperCase();
            restWord = $('.lastName').val().slice(1);
            
            capitalized = firstLetter.concat(restWord); 

            $('.lastName').val(capitalized);
            

        });

        $('.title').keyup(function() {
              var arr = $(this).val().split(' '); 
              var result = ""; 
              for (var i=0; i<arr.length; i++){ 
                  result += arr[i].substring(0,1).toUpperCase() + arr[i].substring(1);
                  if (i < arr.length-1) {
                      result += ' ';
                  }
              } 
              $(this).val(result); 
          });

         $('.company').keyup(function() {
              var arr = $(this).val().split(' '); 
              var result = ""; 
              for (var i=0; i<arr.length; i++){ 
                  result += arr[i].substring(0,1).toUpperCase() + arr[i].substring(1);
                  if (i < arr.length-1) {
                      result += ' ';
                  }
              } 
              $(this).val(result); 
          });

        $( "#phone" ).keyup(function() {
          counter = $('#phone').val().length;
          phone_num = $('#phone').val();


          if(counter > 10){

            var str = phone_num.substring(0, phone_num.length-1);
            $('#phone').val(str);

          }

        });

        $( ".gross" ).keyup(function() {
          if(!$.isNumeric($('.gross').val())) {
            $(this).val("");
          }
          

        });

        $( ".phone" ).keyup(function() {
          if(!$.isNumeric($('.gross').val())) {
            $(this).val("");
          }
          

        });



        $( "#phone" ).blur(function() {
          counter = $('#phone').val().length;
          phone_num = $('#phone').val();


          if(counter < 10){

            
            $('#phone').val("");

          }


        });


$('#submit').on("click",function() { 

      first_name = $('#first_name').val();
      last_name = $('#last_name').val();
      title = $('#title').val();
      email = $('#email').val();
      company = $('#company').val();
      street = $('.street').val();
      city = $('#city').val();
      zip = $('#zip').val();
      state_code = $('#state_code').val();
      com_start = $('.com_start').val();
      URL = $('#URL').val();
      phone = $('#phone').val();
      gross = $('.gross').val();
      test_email = $('#test_email').val();
      from = $('#from').val();
      to = $('#to').val();
      cc = $('#cc').val();
      bcc = $('#bcc').val();


      lob = $('#lob').val();
      user = $('#user').val();
      phone_number = $('#phone_number').val();
      lead_id = $('#lead_id').val();
      campaign_id = $('#campaign_id').val();
      options = $('#options').val();

      link1 = "generate";

      dataString ="test";

      link = link1.concat(dataString);

     $('.gross').keyup(function () {
        if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
           this.value = this.value.replace(/[^0-9\.]/g, '');
        }
      });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
          url: "/generate",
          type: "POST",
          data: {
              _token: CSRF_TOKEN,
              lob: lob,
              user: user,
              lead_id: lead_id,
              campaign_id: campaign_id,
              options: options,
              first_name: first_name,
              last_name: last_name,
              title: title,
              email: email,
              company: company,
              city: city,
              zip: zip,
              state_code: state_code,
              URL: URL,
              phone: phone,
              gross: gross,
              test_email: test_email,
              from: from,
              cc: cc,
              bcc: bcc, 
              from: from,
              to: to

          },
          success:function(data) {
            console.log('ok');
            $('#form').submit();
          },
          error:function(data){
             console.log('failed');
             $('#form').submit();
          }
      });

});//end----


</script>

      <style>

          .hideThis{
            display:none;

          }

          .main-bx{
            text-align: center;
            padding: 10px

          }

          input{

            margin:10px;
          }

      </style>

  </body>
</html>