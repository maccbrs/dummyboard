@extends('templates.third.master')
@section('title', 'Page Title')

<?php 



  $campaign = $page['campaign_id'];


?>

<?php

if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}


$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : " " ;
  $phone_number = !empty($_GET['phone_number']) ? $_GET['phone_number'] : " ";
  $user = !empty($_GET['fullname']) ? $_GET['fullname'] : "test";
?>

@section('custom_head')
<!-- /* this is a comment */ -->
<?php echo $page->custom_head ?>

<style>
 .bx{background-color: <?= (isset($arr_contents['content-2'])?parse_input($arr_contents['content-2'],$url):'') ?> !important;}
</style>
<script>


	</script>
@endsection

@section('content')
<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="pageid" value="{{$page->id}}">
    <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
    <input type="hidden" name="email_subject" value="{{$email_subject}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
    <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

    <div class="container">
      <div class="top">
        
          @if($link == 'preview')
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @elseif($link == 'train' && Auth::check())
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @endif
          
           {{ Session::get('flash_notification.message') }}

      </div>

      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx">

                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

          </div>
        </div>

        <div class="col-md-4 sidebar ">
           <div class="bx o-l">
            
              <div class ="linkDiv">
                  
                @if($campaign == "56ff303456bab")


                  <p><a href= "<?php echo $url. '/570ee783ddf6f/422?user='.$user?>"><input name="MNOS" type="button" value="MNOS" /> </a></p>

                  <p><a href="<?php echo $url. '/57101bd74a14d/467?user='.$user?>"><input name="QHEART" type="button" value="QHEART" /> </a></p>

                  <p><a href="<?php echo $url. '/57101c17c3a0b/456?user='.$user?>"><input name="CURCUMIN" type="button" value="CURCUMIN" /> </a></p>

                  <p><a href="<?php echo $url. '/57101c7d2183d/450?user='.$user?>"><input name="SUPREME BRAIN" type="button" value="SUPREME BRAIN" /> </a></p>

                  <p><a href="<?php echo $url. '/57101d50b098e/477?user='.$user?>"><input name="REJUVENATION" type="button" value="REJUVENATION" /> </a></p>


                  <p><a href="<?php echo $url. '/57101d9db2f39/472?user='.$user?>"><input name="TRANSFORMATION" type="button" value="TRANSFORMATION" /> </a></p>

                  <p><a href="<?php echo $url. '/57101cfd6a2e3/645?user='.$user?>"><input name="REJUVAFLEX" type="button" value="REJUVAFLEX" /> </a></p>

                  <p><a href="<?php echo $url. '/5714fdb2667d1/443?user='.$user?>"><input name="DYFLOGEST" type="button" value="DYFLOGEST" /> </a></p>

                @endif

                @if($campaign == "571e4c72a2370")

                  <p><a href="<?php echo $url. '/571e4d7153706/617?user='.$user?>"><input name="MNOS" type="button" value="MNOS" /></a></p>

                  <p><a href="<?php echo $url. '/571e4e0b94b44/551?user='.$user?>"><input name="REJUV" type="button" value="REJUV" /></a></p>

                  <p><a href="<?php echo $url. '/57b51a7f5ae78?user='.$user?>"><input name="DYFLO" type="button" value="DYFLO" /></a></p>

                  <p><a href="<?php echo $url. '/571e4c72a2370/988?user='.$user?>"><input name="LEGAL GUIDE" type="button" value="LEGAL GUIDE" /></a></p>

                @endif

				
                @if($campaign == "571a577b1cd2f")


                  <p><a href="<?php echo $url. '/571a63ea42803?user='.$user?>"><input name="TTNC" type="button" value="TTNC" /></a></p>

                  <p><a href="<?php echo $url. '/571a5ac93a0f4?user='.$user?>"><input name="OMC" type="button" value="OMC" /></a></p>

                @endif

				<?php 
				/* - # -   

                @if($campaign == "571a577b1cd2f")

                  <p><a href="<?php echo $url. '/571a63ea42803'?>"><input name="OMC" type="button" value="OMC" /></a></p>

                  <p><a href="<?php echo $url. '/571a5ac93a0f4'?>"><input name="TTNC" type="button" value="TTNC" /></a></p>

                @endif

				*/  
				?>
				
				
              </div>
            </div>
        </div>
        @endif
      </div>

    </div>
</form>   

@endsection
<!--
<script type="text/javascript">

$( document ).ready(function() {
   $(":radio[value=yes]").on('click',function(){
     $('select').attr('disabled', 'disabled');
   });
   $(":radio[value=no]").on('click',function(){
     $('select').prop( "disabled", false );
   });       
});
</script>-->

<style>
  input[type=button], button {
    display: inline-block;
    margin: 5px 20px;
    width: 285px;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

  p , .linkDiv{

    text-align:center;
  }

</style>

