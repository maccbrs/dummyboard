<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
   
    <link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
  </head>
  
    <!-- <script src="path/to/timezones.full.js"></script> -->
    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script type='text/javascript' src="{{URL::asset('js/timezones.full.js')}}"></script> 
    <script type="text/javascript">
        
        function InputToggle() {
         $( "div" ).each(function( i ) {
                        if ( $(this).is(":visible"))  {
                           $(this).find('input').prop( "disabled", false );
                        } else {
                          $(this).find('input').prop( "disabled", true );
                        }
                      });
            }

      $( document ).ready(function() {

          $( "form" ).submit(function( event ) {
            
               var chckboxarray_val = [];
               var chckboxarray_name = [];
                $('input:checked').each(function() {
                    chckboxarray_val.push($(this).val());
                    chckboxarray_name.push($(this).prop("name"));
                });
              
               for(var x=0;x< chckboxarray_name.length ; x ++)
                   {
                       if(x == 0)
                           {
                               var temp_name = chckboxarray_name[0];
                               var temp_val = chckboxarray_val[0];
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                           }
                       
                       else if(chckboxarray_name[x - 1] == chckboxarray_name[x] )
                           {
                               temp_val = temp_val + ',' +  chckboxarray_val[x];
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                              // alert(temp_val);
                           }
                       else
                           {
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                                temp_name = chckboxarray_name[x];
                                temp_val = chckboxarray_val[x];
                           }
                   }

            });
          


        campaign = $('#campaign_id').val();

        if(campaign == '583ca9adf1b8b'){

          $('.input-date').datetimepicker({
            format:' F d Y H:i',
          });

          $(".select-timezone").change(function(){
            var datelist = $(".inputdate").val();

            var timezone = $(".select-timezone").val();
              if(datelist){
                var m = moment.utc(datelist, "MMMM D YYYY HH:mm");
                var convertedTime = m.tz("America/Chicago").format('LLLL');
                var datelist = $(".ConvertedDate").val(convertedTime);
              }
          });

        }

    });

  </script>

     @yield('custom_head')

  <body>

    @yield('content')


  </body>
</html>