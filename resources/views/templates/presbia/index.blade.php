@extends('templates.presbia.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 
      foreach ($contents as $k => $v) {
        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  
  $fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
  $CompanyName = !empty($_GET["CompanyName"]) ?  $_GET["CompanyName"] : "";
  $SICCODE = !empty($_GET["SICCODE"]) ?  $_GET["SICCODE"] : "";  
  $SICDESC = !empty($_GET["SICDESC"]) ?  $_GET["SICDESC"] : ""; 
  $GenericCode = !empty($_GET["GenericCode"]) ?  $_GET["GenericCode"] : "";  
  $GenericIndustry = !empty($_GET["GenericIndustry"]) ?  $_GET["GenericIndustry"] : "";  
  $ActualEmpize = !empty($_GET["ActualEmpize"]) ?  $_GET["ActualEmpize"] : "";  
  $ActualAnnualSales = !empty($_GET["ActualAnnualSales"]) ?  $_GET["ActualAnnualSales"] : "";  
  $first_name = !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $middle_initial = !empty($_GET["middle_initial"]) ?  $_GET["middle_initial"] : ""; 
  $last_name = !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $Level = !empty($_GET["Level"]) ?  $_GET["Level"] : ""; 
  $Department = !empty($_GET["Department"]) ?  $_GET["Department"] : "";
  $Position = !empty($_GET["Position"]) ?  $_GET["Position"] : "";
  $vendor_id = !empty($_GET["vendor_id"]) ?  $_GET["vendor_id"] : "";
  $phone_number = !empty($_GET["phone_number"]) ?  $_GET["phone_number"] : "";
  $DID = !empty($_GET["DID"]) ?  $_GET["DID"] : "";   
  $email = !empty($_GET["email"]) ?  $_GET["email"] : ""; 
  $ALTEMAILADDRESS = !empty($_GET["ALTEMAILADDRESS"]) ?  $_GET["ALTEMAILADDRESS"] : ""; 
  $Website = !empty($_GET["Website"]) ?  $_GET["Website"] : "";
  $address1 = !empty($_GET["address1"]) ?  $_GET["address1"] : "";
  $Country = !empty($_GET["Country"]) ?  $_GET["Country"] : "";
  $postal_code = !empty($_GET["postal_code"]) ?  $_GET["postal_code"] : "";
  $DateLastUpdated = !empty($_GET["DateLastUpdated"]) ?  $_GET["DateLastUpdated"] : "";
  $Stage = !empty($_GET["Stage"]) ?  $_GET["Stage"] : "";
  $Notes = !empty($_GET["Notes"]) ?  $_GET["Notes"] : "";
 
// paperchase hidden fields   

  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : "";
  $campaign_id = !empty($_GET['campaign']) ? $_GET['campaign'] : "";
  $user = !empty($_GET['user']) ? $_GET['user'] : "";
  $session_id = !empty($_GET['session_id']) ? $_GET['session_id'] : "";
  $city = !empty($_GET['city']) ? $_GET['city'] : "";
  

?>
@section('custom_head')


<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    $(":text").attr("required", false);
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");
/*
    $("#NoContactBtn").on("click", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", true);
        $("#selectMainDispo").attr("required", false);
    });*/


    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
         
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>

    <form id="mainForm" enctype="multipart/form-data" action="{{url('data/save_outbound/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>

      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="pageid" value="{{$page->id}}">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="Toll_Free_Number" value="{{$vendor_id}}">
      
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">  
      <input type="hidden" name="reference_id" value="{{$lead_id}}">

      <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-12">

              @if($link == 'preview')
            
                <div class="pull-right">
                    <label>test email:</label>
                    <input type="text" class = "form-control" name="test_email">
                    <br>
                </div>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right">
                  <label>test email:</label>
                  <input type="text" class = "form-control" name="test_email">
                  <br>
                </div>

            @endif

                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">
                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif
                        </div>

                        <div class="form-group populate">
                            <label for="pwd">Clinics:</label>
                            <select name="PreferredClinic" id="Clinic" style="width:550px;" class = "form-control"> 
                              <option value="">- - - -</option>
                              <option value="10046">서울밝은세상 - 서울시 강남구 </option>
                              <option value="10052">부산밝은세상 - 부산시 부산진구 </option>
                              <option value="10033">삼성서울병원 - 서울시 강남구 </option>
                              <option value="10047">비앤빛 강남밝은세상 - 서울시 서초구 </option>
                              <option value="10049">분당서울안과 - 성남시 분당구 </option>
                              <option value="10058">압구정안과의원 - 서울시 강남구 </option>
                              <option value="10048">압구정 성모안과 - 서울시 강남구 </option>
                              <option value="10051">새빛 안과병원 - 고양시 일산동구 </option>
                            </select>
                        </div>
                      
                        <iframe src="https://clinics.presbia.com/koreaclinicsbypostalcode.aspx">
                          <p>Your browser does not support iframes.</p>
                        </iframe>
                        
                        <input type="hidden" name="lead_id" value="{{$lead_id}}">
                        <input type="hidden" name="campaign_id" value="{{$campaign_id}}">
                        <input type="hidden" name="user" value="{{$user}}">
                        <input type="hidden" name="session_id" value="{{$session_id}}">


                        <br><br>

                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="col-md-4">
                            <input type="submit" class = "form-control" value="Submit">
                          </div>
                          <div class="col-md-4"></div>
                        </div>
                          <br>
                    </div>

                </div>

            </div>


    </div>
    </div>


</div>

          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}

iframe{

  width: 100%;
  height: 40%;

}

.intakeForm-darken>header {
    border-color: #4169e1;
    background: #4169e1;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #4169e1 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

.populate {
    margin-top: 6px !important;
    background-color: #e3e3e3;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
.well{

  background-color:#e3e3e3!important;
}


</style>
