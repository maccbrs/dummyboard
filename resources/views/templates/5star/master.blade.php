
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/first/custom.css')}}" rel="stylesheet">
  </head>
  <body>
    @yield('content')

    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">


        $( document ).ready(function() {

        //   $(":radio[value=yes]").on('click',function(){
        //   $('select').html('');
        //   $('select').append('<option value=""> </option>');
        // });

        // $(":radio[value=no]").on('click',function(){
        //   $('select').html('');
        //   $('select').prop( "disabled", false );
        //   $('select').append('<option value="" disabled selected>Please Select</option>');
        //   $('select').append('<option value="General Inquiry">General Inquiry</option>');
        //   $('select').append('<option value="Hang Up">Hang Up</option>');
        //   $('select').append('<option value="Wrong Number">Wrong Number</option>');
        //   $('select').append('<option value="No Answer">No Answer</option>');

    
          campaign = $('#5star').val();
          
          if(campaign == "5748837f750c1"){
     
            $("input[name=Product]").on('change', function() {

                if ($("#product").is(':checked')) {

                  $('#otherdiv').remove();

                 $('#checkbox-hide').append( "<div id='netproduct'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='BI Analyzer' type='checkbox'> <strong><span style='font-size:24px'>BI Analyzer</span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Campaign Management' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Campaign Management </span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Central Profile' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Central Profile</span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='CRM Reporting' type='checkbox'></span></strong> <strong><span style='font-size:24px'>CRM Reporting</span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Guest Communicator' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Guest Communicator</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Guest Loyalty' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Guest Loyalty</span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Guest Member Portal' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Guest Member Portal</span></strong><br><strong><span style='font-size:24px'> <input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Guest Newsletter Portal ' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Guest Newsletter Portal</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Profile Distribution' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Profile Distribution</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Profile Lookup' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Profile Lookup</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Profile Management' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Profile Management</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Profile Manager ' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Profile Manager</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module'  class = 'checkbox-net-hotel' value='Sales' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Sales</span></strong><br><strong><span style='font-size:24px'><input name='Net Hotel Module' class = 'checkbox-net-hotel' value='Web Admin' type='checkbox'></span></strong> <strong><span style='font-size:24px'>Web Admin</span></strong><strong> </strong></p><input name='Net Hotel Module' class = 'input-text nethoteltext' type = 'hidden'  value=''></div>" );
                }

                else if ($("#other-condition").is(':checked')) {

                  $('#netproduct').remove();
                  $( "#test" ).append( "<div id ='otherdiv'><input name='other_application' id='input-hide' type='text' class = 'input-text' style = 'width:32%;text-align:center'></div>" );

                }else{

                  $('#otherdiv').remove();
                  $('#netproduct').remove();

                }

                $('.checkbox-net-hotel').each(function() {

                $(this).on("click", function(){

                  if (this.checked) {

                    nethoteltext = $('.nethoteltext').val();

                    $('.nethoteltext').val("");
                    netholder = $(this).val();

                    slash = "/";

                    valueholder = netholder.concat(slash);
                
                    valueholder = valueholder.concat(nethoteltext);

                    $('.nethoteltext').val(valueholder);
                        
                  }else{

                    $('.nethoteltext').val("");

                    $('.checkbox-net-hotel[type=checkbox]').each(function () {       

                      var netholder = (this.checked ? $(this).val() : "");

                        if(netholder != ""){

                          slash = "/";

                          valueholder = netholder.concat(slash);

                          nethoteltext = $('.nethoteltext').val();

                          valueholder = valueholder.concat(nethoteltext);

                          $('.nethoteltext').val(valueholder);

                        }

                    });
                  }

                });
            })
          })
        }
      }); 

      </script>

      <style>

        .hideThis{
          display:none;
        }

        .input-text{

          height: 5%;

        }

        .input-checkbox{

          line-height: 45px;

        }

      </style>

  </body>
</html>