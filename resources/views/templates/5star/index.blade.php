@extends('templates.5star.master')
@section('title', 'Page Title')


<?php


if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}


$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : " " ;
  $phone_number = !empty($_GET['phone_number']) ? $_GET['phone_number'] : " ";
  $user = !empty($_GET['user']) ? $_GET['user'] : "test";
?>

@section('content')

<style type="text/css">
  div.absolute {
    position: absolute;
    top: 0px;
} 
</style>

<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="pageid" value="{{$page->id}}">
    <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
    <input type="hidden" name="email_subject" value="{{$email_subject}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
    <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

    <div class="container">
      <div class="top">
        
        @if($link == 'preview')
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" name="test_email">
         </div>
        @elseif($link == 'train' && Auth::check())
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" name="test_email">
         </div>
        @endif
        
         {{ Session::get('flash_notification.message') }}

      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx" s>
                <input type="hidden" id="5star" value= "<?php echo $page['campaign_id'] ?>"
                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>
                <div style ="text-align:center;font-size: 28px; font-weight: 700; "> 
                  <br><br><br>
                  Thank you for calling Serenata Intraware Help Desk. 

                  My name is _______.     How can I help you? <br><br>

                  To better assist you with that <br><br>

                  May I have the name of the property/hotel name?

                    <input name="Property_Name" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>
                 
                  Your first and last name?<br>

                    <input name="Customer_Name" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>

                  Your position in the company?<br>

                    <input name="Position" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>

                  How about your phone number?<br>

                    <input name="Contact_Number" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>

                  And your email address?<br>

                    <input name="Email_Address" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>

                    In order to understand the problem better I would need to ask you additional questions. <br>

                    Your call is referring to which product? <br>

                    <div style ="text-align:left; margin-left:12% ;">

                      <input name="Product" class = "input-text" type="radio" id ="product" value="Net Hotel" required /> Net Hotel <span classstyle="font-size:18px">( ask &quot;which application are you referring to?&quot;)</span><br>

                          <div style="display: block; margin-left: 27px;" id="checkbox-hide">
                            
                          </div>

                      <input name="Product" type="radio" value="@mail" /><span style="font-size:24px;" class = "input-checkbox" required>@mail</span><br>

                      <input name="Product" type="radio" value="Trust OXI Interface" /><span style="font-size:24px;" class = "input-checkbox" required>Trust OXI Interface</span><br>

                      <input name="Product" type="radio" id = "other-condition" value="Others" /><span class = "input-checkbox" style="font-size:24px;" required>Others </span><section id = "test"></section><br> 

                    </div><br>

                    When did you start experiencing the reported issue ?<br>

                    <input name="Since_when" class = "input-text" required type="text" style = "width:32%;text-align:center;"><br>

                      Are there any error messages?  <br>

                      <input name="Error Message " value="Yes" type="radio" required> YES <br>

                     <span style ="font-size: 24px;">If YES agent will tell the customer: "Please send screenshots of the error or log files to support@serenata.com using your official email.</span><br>
                      <input name="Error Message " value="No" type="radio" required>No <br><br>

                      <span style="font-size:27px">Did you carry out any modifications in your Network/PMS that might be correlated to the reported issue?</span><br>

                      <input name="Modifications in Network/PMS" value="Yes" type="radio" required>YES<br>
                      <input name="Modifications in Network/PMS" value="No" type="radio" required>NO</br><br>

                      
                                            <span style = "color:red; font-size:24px">
                      PRIORITY CLASSIFICATION <br><br>

                      </span> 
                      <input name="Severenity" value="Level1" type="radio" required>HIGH SEVERITY LEVEL 1: SYSTEM FAILURE: <br>
                      <p style="text-align:center">Operation failure, system is completely inactive, does not respond to input, deletes data or cannot be productively used at all.</p> 
                     
                      <span style = "color:black; font-size:24px">
                      WARM TRANSFER (2:00pm-11:00PM Manila) "Let me go ahead and transfer the call to our Technical Support Team Please stay on the line"<br><br>
                      </span> 

                      <span style = "color:black; font-size:24px">
                      LEAVE A MESSAGE IN THE MAILBOX (11:00PM-2:00PM Manila) "The customer's name is [name of customer], calling from [name of the Hotel] with the phone number [phone number]. The issue is regarding [short description of the issue]<br><br>
                      </span> 
                      <span style = "color:blue;">
                      <p style="text-align:center">1. 1st attempt to call: Munich Office<br>
                        Tel: + 49 89 92 90 03 10</p> 

                      <p style="text-align:center">2. 2nd attempt to call: Mr. Nagy Fawzy Dir. of IT:<br>
                        Tel: + 49 176 851 150 74 or private phone + 49 8024 649 99 60</p> 

                      <p style="text-align:center">3. 3rd attempt to call: Mr. Manuel Deil CTO:<br>
                        Tel: + 49 179 233 59 44 or private phone + 49 89 200 44 229</p> 
           
                      <p style="text-align:center">3. 3rd attempt to call: Mr. Manuel Deil CTO:<br>
                        Tel: + 49 179 233 5950 or private phone + 49 821 408 33 71</p>
                      </span>
                      <input name="Severenity" value="Level2" type="radio" required>MEDIUM SEVERITY LEVEL 2:  SERIOUS PROBLEM: <br>
                      <p style="text-align:center">A problem partially interferes with the use of system, however, processes can still be concluded and no loss of data occurs.</p> 

                      <input name="Severenity" value="Level3" type="radio" required>MEDIUM SEVERITY LEVEL 3:   MINOR PROBLEM: <br>
                      <p style="text-align:center">A problem partially interferes with the use of the system, however can be worked around with reasonable effort. The use of the system is not seriously impeded and all required processes are running. Not business critical to the customer.</p> 

                      <input name="Severenity" value="Level4" type="radio" required>LOW SEVERITY LEVEL 4:  ENHANCEMENT: <br>
                      <p style="text-align:center">There is no problem interfering with the use of major parts of the system. The solution requires new functions or enhancements of the installed products. Not business critical to the customer.</p> 

                    </br>

                      <span style = "color:red; font-size:24px">
                          INCIDENT CLASSIFICATION
                          (Choose the best option based on the criteria provided .There is no need to ask this question from  the customer)
                      </span> <br><br>

                      <input name="Incident Classification" value="IT/Systems/Configuration" type="radio" required>IT/Systems/Configuration <span style="font-size:18px">(all systems related issues e.g. if the user can not log in to the user interface)</span> <br>

                      <input name="Incident Classification" value="Custom Service Request" type="radio" required>Custom Service Request<span style="font-size:18px">(Sample: If a client wants to order new services like a new template)</span> <br>

                      <input name="Incident Classification" value="Modification Request" type="radio" required>Modification Request<span style="font-size:18px">(Sample: Customer wants to change an image in the template or a link)</span> <br><br>

                      Thank you for that information. Our Technical Support Team will contact you within 24 hours.

                      Thank you for calling Serenata Intraware . Have a great day.<br><br>

                      Notes: <br>

                      <textarea rows="3" required cols="95" name="Notes"></textarea> <br><br>

                      @if (in_array($page['campaign_id'], $omnitrixBoards))
                        <div class = "absolute">
                          <br> 
                           @if($page['campaign_id'] != '5841c3b6b8ecd') <p><b>Agent's name: </b><input type ="text" name ="Agent_Name" tabindex="1" value = "" style = "width: 300px;" required></p>@endif
                        </div>
                      @endif

                      <button class="btn " type="submit">Submit</button>
                      <br><br>
          </div>
        </div>
        <div class="col-md-4 sidebar" style="position:fixed;top:40px; right:10px ;max-height: 800px; overflow-y: auto; overflow-x: hidden;">
           <div class="bx o-l">

                <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'') ?>
                
                @if(!empty($bookwormBoards))
            
                  @if(in_array($page['campaign_id'], $bookwormBoards ))

                    @if(!empty($arr_contents['content-r']))

                      <div style = "text-align:center; margin-top: -63px;">

                        <input type = "hidden" name = "Agent Name" value ="<?php echo $user ?>">

                        <input type="submit" value="Submit">

                      </div>

                    @endif

                  @endif

                @endif

           </div>
           <div class="bx o-l">
              <div class="bottom-r">
                 @foreach($pages as $p)

                    @if($link == 'live')

                      @if($p->id != $page->id && in_array($p->id,$btns))
                        <a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                      @endif

                    @else

                      @if($p->id != $page->id && in_array($p->id,$btns))
                        <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                      @endif

                    @endif
                 @endforeach
              </div>
           </div>
        </div>
        @endif
      </div>
    </div>
</form>   

@endsection

<script type="text/javascript">
$( document ).ready(function() {
 
   alert('s'); 

});

</script>

<style>

 input[type=submit] {
    display: inline-block;
    margin: 5px 20px;
    width: 285px;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

</style>
