@extends('templates.scorpio2.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 

      foreach ($contents as $k => $v) {

        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

  $fname= !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $lname= !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $user= !empty($_GET["user"]) ?  $_GET["user"] : ""; 

?>


<?php

$lead_info = json_encode($_GET);
$lead = json_decode($lead_info);

$fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
?>
@section('custom_head')


<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    $(":text").attr("required", false);
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");

    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>

  <script>
    $(document).ready(function () {
    
        $(".toggle").click(function() { 
          $($('.container')[$(this).index(".toggle")]).toggle('fast'); 
        });

        $('#ParentContainer').scroll(function() { 
            $('#FixedDiv').css('top', $(this).scrollTop());
        });

    });

</script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>

    @include('templates.scorpio._dm-job-titles-modal',compact('arr_contents','url'))

    <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
      <input type="hidden" name="pageid" value="{{$page->id}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

      <input type="hidden" name="disposition" value="">

      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div id="wrapper">

    <div>
 
          @if($link == 'preview')
            
                <div class="pull-right">
                    <label>test email:</label>
                    <input type="text" name="test_email">
                    <br>
                </div>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right">
                  <label>test email:</label>
                  <input type="text" name="test_email">
                  <br>
                </div>

            @endif

    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-7">
                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">

                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                            <strong style="font-size: 20px;">GETTING STARTED</strong>
                            <hr>
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif

                              @if($link == 'live')

                        @if($p->id != $page->id && in_array($p->id,$btns))
                            
                            <a href="{{url('live/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}"><input type="checkbox" value="">{{$p->title}}</a> 
                        
                        @endif

                    @else

                        

                    @endif

                    <div class="boxed"></div><br>

                    @foreach($pages as $p)


                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    @if($link == 'live')

                        @if($p->id != $page->id && in_array($p->id,$btns))
                            
                                                @if($p->id != $page->id && in_array($p->id,$btns))
                                <?php
                                $child = json_decode($p->contents); 

                                foreach ($child as $k => $v) {

                                  $child_contents[$k] = $v;
                                }
                                ?>
                                <input type="checkbox" class="toggle" name="{{$p->title,$btns}}" value="1"><strong style="color: blue;"> {{$p->title,$btns}}</strong><br>
                                <div class="container" style="display:none"><br>{!! !empty($child_contents['content-l']) ? ($child_contents['content-l']) : "" !!} <br>

                                  <?php
                                    $content_reso = $arr_contents['content-4'];
                                    $reso_a = preg_replace("/\s+/", " ",$arr_contents['content-4']);
                                    $reso_b = explode(";",$reso_a);
                                  ?>

                                  <p><b>Resolution:</b></p>
                                  <select class="form-control" required style ="width:300;" name = "{{$p->title,$btns}} Resolution">
                                    <option value="N/A">Please select</option>
                                      @foreach ($reso_b as $key2 =>$value2)
                                        @if(!empty($value2))
                                          <?php  $trimmed2 = explode(":", trim($value2)); ?>
                                            <option value="{{$trimmed2[0]}}" >{{$trimmed2[1]}}</option>
                                        @endif
                                      @endforeach
                                  </select>

                                  <br><p><b>Reason:</b></p>
                                  <textarea rows="4" class="form-control" name="{{$p->title,$btns}} (Not Resolved) Reason" ></textarea><br>
                                </div>

                    @endif
                        
                        @endif

                    @else

                    @if($p->id != $page->id && in_array($p->id,$btns))
                                <?php
                                $child = json_decode($p->contents); 

                                foreach ($child as $k => $v) {

                                  $child_contents[$k] = $v;
                                }
                                ?>
                                <input type="checkbox" class="toggle" name="{{$p->title,$btns}}" value="1"><strong style="color: blue;"> {{$p->title,$btns}}</strong><br>
                                <div class="container" style="display:none"><br>{!! !empty($child_contents['content-l']) ? ($child_contents['content-l']) : "" !!} <br>

                                  <?php
                                    $content_reso = $arr_contents['content-4'];
                                    $reso_a = preg_replace("/\s+/", " ",$arr_contents['content-4']);
                                    $reso_b = explode(";",$reso_a);
                                  ?>

                                  <p><b>Resolution:</b></p>
                                  <select class="form-control" required style ="width:300;" name = "{{$p->title,$btns}} Resolution">
                                    <option value="N/A">Please select</option>
                                      @foreach ($reso_b as $key2 =>$value2)
                                        @if(!empty($value2))
                                          <?php  $trimmed2 = explode(":", trim($value2)); ?>
                                            <option value="{{$trimmed2[0]}}" >{{$trimmed2[1]}}</option>
                                        @endif
                                      @endforeach
                                  </select>

                                  <br><p><b>Reason:</b></p>
                                  <textarea rows="4" class="form-control" name="{{$p->title,$btns}} (Not Resolved) Reason" ></textarea><br>
                                </div>

                    @endif  

                        @endif

                @endforeach
                      <input type="checkbox" class="toggle" name="Other Issues" value="1">
                        <strong style="color: blue;">Other Issues</strong>
                        <br>
                          <div class="container" style="display:none"><br><p style="text-align: center;">
                            <span style="font-size:24px;"><strong><textarea rows="4" cols="40" name="Other Issues (Reason)"></textarea></strong></span></p>                                                            
                        </div>

                        <br><div class="boxed"></div><br>
                        <p style="text-align: center;"><button class="btn " type="submit">SUBMIT</button></p></div>


                            @foreach($pages as $p)

                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    

                @endforeach




            {{ Session::get('flash_notification.message') }}


                        
                    </div>
                </div>
            </div>

            <div class="col-md-5">
              <div data-spy="affix" style="border: solid 1px; border-color: gainsboro; padding: 10px;">

                @if(isset($arr_contents['content-r']))

                  <div class ="populate">

                    <?php 

                    $content_vici = explode(",",$arr_contents['content-r']); ?>

                    @foreach ($content_vici as $key => $value)
                      <?php   $vici_value = explode(":",$value); ?> 
                 
                      <p></b> <input type="hidden" name="CompanyName" value="{{$vici_value[0]}}">  </p>
                    @endforeach
                   
                    <?php if($arr_contents['content-r'] != ''):

                      $arr_fields = [];
                      $cont_a = preg_replace("/\s+/", " ",$arr_contents['content-r']);
                      $cont_b = explode(";", $cont_a);
                      
                      foreach($cont_b as $b):
                        if($b):
                          $trimmed = explode(":", trim($b));
                          if(count($trimmed) > 1):
                            $arr_fields[$trimmed[0]] = $trimmed[1];
                          endif;
                        endif;
                      endforeach;
                      foreach(array_keys($_GET) as $v):
                        if(isset($arr_fields[$v])):
                          echo '<label>'.$arr_fields[$v].':</label><input name="'.$arr_fields[$v].'" value="'.$_GET[$v].'"><br>';
                        endif;
                      endforeach;
                     ?>
                     
                    <?php endif; ?>

                  </div>
                @endif

                <div class ="populate">
                  <p style="text-align: center;"><b>Other Information - Additional Info or Links</b></p>
                  <div class="boxed"></div><br>

                  <?= (isset($arr_contents['content-2'])?parse_input($arr_contents['content-2'],$url):'') ?>

                  <br><div class="boxed"></div><br>

                  @if($page['campaign_id'] == '58fe45c2685f8')
                  <br>
                    <div class="col-md-4 ">
                      <a href="http://192.168.200.28/iq_link?user={{$user}}&first_name={{$fname}}&last_name={{$lname}}" target="_blank">IQ Send Link</a>
                    </div>
                    <div class="col-md-5 ">
                      <a href="http://192.168.200.28/iq_sched?user={{$user}}&first_name={{$fname}}&last_name={{$lname}}" target="_blank">IQ Send Sched</a>
                    </div>
                    <br>
                  
                  @endif

                </div>

<!--                 <div class="populate">
                    <a data-toggle='modal' data-target='#rebuttals' class='btn '>Rebuttals</a>
                </div> -->

                @if(!empty($arr_contents['content-3']))
                  <div class="populate">
                    <?php
                      $content_dispo = $arr_contents['content-3'];
                      $dispo_a = preg_replace("/\s+/", " ",$arr_contents['content-3']);
                      $dispo_b = explode(";",$dispo_a);
                    ?>

  <td style="background:#B3AEB5;">
    <div class="form-group text-center">
        <div class="input-group" style="margin:auto;">
            <p style="text-align: center;"><b>Disposition:</b></p>
            <select class="form-control" required style ="width:400px;" name = "disposition">
                      <option value="N/A">Please select</option>
                        @foreach ($dispo_b as $key2 =>$value2)
                          @if(!empty($value2))
                            <?php  $trimmed2 = explode(":", trim($value2)); ?>
                              <option value="{{$trimmed2[0]}}" >{{$trimmed2[1]}}</option>
                          @endif
                        @endforeach
            </select>
          </div>
      </div>
  </td>


                  </div>
                @endif

            </div>
        </div>
    </div>
    </div>
</div>

          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


.intakeForm-darken>header {
    border-color: #63D1F4;
    background: #63D1F4;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #63D1F4 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

.populate {
    margin-top: 6px !important;
    background-color: #e3e3e3;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
.well{

  background-color:#e3e3e3!important;
}
table {
    border-collapse: collapse;
    width: 100%;
}


th, td {
    text-align: left;
    padding: 8px;
    background-color: white;
}

.boxed {
  border: 1px #63D1F4;
  height: 5px;
  background-color: #63D1F4
}

</style>
