
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">
  </head>
  <body>

    @yield('content')

    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('js/gen_validatorv4.js')}}"></script>


    <script type="text/javascript">
            var frmvalidator = new Validator("myform");
            frmvalidator.addValidation("first_name","req","Please enter First Name");
            frmvalidator.addValidation("last_name","req","Please enter Last Name");
            frmvalidator.addValidation("phone","minlen=10","phone must be 10 numbers");
            frmvalidator.addValidation("phone","numeric","phone only numeric allowed");
    </script>
  </body>
</html>