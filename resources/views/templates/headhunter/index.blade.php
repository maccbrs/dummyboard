@extends('templates.paperchase.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 
      foreach ($contents as $k => $v) {
        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  
  $fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
  $CompanyName = !empty($_GET["CompanyName"]) ?  $_GET["CompanyName"] : "";
  $SICCODE = !empty($_GET["SICCODE"]) ?  $_GET["SICCODE"] : "";  
  $SICDESC = !empty($_GET["SICDESC"]) ?  $_GET["SICDESC"] : ""; 
  $GenericCode = !empty($_GET["GenericCode"]) ?  $_GET["GenericCode"] : "";  
  $GenericIndustry = !empty($_GET["GenericIndustry"]) ?  $_GET["GenericIndustry"] : "";  
  $ActualEmpize = !empty($_GET["ActualEmpize"]) ?  $_GET["ActualEmpize"] : "";  
  $ActualAnnualSales = !empty($_GET["ActualAnnualSales"]) ?  $_GET["ActualAnnualSales"] : "";  
  $first_name = !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $middle_initial = !empty($_GET["middle_initial"]) ?  $_GET["middle_initial"] : ""; 
  $last_name = !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $Level = !empty($_GET["Level"]) ?  $_GET["Level"] : ""; 
  $Department = !empty($_GET["Department"]) ?  $_GET["Department"] : "";
  $Position = !empty($_GET["Position"]) ?  $_GET["Position"] : "";

  $phone_number = !empty($_GET["phone_number"]) ?  $_GET["phone_number"] : "";
  $DID = !empty($_GET["DID"]) ?  $_GET["DID"] : "";   
  $email = !empty($_GET["email"]) ?  $_GET["email"] : ""; 
  $ALTEMAILADDRESS = !empty($_GET["ALTEMAILADDRESS"]) ?  $_GET["ALTEMAILADDRESS"] : ""; 
  $Website = !empty($_GET["Website"]) ?  $_GET["Website"] : "";
  $address1 = !empty($_GET["address1"]) ?  $_GET["address1"] : "";
  $Country = !empty($_GET["Country"]) ?  $_GET["Country"] : "";
  $postal_code = !empty($_GET["postal_code"]) ?  $_GET["postal_code"] : "";
  $DateLastUpdated = !empty($_GET["DateLastUpdated"]) ?  $_GET["DateLastUpdated"] : "";
  $Stage = !empty($_GET["Stage"]) ?  $_GET["Stage"] : "";
  $Notes = !empty($_GET["Notes"]) ?  $_GET["Notes"] : "";
 
// paperchase hidden fields   

  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : "";
  $campaign_id = !empty($_GET['campaign']) ? $_GET['campaign'] : "";
  $user = !empty($_GET['user']) ? $_GET['user'] : "";
  $session_id = !empty($_GET['session_id']) ? $_GET['session_id'] : "";
  $city = !empty($_GET['city']) ? $_GET['city'] : "";
  

?>
@section('custom_head')


<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    $(":text").attr("required", false);
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");
/*
    $("#NoContactBtn").on("click", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", true);
        $("#selectMainDispo").attr("required", false);
    });*/


    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
         
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>

    @include('templates.paperchase._dm-job-titles-modal');


    <form id="mainForm" enctype="multipart/form-data" action="{{url('data/save_outbound/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>

       
      
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="pageid" value="{{$page->id}}">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

        <input type="hidden" name="disposition" value="">

      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-7">
                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">
                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="col-md-5 " style = "max-height: 100%;position:fixed;top:10px; right:10px ;">
              <div style="border: solid 1px; border-color: gainsboro; padding: 10px;">

                @if($link == 'preview')
            
                <div class="pull-right">
                    <label>test email:</label>
                    <input type="text" name="test_email">
                    <br>
                </div>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right">
                  <label>test email:</label>
                  <input type="text" name="test_email">
                  <br>
                </div>

            @endif
            
            {{ Session::get('flash_notification.message') }}

                @foreach($pages as $p)

                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    @if($link == 'live')

                        @if($p->id != $page->id && in_array($p->id,$btns))
                            
                            <a href="{{url('live/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn">{{$p->title}}</a> 
                        
                        @endif

                    @else

                        @if($p->id != $page->id && in_array($p->id,$btns))

                            <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn" <?php if ($pos === false){  } else { echo "target='_blank'";} ?> >{{$p->title}}</a> 
                       
                        @endif

                    @endif

                @endforeach

        

                <br>

                <div class ="populate">

                  <div class="row">
                      <div class="col-sm-6"> 

                        <p><b>Company Name :</b>{{$CompanyName}} <input type="hidden" name="CompanyName" value="{{$CompanyName}}">  </p>
                        <p><b>SIC Code:</b> {{$SICCODE}} <input type="hidden" name="SIC Code" value="{{$SICCODE}}"> </p>
                        <p><b>SIC Description :</b> {{$SICDESC}} <input type="hidden" name="SIC Description" value="{{$SICDESC}}"> </p>
                        <p><b>Generic Code :</b>{{$GenericCode}} <input type="hidden" name="Generic Code" value="{{$GenericCode}}"> </p>
                        <p><b>Generic Industry:</b>{{$GenericIndustry}} <input type="hidden" name="Generic Industry" value="{{$GenericIndustry}}"> </p>
                        <p><b>Employee Size:</b>{{$ActualEmpize}} <input type="hidden" name="Employee Size" value="{{$ActualEmpize}}"> </p>
                        <p><b>Annual Revenue:</b> {{$ActualAnnualSales}} <input type="hidden" name="Annual Revenue" value="{{$ActualAnnualSales}}"> </p>
                        <p><b>First Name:</b> {{$first_name}} <input type="hidden" name="First Name" value="{{$first_name}}"> </p>
                        <p><b>Last Name:</b>{{$last_name}}<input type="hidden" name="Last Name" value="{{$last_name}}"></p>
                        <p><b>Position :</b>{{$Position}} <input type="hidden" name="Position" value="{{$Position}}">  </p>
                        <p><b>Level :</b> {{$Level}} <input type="hidden" name="Level" value="{{$Level}}"> </p>
                        <p><b>Department :</b> {{$Department}} <input type="hidden" name="Department" value="{{$Department}}"> </p>
                 
                      </div>
                      <div class="col-sm-6">

                        <p><b>Phone Number :</b>{{$phone_number}} <input type="hidden" name="Phone Number" value="{{$phone_number}}"> </p>
                        <p><b>DID :</b>{{$DID}} <input type="hidden" name="DID" value="{{$DID}}"> </p>
                        <p><b>Email Address :</b>{{$email}} <input type="hidden" name="Email Address" value="{{$email}}"> </p>
                        <p><b>Alt Email Address :</b> {{$ALTEMAILADDRESS}} <input type="hidden" name="Alt Email Address" value="{{$ALTEMAILADDRESS}}"> </p>
                        <p><b>Website:</b> {{$Website}} <input type="hidden" name="Website" value="{{$Website}}"> </p>
                        <p><b>Address:</b>{{$address1}}<input type="hidden" name="Address" value="{{$address1}}"></p>
                        <p><b>Country :</b> {{$Country}} <input type="hidden" name="Country" value="{{$Country}}"> </p>
                        <p><b>City/State:</b> {{$city}} <input type="hidden" name="City/State" value="{{$city}}"> </p>
                        <p><b>ZipCode:</b>{{$postal_code}}<input type="hidden" name="ZipCode" value="{{$postal_code}}"></p>
                        <p><b>Date Last Updated :</b> {{$DateLastUpdated }} <input type="hidden" name="Date Last Updated" value="{{$DateLastUpdated }}"> </p>
                        <p><b>Stage:</b> {{$Stage}} <input type="hidden" name="Stage" value="{{$Stage}}"> </p>
                        <p><b>Notes:</b>{{$Notes}}<input type="hidden" name="Notes" value="{{$Notes}}"></p>

                      </div>
                  </div>
                </div>
                    <div class="populate">
                        <a data-toggle='modal' data-target='#jobTitlesModal' class='btn '>DM Job Titles</a>
                    </div>
                <div class="populate">
                    <p><b>Disposition :</b>

                        <select class="form-control pull-right" required style ="width:300;" name = "disposition">
                          <option value="" >Please select</option>
                          <option value="LQ">LQ - Lead Qualified</option>
                          <option value="CB">CB - Call Back</option>
                          <option value="NI">NI - Not Interested</option>
                          <option value="HU">HU - Hang Up</option>
                          <option value="NA">NA - No Answer</option>
                          <option value="AM">AM - Answering Machine</option>
                          <option value="HUP">HUP - Hang Up During Pitch</option>
                          <!-- <option value="DMNA">DMNA - Decision Maker Not Available</option> -->
                          <option value="WN">WN - Wrong Number</option>
                          <option value="BUSY">BUSY - Busy tone</option>
                          <option value="DNC">DNC - Do not call</option>
                           <option value="DN">DN - Disconected No</option>
                          <option value="FM">FM - Fax  Machine</option>
                          <option value="LB">LB - Language Barrier</option>
                          <option value="BC">BC - Bad Connection</option>
                          <option value="EMAIL">EMAIL - Email</option>
                          <option value="NQ">NQ - Not Qualified</option>
                          <option value="FU">FU - For Update</option>
                          <option value="PL">PL - Profiled Lead</option>
                        </select>
                    </p>
                </div>

                <input type="hidden" name="lead_id" value="{{$lead_id}}">
                <input type="hidden" name="campaign_id" value="{{$campaign_id}}">
                <input type="hidden" name="user" value="{{$user}}">
                <input type="hidden" name="session_id" value="{{$session_id}}">

            </div>
        </div>
    </div>
    </div>
</div>

          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


.intakeForm-darken>header {
    border-color: #e67e22;
    background: #e67e22;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #e67e22 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

.populate {
    margin-top: 6px !important;
    background-color: #e3e3e3;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
.well{

  background-color:#e3e3e3!important;
}


</style>
