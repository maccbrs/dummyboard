<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
   
    <link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
  </head>
  
    <!-- <script src="path/to/timezones.full.js"></script> -->
    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script type='text/javascript' src="{{URL::asset('js/timezones.full.js')}}"></script> 
    <script type="text/javascript">
        
        function InputToggle() {
         $( "div" ).each(function( i ) {
                        if ( $(this).is(":visible"))  {
                           $(this).find('input').prop( "disabled", false );
                        } else {
                          $(this).find('input').prop( "disabled", true );
                        }
                      });
            }

      $( document ).ready(function() {
         
         
          $( "form" ).submit(function( event ) {
            
               var chckboxarray_val = [];
               var chckboxarray_name = [];
                $('input:checked').each(function() {
                    chckboxarray_val.push($(this).val());
                    chckboxarray_name.push($(this).prop("name"));
                });
              
               for(var x=0;x< chckboxarray_name.length ; x ++)
                   {
                       if(x == 0)
                           {
                               var temp_name = chckboxarray_name[0];
                               var temp_val = chckboxarray_val[0];
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                           }
                       
                       else if(chckboxarray_name[x - 1] == chckboxarray_name[x] )
                           {
                               temp_val = temp_val + ',' +  chckboxarray_val[x];
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                              // alert(temp_val);
                           }
                       else
                           {
                               $( "input[name='" + temp_name + "']" ).val(temp_val);
                                temp_name = chckboxarray_name[x];
                                temp_val = chckboxarray_val[x];
                               
                               //alert(chckboxarray_val.join(","));
                           }
                       
                       
                       
                       
                   }
                   
                   
                   
              //$( "input[name='" +chckboxarray_name[0]+ "']" ).val(chckboxarray_val.join(","))
              
              
              
               //event.preventDefault(); 
              InputToggle();
              /*
               $( "div" ).each(function( i ) {
                        if ( $(this).is(":visible"))  {
                           $(this).find('input').prop( "disabled", false );
                        } else {
                          $(this).find('input').prop( "disabled", true );
                        }
                      });
               */
              
            });
          
          
          
        
            
          // irecruit
          $('.hideme').hide();
          $('.ifyes').hide();
          $('.ifno').hide();
          $('.ifothers').hide();
          $('.ifothers2').hide();
          $('.someonetotalk').hide();
          $('.callbackreason').hide();
          $('.companyrole').hide();
          $('.notinterested').hide();
          $('.rolenodiv').hide();
          $('.sendingjd').hide();
          $('.askquestion').hide();
          $('.askquestionsdiv').hide();
          $('.phonenumberdiv').hide();
          $('.emailaddressdiv').hide();
          $('.companydiv').hide();

        campaign = $('#campaign_id').val();


        if(campaign == '583ca9adf1b8b'){

          $('.input-date').datetimepicker({
            format:' F d Y H:i',
          });
      /* Add By Cleo */
          //$('select').timezones(); // Commented By Cleo     
            
            
           
            
            
            
            
            
            
      if($('#Now_or_Call_Back').val() =='yes')
      {
      $('.companyrole').show();
      $( ".companyrole_sub" ).prop( "disabled", false );
      $( ".hideme_sub" ).prop( "disabled", true );
      $( ".notinterested_sub" ).prop( "disabled", true );
      $('.sendingjd').show();
    }

      
      
      $('#Now_or_Call_Back').on('change', function() {
        if( this.value  == 'yes')
        {
            $('.notinterested').hide();
            $('.companyrole').show();
            $('.hideme').hide();
            $( ".hideme_sub" ).prop( "disabled", true );
            $( ".notinterested_sub" ).prop( "disabled", true );
            $( ".companyrole_sub" ).prop( "disabled", false );

        }
        else if( this.value  == 'callback')
        {
            $('.notinterested').hide();
            $('.companyrole').hide();
            $('.hideme').show();
            $( ".hideme_sub" ).prop( "disabled", false );
            $( ".notinterested_sub" ).prop( "disabled", true );
            $( ".companyrole_sub" ).prop( "disabled", true );
        }
        else {
            $('.notinterested').show();
            $('.companyrole').hide();
            $('.hideme').hide();
            $( ".hideme_sub" ).prop( "disabled", true );
            $( ".notinterested_sub" ).prop( "disabled", false );
            $( ".companyrole_sub" ).prop( "disabled", true );
        }
      
      })
      
          $('#role').on('change', function() {
          
            if( this.value  == 'yes')
            {
                $('.sendingjd').show();
                $('.rolenodiv').hide();
              

            }
            else if( this.value  == 'no')
            {
                $('.rolenodiv').show();
                $('.sendingjd').hide();
            }
        
         

          }); 
      

      
      /* END HERE*/
      

          $(":radio[value=CallBack]").on('click',function(){
            
            $('.notinterested').hide();
            $('.companyrole').hide();
            $('.hideme').show();

          }); 

          $(".companyroleradio").on('click',function(){
        
            $('.notinterested').hide();
            $('.companyrole').show();
            $('.hideme').hide();


          }); 

          $(".notinterestedradio").on('click',function(){
        
            $('.notinterested').show();
            $('.companyrole').hide();
            $('.hideme').hide();

          }); 

          $(":radio[value=Yes]").on('click',function(){
            
            $( ".hideme" ).hide();
      
          }); 

          $(":radio[value=No]").on('click',function(){
            
            $( ".hideme" ).hide();
          
          }); 

          $(".NoPermit").on('click',function(){
             
            $('.ifyes').hide();
             
          }); 

          $(".YesPermit").on('click',function(){
            
            $('.ifyes').show();

          }); 

          $(".nointerview").on('click',function(){

           $('.ifno').show();
             
          }); 

          $(".yesinterview").on('click',function(){
        
            $('.ifno').hide();

          }); 

      
          $(".CompanyOrPosition").change(function() {
              if(this.checked) {
                $('.ifothers').show();
        $('.ifothers').find('input').prop('disabled',false);
              }else{
                $('.ifothers').hide();
        $('.ifothers').find('input').prop('disabled',true);

              }
          });

          $(".otherscheck").change(function() {
              if(this.checked) {
                $('.ifothers2').show();
        $('.ifothers2').find('input').prop('disabled',false);
              }else{
                $('.ifothers2').hide();
        $('.ifothers2').find('input').prop('disabled',true);
              }
          });

          $(".callbackyes").on('click',function(){
        
            $('.someonetotalk').show();

          }); 

          $(".callbackno").on('click',function(){
        
            $('.someonetotalk').hide();

          }); 

          $(".RoleNo").on('click',function(){
        
            $('.rolenodiv').show();
            $('.sendingjd').hide();

          }); 

          $(".RoleYes").on('click',function(){
        
            $('.sendingjd').show();
            $('.rolenodiv').hide();

          }); 

          $(".askquestionno").on('click',function(){
            $('.sending_jd').hide();
            $('.askquestion').show();

          }); 

          $(".askquestionyes").on('click',function(){
            $('.sending_jd').show();
            $('.askquestion').hide();

          });

          $(".askquestions").on('click',function(){
        
            $('.askquestionsdiv').show();

          });

          $(".phonenumberno").on('click',function(){
        
            $('.phonenumberdiv').show();

          });

          $(".phonenumberyes").on('click',function(){
        
            $('.phonenumberdiv').hide();

          });

          $(".emailaddressno").on('click',function(){
        
            $('.emailaddressdiv').show();

          });

          $(".emailaddressyes").on('click',function(){
        
            $('.emailaddressdiv').hide();

          });

          $(".companyyes").on('click',function(){
        
            $('.companydiv').hide();

          });

          $(".companyno").on('click',function(){
        
            $('.companydiv').show();

          });

          $(".otherscallback").change(function() {

              if(this.checked) {

                $('.callbackreason').show();

              }else{

                $('.callbackreason').hide();

              }

          }); 

          $(".inputdate").change(function(){
              
          });

          $(".select-timezone").change(function(){
            var datelist = $(".inputdate").val();

            var timezone = $(".select-timezone").val();
              if(datelist){
                var m = moment.utc(datelist, "MMMM D YYYY HH:mm");
                var convertedTime = m.tz("America/Chicago").format('LLLL');
                var datelist = $(".ConvertedDate").val(convertedTime);
              }
          });

        }

    });

  </script>

     @yield('custom_head')

  <body>

    @yield('content')


  </body>
</html>