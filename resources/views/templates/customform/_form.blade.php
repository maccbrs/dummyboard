<?php //pre($page);
  if(!empty($page->contents)){
    $contents = json_decode($page->contents);
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
  }
?>

    <div id="page-wrapper"> 
    	<div class="m-t"></div>
        <div class="row">
            <form enctype="multipart/form-data" action="{{url('dashboard/page/edit/'.$page->id)}}" method="post">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="id" value="{{$page->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">  


        	<div class="col-md-12">
				<div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        Custom Template

                       <button type="submit" class="btn btn-default pull-right">update</button> <a href="{{url('preview/'.$page->campaign_id.'/'.$page->id)}}" target="_tab" class=" preview btn btn-default pull-right">Preview</a>
                       <a href="{{url('dashboard/boards/edit/'.$page->campaign_id)}}" class="preview btn btn-default pull-right">Board Main</a>
                    </div>
                    <div class="panel-body">
                       <div class="col-md-10 col-md-offset-1">
                              @include('flash::message')


                              <div class="panel panel-primary">
                                 <div class="panel-heading">
                                    Form
                                 </div>
                                 <div class="panel-body">
                                    <p>Email Subject: <input type="test" name="email-subject" value="{{(isset($arr_contents['email-subject'])?$arr_contents['email-subject']:'')}}"></p>
                                    <p>From: <input type="test" name="email-from" value="{{(isset($arr_contents['email-from'])?$arr_contents['email-from']:'')}}"></p>
                                    <p>To: <input type="test" name="email-to" value="{{(isset($arr_contents['email-to'])?$arr_contents['email-to']:'')}}"></p>
                                    <p>CC: <input type="test" name="email-cc" value="{{(isset($arr_contents['email-cc'])?$arr_contents['email-cc']:'')}}"></p>
                                    <textarea id="editor" class="form-control" rows="20" height="100px" name="form-main"><?= (isset($arr_contents['form-main'])?$arr_contents['form-main']:'') ?></textarea>
                                 </div>
                              </div>



                       </div>
                    </div>
				</div>
        	</div>
            </form>
        </div>
    </div>

    <style type="text/css">
      p > input{
        display: inline-block;
        width: 800px;
        background-color: #fff;
        color: #000;
        padding: 2px;
        border: 1px solid #333;        
      }
    </style>