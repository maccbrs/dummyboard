

<?php

if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}
?>

<form enctype="multipart/form-data" id='custom-form-submit{{$page->id}}' action="{{url('data/save/'.$page->campaign_id)}}" method="post" >
  <input type="hidden" name="pageid" value="{{$page->id}}">
  <input type="hidden" name="_method" value="POST">
  <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
  <input type="hidden" name="email_subject" value="{{(isset($arr_contents['email-subject'])?$arr_contents['email-subject']:'')}}">
   <input type="hidden" name="email_from" value="{{(isset($arr_contents['email-from'])?$arr_contents['email-from']:'')}}">
    <input type="hidden" name="email_to" value="{{(isset($arr_contents['email-to'])?$arr_contents['email-to']:'')}}">
     <input type="hidden" name="email_cc" value="{{(isset($arr_contents['email-cc'])?$arr_contents['email-cc']:'')}}">

  <input type="hidden" name="_token" value="{{ csrf_token() }}">  
  <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
  <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">
  <div id="form{{$page->id}}">
    {!!(isset($arr_contents['form-main'])?parse_input($arr_contents['form-main'],$url):'')!!} 
  </div>
  

</form>

<script type="text/javascript">
  
  $('#custom-form-submit{{$page->id}}').on('submit', function(){
      var base = "{{url('/')}}";
      var form = $(this);
      var formdata = false;
      if (window.FormData){
          formdata = new FormData(form[0]);
      }

      var formAction = form.attr('action');
      $.ajax({
          url         : base + "/custom-form",
          data        : formdata ? formdata : form.serialize(),
          cache       : false,
          contentType : false,
          processData : false,
          type        : 'POST',
          success     : function(data, textStatus, jqXHR){
              $('#form{{$page->id}}').html(data);
          }
      });
      return false;
  });

</script>