@extends('templates.first.master')
@section('title', 'Page Title')
<style type="text/css">
  div.absolute {
    position: absolute;
    top: 0px;
} 
</style>

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 
      foreach ($contents as $k => $v) {
        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : null ;
  $phone_number = !empty($_GET['phone_number']) ? $_GET['phone_number'] : " ";
  $user = !empty($_GET['user']) ? $_GET['user'] : "n/a";
  $fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : " ";
?>
@section('custom_head')

<style>
 .bx{background-color: <?= (isset($arr_contents['content-2'])?parse_input($arr_contents['content-2'],$url):'') ?> !important;}
</style>
<script>


$(document).ready(function(){
    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    });


});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
          <div class="noleadid">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Live Dummy Board Cannot be Used without a Call

          </div>
         
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>


    <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
      
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="pageid" value="{{$page->id}}">

      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div class="container">
        <div class="top">

          @if($link == 'preview')

          <div class="pull-right">

            <label>test email:</label>
            <input type="text" name="test_email">

          </div>

          @elseif($link == 'train' && Auth::check())

          <div class="pull-right">

            <label>test email:</label>
            <input type="text" name="test_email">

          </div>

          @endif
            
            {{ Session::get('flash_notification.message') }}

          </div>
        
          <div class="col-md-8">
            <div class="bx o-l main-bx">
              @if (Session::has('flash_notification.message'))
                @include('templates.'.$page->template.'._success')

              @else
				  
			  @if($page['campaign_id'] == 'Strainee')
              <br>
              <br>
              <br>
			  <?php 
			  session_start();
			  if (isset($_GET['tfn']))
				  
			  {$_SESSION["tfn"] = $_GET['tfn']; }
		     
			  //$_SESSION["tfn"] = if (isset($_GET['tfn'])) $_GET['tfn'] : "";?>
                <p><b>TFN: </b><input type ="text" name ="tfn" value = "<?php if (isset($_SESSION["tfn"]))
																		echo $_SESSION["tfn"]; else echo ''; ?>" style = "width: 300px;" required></p>
																	
				
              @endif

              @if($page['campaign_id'] == '56fd65a2750dd')
              <br>
              <br>
              <br>
                <p><b>Email Subject: </b><input type ="text" name ="email_subject" value = "" style = "width: 300px;" required></p>
              @endif

              @if($page['campaign_id'] == '5aa979e208f2b')
              <div class="row">
                <div class="col-md-6">
                  <button type="button" id="starttime_button">Start</button>
                  <label style="font-size: 14px">Start Time: </label>
                  <label style="font-size: 14px" id="starttime_label"></label>
                  <input type="hidden" name="start_time" id="start_time" value="">
             <!--  <input type="time" name="start_time" id="starttime">
                 </div>
                 <div class="col-md-4">
               <p style="font-size: 14px">End Time: </p>
              <input type="time" name="end_time"> -->
              </div>
               <div class="col-md-6">
                  <button type="button" id="endtime_button">End</button>
                  <label style="font-size: 14px">End Time: </label>
                  <label style="font-size: 14px" id="endtime_label"></label>
                  <input type="hidden" name="end_time" id="end_time" value="">
              </div>
              </div>

              @endif   

              @if($page['campaign_id'] == '5b58dc9911d9e')
              <div class="row">
                <div class="col-md-6" style="padding: 20px;">
                  <!-- <button type="button" style="margin-top: 20px;" id="starttime_button">Start</button> -->
                  <label style="font-size: 14px">Start Time: {{ date('Y-m-d H:i:s') }}</label>
                  <label style="font-size: 14px" id="starttime_label"></label>
                  <input type="hidden" name="start_time" id="start_time" value="{{ date('Y-m-d H:i:s') }}">
              </div>
              
              </div>

              @endif  

              @if($page['campaign_id'] == '5ab2b72e64bf1')

             {{-- <div class="row">

                <div class="col-md-6" style="padding: 20px;">
                  <!-- <button type="button" style="margin-top: 20px;" id="starttime_button">Start</button> -->
                  <label style="font-size: 14px">Duration: </label>
                  <label style="font-size: 14px" id="timer_label"></label>
                  <input type="hidden" name="Duration" id="duration" value="">
              </div>
              
              </div> --}}

              @endif  

              @if($page['campaign_id'] == '5748837f750c1' and $page['id'] == '1477')
              <input type ="text" name ="email_to" value = "it-escalation@serenata.com" style = "width: 300px;" hidden="">
              @endif

              @if($page['id'] != '872' && $page['campaign_id'] == '57f80f9d69859')
              <br>
              <br>
              <br>
              <p><b>Caller ID: </b><input type ="text" name ="Caller_ID" tabindex="2" value = "" style = "width: 300px;" required></p>
              @endif
              
              <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

              @if($page['campaign_id'] == '57f544bb414ed')
              <input type = "hidden" name = "Agent_Name" value ="<?php echo $fullname?>">
              @endif 


				
			  @if($page['campaign_id'] == '5a652495db059')
              <br>
              <br>
              <br>
			  <?php 
			  session_start();
			  if (isset($_GET['uniqueid']))
				  
			  {$_SESSION["uniqueid"] = $_GET['uniqueid']; }
		     
			  //$_SESSION["tfn"] = if (isset($_GET['tfn'])) $_GET['tfn'] : "";?>
                <p style="background-color:#f2f2f2;position:fixed;top:0"><b>REFERENCE CALL: </b><input type ="text" name ="uniqueid" value = "<?php if (isset($_SESSION["uniqueid"]))
																		echo $_SESSION["uniqueid"]; else echo ''; ?>" style = "width: 300px;" ></p>
																	
				
              @endif	

              @if (in_array($page['campaign_id'], $omnitrixBoards))
              <div class = "absolute">
                <br> 
                @if($page['id'] != '872' && $page['campaign_id'] != '5841c3b6b8ecd') <p><b>Agent's name: </b><input type ="text" name ="Agent_Name" tabindex="1" value = "" style = "width: 300px;" required></p>
                @endif           

              </div>
              @endif

            </div>



          </div>
      </form>   

      <div class="col-md-4 sidebar ">
      
        <div class="bx o-l">
          <h3 class = "center" style="background-color:rgb(19, 10, 75);color:#ffffff;font-size:36px;margin:0px;font-weight:bold;border-radius: 10px;">{{$board['lob']}}</h3>
              <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                <div style="display:none">
                  <p>Test-Email :&nbsp;<input name="test_email" type="text" /></p>
                </div>

                  <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'') ?>
              
                  @if(!empty($bookwormBoards))
              
                    @if(in_array($page['campaign_id'], $bookwormBoards ))

                      @if(!empty($arr_contents['content-r']))

                        <div style = "text-align:center; ">
              
                          <input type = "hidden" name = "Agent Name" value ="<?php echo $user ?>">

                          <input type="submit" value="Submit"> 

                        </div>

                      @endif

                    @endif

                  @endif
                </form> 
             </div>

             <div class="bx o-l">
                  <div class="bottom-r">

                           @foreach($pages as $p)
               <?php $str = strtolower($p->title);
               $findme   = 'tracking ids';
               $pos = strpos($str, $findme);
               ?>

                              @if($link == 'live')

                                @if($p->id != $page->id && in_array($p->id,$btns))
                                  <a href="{{url('live/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn">{{$p->title}}</a> 
                                @endif

                              @else

                                @if($p->id != $page->id && in_array($p->id,$btns))
                                  <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn" <?php if ($pos === false) 
                  {  } else { echo "target='_blank'";
                  } 
                  ?> >{{$p->title}}</a> 
                                @endif

                              @endif

                           @endforeach
               
                @if(!empty($bookwormBoards))
              
                  @if(in_array($page['campaign_id'], $bookwormBoards ))

                   @if($link == 'live')

                      @if(in_array($page['campaign_id'], $bookwormBoardsUS ))

                        <a href="{{url('live/'. '56ff303456bab')}}" class="btn">Home</a> 
                    

                      @endif

                      @if(in_array($page['campaign_id'], $bookwormBoardsAUS )) 

                        <a href="{{url('live/'. '571e4c72a2370')}}" class="btn">Home</a> 

                      @endif

                       @if(in_array($page['campaign_id'], $bookwormBoardsTOTBP ))

                          <a href="{{url('live/'. '571a577b1cd2f')}}" class="btn">Home</a> 
                        
                      @endif 

                    @else

                      @if(in_array($page['campaign_id'], $bookwormBoardsUS ))

                          <a href="{{url('preview/56ff303456bab')}}" class="btn" <?php if ($pos === false) 
                      {  } else { echo "target='_blank'";
                      } 
                      ?> >Home</a>  
                      
                      @endif

                      @if(in_array($page['campaign_id'], $bookwormBoardsAUS ))

                          <a href="{{url('preview/571e4c72a2370')}}" class="btn" <?php if ($pos === false) 
                      {  } else { echo "target='_blank'";
                      } 
                      ?> >Home</a>  

                      @endif

                       @if(in_array($page['campaign_id'], $bookwormBoardsTOTBP ))

                         <a href="{{url('preview/571a577b1cd2f')}}" class="btn" <?php if ($pos === false) 
                        {  } else { echo "target='_blank'";
                        } 
                        ?> >Home</a>  
                    
                      @endif

                    @endif

                  @endif

                @endif
                
              
                  </div>
             </div>
          </div>
          
          @endif
        </div>
      </div>
  @endif

@endsection
<!--
<script type="text/javascript">
$( document ).ready(function() {
   $(":radio[value=yes]").on('click',function(){
     $('select').attr('disabled', 'disabled');
   });
   $(":radio[value=no]").on('click',function(){
     $('select').prop( "disabled", false );
   });       

   alert('s'); 

});
</script>
-->
<style>

 input[type=submit] {
    display: inline-block;
    margin: 0px;
    width: 80%;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

  .center, .divfinnyes, .divfinnno{

    text-align: center;

  }

  .noleadid{
    background-color: #008080;
    margin: 30px;
    color: white;
    padding: 10px;
    text-align:center;
  }


</style>
