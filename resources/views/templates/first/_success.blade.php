
	<div class="row">
		<div class="col-md-12">
			<div style="height:175px; padding-top:40px;">
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			    <div class="btn-cont" style="text-align:center;">
			    	<a href="javascript:close_window();"  class="btn" style="width: 65px;display: inline;">Close</a>

			    	<br><br>
			    </div>
			</div>
		</div>
	</div>

	<script>

		function close_window() {
		 
		    open(location, '_self').close();
		 
		}
	</script>