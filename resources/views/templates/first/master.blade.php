
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/first/custom.css')}}" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
  </head>


    <script src="path/to/timezones.full.js"></script>
    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script type='text/javascript' src="{{URL::asset('js/timezones.full.js')}}"></script> 
    <script type="text/javascript">

              function InputToggle() {
                     $( "div" ).each(function( i ) {
                                    if ( $(this).is(":visible"))  {
                                       $(this).find('input').prop( "disabled", false );
                                    } else {
                                      $(this).find('input').prop( "disabled", true );
                                    }
                                  });
                        }

        totalSecs = 0;
        function incTimer() {
            var currentMinutes = Math.floor(totalSecs / 60);
            var currentSeconds = totalSecs % 60;
            //var sec = totalSecs % 60;
            if(currentSeconds <= 9) currentSeconds = "0" + currentSeconds;
            if(currentMinutes <= 9) currentMinutes = "0" + currentMinutes;
                        $("#duration").val(totalSecs);
            totalSecs++;
            $("#timer_label").text(currentMinutes + ":" + currentSeconds);
            setTimeout('incTimer()', 1000);
          }
          
        
       
        $( document ).ready(function() {
              $( "form" ).submit(function( event ) {
                InputToggle();
            });
            
					$('input[type=radio]').each(function(){
					 if(($(this).prop('checked')) &&  ($(this).data("toggledisplay") === true)) {
						$("." + $(this).data("child")).show();
						
					  }
					  else
						{
							$("." + $(this).data("child")).hide(); 
							
						
						}
						InputToggle(); 
					});
					
					$('input[type=checkbox]').each(function(){
					 if(($(this).prop('checked')) &&  ($(this).data("toggledisplay") === true)) {
						$("." + $(this).data("child")).show();
						
					  }
					  else
						{
							$("." + $(this).data("child")).hide(); 
							
						
						}
						InputToggle(); 
					});
					
					
					$(function(){
		  $('input[type="checkbox"]').click(function(){
			if ($(this).is(':checked'))
			{
					
					
					
					 if(($(this).prop('checked')) &&  ($(this).data("toggledisplay") === true)) {
						$("." + $(this).data("child")).show(); 
						
					  }
					  else
						{
							$("." + $(this).data("child")).hide(); 

						
						}
					InputToggle();	
					
					
				
				
				
			 // alert($(this).val());
			}
			else
			{if ($(this).data("toggledisplay") === true) 
				{
					$("." + $(this).data("child")).hide(); 
				}
			}
		  });
		});  					
					$(function(){
		  $('input[type="radio"]').click(function(){
			if ($(this).is(':checked'))
			{
					
					
					$('input[type=radio]').each(function(){
					 if(($(this).prop('checked')) &&  ($(this).data("toggledisplay") === true)) {
						$("." + $(this).data("child")).show(); 
						
					  }
					  else
						{
							$("." + $(this).data("child")).hide(); 

						
						}
					InputToggle();	
					});
					
				
				
				
			 // alert($(this).val());
			}
		  });
		});  		

            $('#yes-hide').hide();
            $('#no-hide').hide();
            $('#yes-submit').hide();

            // irecruit
            $('.hideme').hide();
            $('.ifyes').hide();
            $('.ifno').hide();
            $('.ifothers').hide();
            $('.ifothers2').hide();
            $('.someonetotalk').hide();
            $('.callbackreason').hide();
            $('.callbackreason').hide();
            // 

            // finn

            $('.divfinnyes').hide();
            $('.divfinnno').hide();

            // 

          $(":radio[value=yes]").on('click',function(){
            $('select').html('');
            $('select').append('<option value="" disabled selected>Please Select</option>');
            $('select').append('<option value="Customer Service">Customer Service</option>');
            $('select').append('<option value="YES - Customer HangUp">YES - Customer HangUp</option>');
            $('#yes-hide').show();
            $('#yes-submit').show();
            $('#no-hide').hide();
          });


          $(":radio[value=no]").on('click',function(){
            $('select').html('');
            $('select').prop( "disabled", false );
            $('select').append('<option value="" disabled selected>Please Select</option>');
            $('select').append('<option value="Auto HangUp">Auto HangUp</option>');
            $('select').append('<option value="Wrong Number">Wrong Number</option>');
            $('select').append('<option value="No Answer">No Answer</option>');
            $('select').append('<option value="GI - Advertisement">GI - Advertisement</option>');
            $('select').append('<option value="GI - Employment">GI - Employment</option>');
            $('select').append('<option value="GI - General Inquiry">GI - General Inquiry</option>');
            $('select').append('<option value="NO - Customer HangUp">NO - Customer HangUp</option>');
          
            $('#no-hide').show();
            $('#yes-hide').hide();
            $('#yes-submit').hide();

          }); 


          campaign = $('#5star').val();
          
          if(campaign == "5748837f750c1"){

            $('#checkbox-hide').hide();
            $('#input-hide').hide();

            $("input[name=Product]").on('change', function() {

                 if ($("input[name=Product]:first").is(':checked')) {
                   $('#checkbox-hide').show();
                 }

                else if ($("input[value=Others]").is(':checked')) {
                   $('#input-hide').show();
                }

                 else{

                  $('#input-hide').hide();
                  $('#checkbox-hide').hide();

                 }
            })

          }

          campaign = $('#campaign_id').val();

          $("input[name=shipping_address]").hide();



          if(campaign == "5776d34f949fb"){

            $(":radio[value=YES]").on('click',function(){
              
              $("input[name=shipping_address]").prop('disabled', true);
              $("input[name=shipping_address]").hide();
              

            });

            $(":radio[value=NO]").on('click',function(){
                
              $("input[name=shipping_address]").prop('disabled', false);
              $("input[name=shipping_address]").show();
                
            });
          }

          if(campaign == '583ca9adf1b8b'){

            $('.input-date').datetimepicker({
              format:' F d Y H:i',
            });

            $('select').timezones();

            $(":radio[value=CallBack]").on('click',function(){
              
              $('.hideme').show();

            }); 

            $(":radio[value=Yes]").on('click',function(){
              
              $( ".hideme" ).hide();
        
            }); 

            $(":radio[value=No]").on('click',function(){
              
              $( ".hideme" ).hide();
            
            }); 

            $(".NoPermit").on('click',function(){
               
              $('.ifyes').hide();
               
            }); 

            $(".YesPermit").on('click',function(){
              
              $('.ifyes').show();

            }); 

            $(".nointerview").on('click',function(){

             $('.ifno').show();
               
            }); 

            $(".yesinterview").on('click',function(){
          
              $('.ifno').hide();

            }); 

            $(".CompanyOrPosition").change(function() {
                if(this.checked) {
                  $('.ifothers').show();
                }else{
                  $('.ifothers').hide();

                }
            });

            $(".otherscheck").change(function() {
                if(this.checked) {
                  $('.ifothers2').show();
                }else{
                  $('.ifothers2').hide();
                }
            });

            $(".callbackyes").on('click',function(){
          
              $('.someonetotalk').show();

            }); 

            $(".callbackno").on('click',function(){
          
              $('.someonetotalk').hide();

            }); 

            $(".otherscallback").change(function() {
                 if(this.checked) {
                  $('.callbackreason').show();
                 }else{
                  $('.callbackreason').hide();
                 }

            });

            $(".inputdate").change(function(){
                
            });

            $(".select-timezone").change(function(){
              var datelist = $(".inputdate").val();

              var timezone = $(".select-timezone").val();
                if(datelist){
                  var m = moment.utc(datelist, "MMMM D YYYY HH:mm");
                  var convertedTime = m.tz("America/Chicago").format('LLLL');
                  var datelist = $(".ConvertedDate").val(convertedTime);
                }
            });

          }

                      // finn 5851948ebafb1

        if(campaign == '5851948ebafb1'){

            $(".finnyes").on('click',function(){
          
              $('.divfinnyes').show();
              $('.divfinnno').hide();
              
            }); 

            $(".finnno").on('click',function(){
          
              $('.divfinnyes').hide();
              $('.divfinnno').show();
            
            }); 

        }

        if(campaign == '5879861681241'){
            
            $('.input-date').datetimepicker({

              format:'Y-m-d h:i',

            });

        }

        if(campaign == '5aa979e208f2b'){
          function addZero(x){
            if(x < 10){
              x = "0" + x;
            }
            return x;
          }
          $('#starttime_button').click(function (){
              var now = new Date();
              var today = now.getFullYear()+"-"+addZero((now.getMonth()+1))+"-"+addZero(now.getDate());
              var time = addZero(now.getHours()) + ":" + addZero(now.getMinutes()) + ":" + addZero(now.getSeconds());
              var datetime = today+" "+time;
              $('#starttime_label').text(datetime);
              $('#start_time').val(datetime);


          });

           $('#endtime_button').click(function (){
              var now = new Date();
              var today = now.getFullYear()+"-"+addZero((now.getMonth()+1)) +"-"+addZero(now.getDate());
              var time = addZero(now.getHours()) + ":" + addZero(now.getMinutes()) + ":" + addZero(now.getSeconds());
              var datetime = today+" "+time;
              $('#endtime_label').text(datetime);
              $('#end_time').val(datetime);
              
              
          });

        }

         if(campaign == '5ab2b72e64bf1'){

            incTimer();
 

        //   function addZero(x){
        //     if(x < 10){
        //       x = "0" + x;
        //     }
        //     return x;
        //   }
        // //  $('#starttime_button').click(function (){
        //       var now = new Date();
        //       var today = now.getFullYear()+"-"+addZero((now.getMonth()+1))+"-"+addZero(now.getDate());
        //       var time = addZero(now.getHours()) + ":" + addZero(now.getMinutes()) + ":" + addZero(now.getSeconds());
        //       var datetime = today+" "+time;
        //       $('#starttime_label').text(datetime);
        //       $('#start_time').val(datetime);


         // });

         

        }

      });

      </script>

	   @yield('custom_head')

  <body>

    @yield('content')

      <style>

        .hideThis{
          display:none;

        }

      </style>

  </body>
</html>