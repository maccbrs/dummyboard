<div class="modal fade" id="dispoModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title">No Contact - Immediate Call Dispo</h4>
            </div>
            
            <div class="modal-body">
                <p><b>Disposition :</b>

                <select id="selectModalDispo"  class="form-control pull-right" style="width: 300px" name="disposition">
                    <option value="" style="display:none">Please select</option>
                    <option value="Lead Qualified">LQ - Lead Qualified</option>
                    <option value="Call Back">CB - Call Back</option>
                    <option value="Not Interested">NI - Not Interested</option>
                    <option value="Hang Up">HU - Hang Up</option>
                    <option value="No Answer">NA - No Answer</option>
                    <option value="Answering Machine">AM - Answering Machine</option>
                    <option value="Hang Up During Pitch">HUP - Hang Up During Pitch</option>
                    <option value="Decision Maker Not Available">DMNA - Decision Maker Not Available</option>
                    <option value="Wrong Number">WN - Wrong Number</option>
                    <option value="Busy tone">BUSY - Busy tone</option>
                    <option value="Do not call">DNC - Do not call</option>
                     <option value="Disconected No">DN - Disconected No</option>
                    <option value="Fax  Machine">FM - Fax  Machine</option>
                    <option value="Language Barrier">LB - Language Barrier</option>
                    <option value="Bad Connection">BC - Bad Connection</option>
                    <option value="EMAIL">EMAIL - Email</option>
                    <option value="For Update">FU - For Update</option>
                </select>
                </p>
            </div>

            <div class="modal-footer">
                <p>
                    <button type="submit" class="btn btn-sm">Submit</button>
                </p>
                <button type="submit" onclick="$('#selectMainDispo').attr('required', false);">Submit</button>
            </div>
        </div>
    </div>
</div>


