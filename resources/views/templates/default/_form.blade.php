<?php
  if(!empty($page->contents)){
    $contents = json_decode($page->contents);
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
  }
?>

    <div id="page-wrapper"> 
    	<div class="m-t"></div>
        <div class="row">
            <form enctype="multipart/form-data" action="{{url('dashboard/page/edit/'.$page->id)}}" method="post">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="id" value="{{$page->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">  


        	<div class="col-md-12">
				<div class="panel panel-default">
                    <div class="panel-heading clearfix">
                       Default template 

                       <button type="submit" class="btn btn-default pull-right">update</button> <a href="{{url('preview/'.$page->campaign_id.'/'.$page->id)}}" target="_tab" class=" preview btn btn-default pull-right">Preview</a>
                       <a href="{{url('dashboard/boards/edit/'.$page->campaign_id)}}" class="preview btn btn-default pull-right">Board Main</a>
                    </div>
                    <div class="panel-body">
                       <div class="col-md-10 col-md-offset-1">
                              @include('flash::message')
                              <div class="panel panel-primary">
                                 <div class="panel-heading">
                                    Header Block
                                 </div>
                                 <div class="panel-body">
                                    <div class="form-group">
                                        <label>Heading Logo or Banner (suggested dimension 100x600px)</label> 
                                        <input type="file" name="logo">
                                        <?php if(isset($arr_contents['logo'])){?>
                                         <img src="<?= url('uploads/'.$arr_contents['logo'])?>">
                                        <?php } ?>
                                    </div>
                                 </div>
                              </div>


                              <div class="panel panel-primary">
                                 <div class="panel-heading">
                                    Left Block
                                 </div>
                                 <div class="panel-body">
                                    <textarea id="editor" class="form-control" rows="20" height="100px" name="content-l"><?= (isset($arr_contents['content-l'])?$arr_contents['content-l']:'') ?></textarea>
                                 </div>
                              </div>

                              <div class="panel panel-primary">
                                 <div class="panel-heading">
                                    Right block - <small>text contents</small>
                                 </div>
                                 <div class="panel-body">
                                    <textarea id="editor2" class="form-control" rows="20" height="100px" name="content-r"><?= (isset($arr_contents['content-r'])?$arr_contents['content-r']:'') ?></textarea>
                                 </div>
                              </div>

                       </div>
                    </div>
				</div>
        	</div>
            </form>
        </div>
    </div>