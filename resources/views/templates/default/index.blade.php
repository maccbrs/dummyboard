@extends('templates.default.master')
@section('title', 'Page Title')
<?php
if(!empty($page->contents)){
$contents = json_decode($page->contents);
foreach ($contents as $k => $v) {
$arr_contents[$k] = $v;
}
}

?>
@section('content')

<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="pageid" value="{{$page->id}}">
    <input type="hidden" name="hidden_lead_id" value="{{(isset($_GET['lead_id'])?$_GET['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_GET['phone_number'])?$_GET['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_GET['user'])?$_GET['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')?'test':'live')}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
	<div id="main-container">
		<div class="container">

			@if (Session::has('flash_notification.message'))
				@include('templates.'.$page->template.'._success')
			@else

			<div class="row first-row">
				<div class="col-md-12">
					<div class="panel panel-default">
	                    <div class="panel-heading clearfix">
	                       {{$page->title}} 
	                       <div class="pull-right">
	                       	<label>test email:</label>
	                       	<input type="text" name="test_email">
	                       </div>
	                       @foreach($pages as $p)
	                       		@if($p->id != $page->id)
	                   
	                       		<a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class=" preview btn btn-default pull-right">{{$p->title}}</a>
	                       		@endif
	                       @endforeach
	                    </div>
						<div class="panel-body">
							<?php if(isset($arr_contents['logo'])){?>
							<img style="max-height: 50px;" src="<?= url('uploads/'.$arr_contents['logo'])?>">
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
							<?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l']):'') ?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
							<?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r']):'') ?>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</form>
@endsection