			<div class="row first-row">
				<div class="col-md-12">
					<div class="panel panel-default">
	                    <div class="panel-heading clearfix">
	                       {{$page->title}} 
	                       @foreach($pages as $p)
	                       		@if($p->id != $page->id)
	                       		<a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class=" preview btn btn-default pull-right">{{$p->title}}</a>
	                       		@endif
	                       @endforeach
	                    </div>
						<div class="panel-body">
							<?php if(isset($arr_contents['logo'])){?>
							<img src="<?= url('uploads/'.$arr_contents['logo'])?>">
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
					    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{ Session::get('flash_notification.message') }}
					    </div>
					</div>
				</div>
			</div>