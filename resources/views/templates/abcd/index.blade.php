@extends('templates.paperchase.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 

      foreach ($contents as $k => $v) {

        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php

$lead_info = json_encode($_GET);
$lead = json_decode($lead_info);


  
  $fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
  
  

?>
@section('custom_head')


<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    $(":text").attr("required", false);
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");

    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>

    @include('templates.paperchase._dm-job-titles-modal')

    <form id="mainForm" enctype="multipart/form-data" action="{{route('page.edit2.save',$page->id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
      <input type="hidden" name="pageid" value="{{$page->id}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all" value="{{json_encode($_GET)}}">

      <input type="hidden" name="disposition" value="">

      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-7">
                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">
                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="col-md-5 " style = "max-height: 100%;position:fixed;top:10px; right:10px ;">
              <div style="border: solid 1px; border-color: gainsboro; padding: 10px;">

                @if($link == 'preview')
            
                <div class="pull-right">
                    <label>test email:</label>
                    <input type="text" name="test_email">
                    <br>
                </div>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right">
                  <label>test email:</label>
                  <input type="text" name="test_email">
                  <br>
                </div>

            @endif
            
            {{ Session::get('flash_notification.message') }}

                @foreach($pages as $p)

                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    @if($link == 'live')

                        @if($p->id != $page->id && in_array($p->id,$btns))
                            
                            <a href="{{url('live/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn">{{$p->title}}</a> 
                        
                        @endif

                    @else

                        @if($p->id != $page->id && in_array($p->id,$btns))

                            <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn" <?php if ($pos === false){  } else { echo "target='_blank'";} ?> >{{$p->title}}</a> 
                       
                        @endif

                    @endif

                @endforeach

        

                <br>

                <div class ="populate">

                  <?php //$test = json_encode($arr_contents['content-r']); 
                    //foreach ($test as $key => $value) {

                  //  $content_vici = explode(",", $arr_contents['content-r']);

                  $content_vici = explode(",",$arr_contents['content-r']); ?>

                  @foreach ($content_vici as $key => $value)
                    <?php   $vici_value = explode(":",$value); ?> 
               
                    <p></b> <input type="hidden" name="CompanyName" value="{{$vici_value[0]}}">  </p>
                  @endforeach
                 
                  <?php if($arr_contents['content-r'] != ''):

                    $arr_fields = [];
                    $cont_a = preg_replace("/\s+/", " ",$arr_contents['content-r']);
                    $cont_b = explode(";", $cont_a);
                    
                    foreach($cont_b as $b):
                      if($b):
                        $trimmed = explode(":", trim($b));
                        if(count($trimmed) > 1):
                          $arr_fields[$trimmed[0]] = $trimmed[1];
                        endif;
                      endif;
                    endforeach;
                    foreach(array_keys($_GET) as $v):
                      if(isset($arr_fields[$v])):
                        echo '<label>'.$arr_fields[$v].':</label><input name="'.$arr_fields[$v].'" value="'.$_GET[$v].'"><br>';
                      endif;
                    endforeach;
                   ?>
                   
                  <?php endif; ?>

                </div>

                <div class="populate">
                    <a data-toggle='modal' data-target='#jobTitlesModal' class='btn'>DM Job Titles</a>
                </div>
                <div class="populate">
                    <button type="submit" class='btn'>Submit</button>
                </div>                    
                <div class="populate">
                    <p><b>Disposition :</b>

                        <select class="form-control pull-right" required style ="width:300;" name = "disposition">
                          <option value="" >Please select</option>
                          <option value="LQ">LQ - Lead Qualified</option>
                          <option value="CB">CB - Call Back</option>
                          <option value="NI">NI - Not Interested</option>
                          <option value="HU">HU - Hang Up</option>
                          <option value="NA">NA - No Answer</option>
                          <option value="AM">AM - Answering Machine</option>
                          <option value="HUP">HUP - Hang Up During Pitch</option>
                          <option value="DMNA">DMNA - Decision Maker Not Available</option>
                          <option value="WN">WN - Wrong Number</option>
                          <option value="BUSY">BUSY - Busy tone</option>
                          <option value="DNC">DNC - Do not call</option>
                           <option value="DN">DN - Disconected No</option>
                          <option value="FM">FM - Fax  Machine</option>
                          <option value="LB">LB - Language Barrier</option>
                          <option value="BC">BC - Bad Connection</option>
                          <option value="EMAIL">EMAIL - Email</option>
                          <option value="NQ">NQ - Not Qualified</option>
                          <option value="FU">FU - For Update</option>
                          <option value="PL">PL - Profiled Lead</option>
                        </select>
                    </p>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>

          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


.intakeForm-darken>header {
    border-color: #ff6961;
    background: #ff6961;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #ff6961 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

.populate {
    margin-top: 6px !important;
    background-color: #e3e3e3;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
.well{

  background-color:#e3e3e3!important;
}


</style>
