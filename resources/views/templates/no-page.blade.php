@extends('templates.default.master')
@section('title', 'No Page')

@section('content')
<div id="main-container">
	<div class="container">

		<div class="row first-row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<h2>No page found for this board</h2>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection