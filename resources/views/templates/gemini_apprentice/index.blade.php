@extends('templates.gemini_apprentice.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 

      foreach ($contents as $k => $v) {

        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

  $fname= !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $lname= !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $user= !empty($_GET["user"]) ?  $_GET["user"] : ""; 

?>


<?php

$lead_info = json_encode($_GET);
$lead = json_decode($lead_info);

$fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
?>
@section('custom_head')


<script>

$(document).ready(function(){
  $("SELECT").prop('required',true);

    $("#selectMainDispo").attr("required", false);
    $(":text").attr("required", false);
    $(":radio").attr("required", false);

    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    })

    $("#NoContactBtn").find("p").append("<button type='button' data-toggle='modal' data-target='#dispoModal' class='btn btn-sm' style='width:200px' >No Contact</button>");

    $("#selectMainDispo").on("change", function () {
        var dispo = $("select[name='selectMainDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#selectModalDispo").on("change", function () {
        var dispo = $("select[name='selectModalDispo']").val();
        $("input[name='disposition']").val(dispo);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", true);
        $(":radio").attr("required", true);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });

    $("#dispoModal").on("hidden.bs.modal", function () {
        $(":text").attr("required", false);
        $(":radio").attr("required", false);
        $("#selectModalDispo").attr("required", false);
        $("#selectMainDispo").attr("required", false);
    });
});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>

    <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>

      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">
      @if($page['campaign_id'] != '59b96bfe41291')
      <input type="hidden" name="disposition" value="">
      @endif
      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-12">
                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                    
                    @if($link == 'preview')
            
                      <div class="pull-right">
                          <label>test email:</label>
                          <input type="text" name="test_email">
                          <br>
                      </div>

                    @elseif($link == 'train' && Auth::check())
                  
                      <div class="pull-right">
                        <label>test email:</label>
                        <input type="text" name="test_email">
                        <br>
                      </div>

                    @endif

                        <div class="well scriptWell">

                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif

                              @if($page['campaign_id'] == '59fbb3f53dde9')
                                <input type ="text" name ="to">
                              @endif
                        </div>
                        
                    </div>
                </div>
            </div>


    </div>
    </div>
</div>

          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


.intakeForm-darken>header {
    border-color: #506987;
    background: #506987;
    color: #fff;
    height: 70px;
        font-family: Impact, Charcoal, sans-serif !important;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #506987 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color: #e3e3e3;!important;
}

.populate {
    margin-top: 6px !important;
    background-color: #e3e3e3;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}
.well{

  background-color:#e3e3e3!important;
}


</style>
