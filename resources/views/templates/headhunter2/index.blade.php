@extends('templates.headhunter2.master')
@section('title', 'Page Title')

<?php

  if(!empty($page->contents)){
      $contents = json_decode($page->contents); 
      foreach ($contents as $k => $v) {
        $arr_contents[$k] = $v;
      }
  }

$email_subject = '';
$option = json_decode($page->options);

if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  
  $fullname = !empty($_GET['fullname']) ? $_GET['fullname'] : "n/a";
  
  $source_id = !empty($_GET["SourceDetailsSourceDetails"]) ?  $_GET["source_id"] : "";
  $IDDetail = !empty($_GET["IDDetail"]) ?  $_GET["IDDetail"] : "";
  $first_name = !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $last_name = !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $email = !empty($_GET["email"]) ?  $_GET["email"] : ""; 
  $country_code = !empty($_GET["country_code"]) ?  $_GET["country_code"] : ""; 
  $phone = !empty($_GET["phone"]) ?  $_GET["phone"] : ""; 
  $CurrentLocation = !empty($_GET["CurrentLocation"]) ?  $_GET["CurrentLocation"] : "";
  $Nationality = !empty($_GET["Nationality"]) ?  $_GET["Nationality"] : "";
  $Salary = !empty($_GET["Salary"]) ?  $_GET["Salary"] : "";
  $WorkExperience = !empty($_GET["WorkExperience"]) ?  $_GET["WorkExperience"] : "";
  $LatestEmployer = !empty($_GET["LatestEmployer"]) ?  $_GET["LatestEmployer"] : "";
  $Position = !empty($_GET["Position"]) ?  $_GET["Position"] : "";
  $question1 = !empty($_GET["Question1"]) ?  $_GET["Question1"] : "";
  $question2 = !empty($_GET["Question2"]) ?  $_GET["Question2"] : "";
  $question3 = !empty($_GET["Question3"]) ?  $_GET["Question3"] : "";
  $question4 = !empty($_GET["Question4"]) ?  $_GET["Question4"] : "";
  $Age = !empty($_GET["Age"]) ?  $_GET["Age"] : "";
  $JobID = !empty($_GET["JobID"]) ?  $_GET["JobID"] : "";
  $OfferCompanyName  = !empty($_GET["OfferCompanyName"]) ?  $_GET["OfferCompanyName"] : "";
  $OfferCompanyInformation  = !empty($_GET["OfferCompanyInformation"]) ?  $_GET["OfferCompanyInformation"] : "";
  $OfferJobTitle = !empty($_GET["OfferJobTitle"]) ?  $_GET["OfferJobTitle"] : "";
  $OfferSalaryRange = !empty($_GET["OfferSalaryRange"]) ?  $_GET["OfferSalaryRange"] : "";
  $OfferJobdescription  = !empty($_GET["OfferJobdescription"]) ?  $_GET["OfferJobdescription"] : "";
  $candidatename = !empty($_GET["fullname"]) ?  $_GET["fullname"] : ""; 

?>
@section('custom_head')


<script>

$(document).ready(function(){
    $("input[name='test_email']").change(function(){
       $("input[name*='test_email']").val($(this).val());
    });


});
  </script>
@endsection

@section('content')

  @if($fullname == null && $link = 'live' )
    
    <div class="noleadid">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Live Dummy Board Cannot be Used without a Call

    </div>
         
 
  @else

    <input type="hidden" id="action_link" value ="{{url('data/save/'.$page->campaign_id)}}"/> 
    <input type="hidden" id="csrf_Val" value="{{ csrf_token() }}"/>


    <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
      
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="pageid" value="{{$page->id}}">
      <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
      <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
      <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
      <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
      <input type="hidden" name="email_subject" value="{{$email_subject}}">
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
      <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
      <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

      <input type="hidden" id="campaign_id" value="{{$page->campaign_id}}">

      <div id="wrapper">

    <div>
        
    <div class="container-fluid intakeFormContainer">
        <div class="row">
            <div class="col-md-7">
                <div class="intakeForm intakeForm-darken" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2>{{$board['lob']}}</h2>
                    </header>
                    <div class="row">
                        <div class="well scriptWell">
                            @if (Session::has('flash_notification.message'))
                                @include('templates.'.$page->template.'._success')
                            @else
                                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

                              @if($page['campaign_id'] == '57f544bb414ed')
                                <input type ="hidden" name ="agent" value = "<?php echo $fullname?>">
                              @endif
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="col-md-5 " style = "max-height: 100%;position:fixed;top:10px; right:10px ;">
              <div style="border: solid 1px; border-color: gainsboro; padding: 10px;">

                @if($link == 'preview')
            
                <div class="pull-right">
                    <label>test email:</label>
                    <input type="text" name="test_email">
                    <br>
                </div>

                @elseif($link == 'train' && Auth::check())
                  
                <div class="pull-right">
                  <label>test email:</label>
                  <input type="text" name="test_email">
                  <br>
                </div>

            @endif
            
            {{ Session::get('flash_notification.message') }}

                @foreach($pages as $p)

                    <?php $str = strtolower($p->title);
                        $findme   = 'tracking ids';
                        $pos = strpos($str, $findme);
                    ?>

                    @if($link == 'live')

                        @if($p->id != $page->id && in_array($p->id,$btns))
                            
                            <a href="{{url('live/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn">{{$p->title}}</a> 
                        
                        @endif

                    @else

                        @if($p->id != $page->id && in_array($p->id,$btns))

                            <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id.'?fullname='.$fullname)}}" class="btn" <?php if ($pos === false){  } else { echo "target='_blank'";} ?> >{{$p->title}}</a> 
                       
                        @endif

                    @endif

                @endforeach

        

                <br>

                <div class ="populate">

                  <div class="row">
                      <div class="col-sm-6"> 

                        <p><b>ID :</b>{{$IDDetail}} <input type="hidden" name="ID" value="{{$IDDetail}}">  </p>
                        <p><b>Job ID:</b> {{$JobID}} <input type="hidden" name="Job ID" value="{{$JobID}}"> </p>
                        <p><b>Offer Company Name :</b> {{$OfferCompanyName}} <input type="hidden" name="Offer Company Name" value="{{$OfferCompanyName}}"> </p>
                        <p><b>Offer Company Information :</b>{{$OfferCompanyInformation}} <input type="hidden" name="Offer Company Information" value="{{$OfferCompanyInformation}}"> </p>
                        <p><b>Offer Job Title:</b>{{$OfferJobTitle}} <input type="hidden" name="Offer Job Title" value="{{$OfferJobTitle}}"> </p>
                        <p><b>Offer Job Description:</b>{{$OfferJobdescription}} <input type="hidden" name="Offer Job Description" value="{{$OfferJobdescription}}"> </p>
                        <p> <b>Position:</b> {{$Position}} <input type="hidden" name="Position" value="{{$Position}}"> </p>
                        <p> <b>Work Experience:</b> {{$WorkExperience}} <input type="hidden" name="Work Experience" value="{{$WorkExperience}}"> </p>
                        <p><b>Latest Employer:</b>{{$LatestEmployer}}<input type="hidden" name="LatestEmployer" value="{{$LatestEmployer}}"></p>
                      
                      </div>
                      <div class="col-sm-6">
                        <p><b>Candidate Name:</b> {{$first_name . " " . $last_name}}  <input type="hidden" name="Candidate Name" value="{{$first_name . " " . $last_name}}"> </p>
                        <p><b>Email Address:</b>{{$email}}<input type="hidden" name="Email" value="{{$email}}"> </p>
                        <p><b>Phone Number :</b>{{$phone}}<input type="hidden" name="Phone Number" value="{{$phone}}"></p>
                        <p><b>Age :</b>{{$Age}}<input type="hidden" name="updated age" value="{{$Age}}"> </p>
                        <p><b>Salary:</b>{{$Salary}} <input type="hidden" name="salary" value="{{$Salary}}"> </p>
                        <p><b>Offer Salary Range:</b>{{$OfferSalaryRange}} <input type="hidden" name="Offer Salary Range" value="{{$OfferSalaryRange}}"> </p>   
                        <p><b>COUNTRY_CODE_PHONE :</b>{{$country_code}}<input type="hidden" name="COUNTRY_CODE_PHONE" value="{{$country_code}}">  </p>
                        <p><b>Source :</b>{{$source_id}}<input type="hidden" name="Source" value="{{$source_id}}">  </p>
                        <p><b>Current Location:</b>{{$CurrentLocation}} <input type="hidden" name="Current Location" value="{{$CurrentLocation}}"> </p>
                        <p><b>Nationality :</b>{{$Nationality}} <input type="hidden" name="Nationality" value="{{$Nationality}}"></p>
                        
                        <p><b>Question 1:</b>{{$question1}}<br>
                        <input type="radio" name="{{$question1}}" value="yes">Yes
                        <input type="radio" name="{{$question1}}" value="no">No </p>
                        <p><b>Question 2:</b>{{$question2}}<br>
                        <input type="radio" name="{{$question2}}" value="yes">Yes
                        <input type="radio" name="{{$question2}}" value="no">No </p>
                        <p><b>Question 3:</b>{{$question3}}<br>
                        <input type="radio" name="{{$question3}}" value="yes">Yes
                        <input type="radio" name="{{$question3}}" value="no">No</p>
                        <p><b>Question 4 :</b>{{$question4}}<br>
                        <input type="radio" name="{{$question4}}" value="yes">Yes
                        <input type="radio" name="{{$question4}}" value="no">No</p>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>


    </div>

</div>



          @endif
        </div>
      </div>
  @endif

@endsection

<style>

  .container{

    width:100% !important;
  }

.intakeFormContainer {
    margin: 1% 3% 0 3%;
}

.intakeForm>header {
    height: 70px;
    padding-left: 10px;
    line-height: 68px;
    font-family: 'Open Sans', Sans-Serif;
}

header > ul > li > a {
    font-size: 1em !important;
    color: white;
    background-color: #0aa66e;
    padding: 2px 5px 2px 5px;
    font-weight: 600;
    text-transform: uppercase;
    transition-duration: 0.5s;
}

.intakeForm>div {
    float: left;
    width: 100%;
    position: relative;
    font-size: 13px;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
    margin: 0;
    border-width: 1px 1px 2px;
    border-style: solid;
    border-top: none;
    border-right-color: #CCC!important;
    border-bottom-color: #CCC!important;
    border-left-color: #CCC!important;
    padding: 13px 13px 0;
    overflow: visible
}

.intakeForm>header {
    color: #333;
    border: 1px solid #C2C2C2;
    background: #fafafa
}


.intakeForm-darken>header {
    border-color: #383838!important;
    background: #404040;
    color: #fff;
    height: 70px;
}

.btn{
    width: 100%;
    /* background-color: #659EC7 !important; */
    background-color: #202f42 !important;
    color: white  !important;
    font-size: 20px  !important;
/*    padding: 0px !important;*/


}

.sm, .btn[type="submit"]{
    width: 25%;
    text-align: center;
}

.scriptWell{
    background-color:#659ec7 !important;
}

.populate {
    margin-top: 6px !important;
    background-color: #659ec7;
    padding: 12.5px !important;
    font-size: 16px !important;
    line-height: 17px !important;
}


</style>
