<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/first/custom.css')}}" rel="stylesheet">
  </head>
  <body>

    @yield('content')

    <script src="{{URL::asset('bootstrap/js/jquery.js')}}"></script>
    <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">
    
        $( document ).ready(function() {
          $('.block1').show();
          $(":radio[value=yes]").on('click',function(){
          $('select').html('');
          $('select').append('<option value=""> </option>');

        });

        $(":radio[value=no]").on('click',function(){
          $('select').html('');
          $('select').prop( "disabled", false );
          $('select').append('<option value="" disabled selected>Please Select</option>');
          $('select').append('<option value="General Inquiry">General Inquiry</option>');
          $('select').append('<option value="Hang Up">Hang Up</option>');
          $('select').append('<option value="Wrong Number">Wrong Number</option>');
          $('select').append('<option value="No Answer">No Answer</option>');

        });       
      });

      function myFunction() {
       
        $('.block1').hide();
        $('.block2').show();

      }

      function myFunction2() {
       
        $('.block2').hide();
        $('.block3').show();

      }

      function myFunction3() {
       
        $('.block3').hide();
        $('.block4').show();

      }

      

      </script>

      <style>

        .block1{
          display: none;
        }

        .block2{
          display: none;
        }

        .block3{
          display: none;
        }

        .block4{
          display: none;
        }

      </style>

  </body>
</html>