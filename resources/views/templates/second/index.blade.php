@extends('templates.second.master')
@section('title', 'Page Title')

<?php

if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}

$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

@section('content')
<style type="text/css">
  div.absolute {
    position: absolute;
    top: 0px;
} 
</style>


<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="pageid" value="{{$page->id}}">
    <input type="hidden" name="hidden_lead_id" value="{{(isset($_GET['lead_id'])?$_GET['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_GET['phone_number'])?$_GET['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_GET['user'])?$_GET['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
    <input type="hidden" name="email_subject" value="{{$email_subject}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

    <div class="container">
      <div class="top">
        
          @if($link == 'preview')
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @elseif($link == 'train' && Auth::check())
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @endif
          
           {{ Session::get('flash_notification.message') }}

      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx block1">
            <br>
                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>
                <center>
                <input onclick="myFunction()" type = "button" value = "next" style = "display: inline-block; margin: 5px 20px;width: 285px;background-color: #D3D3D3;box-shadow: inset 0 0 10px #333;border-radius: 3px;font-size: 20px;text-align: center;font-weight: bold;">
                </center>
                <br><br>
          </div>
          
          <div class="bx o-l main-bx block2">
              <br>
                <?= (isset($arr_contents['content-2'])?parse_input($arr_contents['content-2'],$url):'') ?>
                <center>
                  <input onclick="myFunction2()" type = "button" value = "next" style = "display: inline-block; margin: 5px 20px;width: 285px;background-color: #D3D3D3;box-shadow: inset 0 0 10px #333;border-radius: 3px;font-size: 20px;text-align: center;font-weight: bold;">
                </center>
          </div>
       
          <div class="bx o-l main-bx block3">
            <br>
                <?= (isset($arr_contents['content-3'])?parse_input($arr_contents['content-3'],$url):'') ?>

              @if (in_array($page['campaign_id'], $omnitrixBoards))

            <center>
              <input onclick="myFunction3()" type = "button" value = "next" style = "display: inline-block; margin: 5px 20px;width: 285px;background-color: #D3D3D3;box-shadow: inset 0 0 10px #333;border-radius: 3px;font-size: 20px;text-align: center;font-weight: bold;">
            </center>
          </div>

          <div class="bx o-l main-bx block4">
            <br>
                <?= (isset($arr_contents['content-4'])?parse_input($arr_contents['content-4'],$url):'') ?>                
          </div>

              <div class = "absolute">
                <br> 
                @if($page['campaign_id'] != '5841c3b6b8ecd') <p><b>Agent's name: </b><input type ="text" name ="Agent_Name" tabindex="1" value = "" style = "width: 300px;" required></p>@endif
                <br>
              </div>
              @endif

      </div>

      <div class="col-md-4 sidebar ">
         <div class="bx o-l">
            <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'') ?>
         </div>
         <div class="bx o-l">
              <div class="bottom-r">
                 @foreach($pages as $p)

                    @if($link == 'live')

                      @if($p->id != $page->id && in_array($p->id,$btns))
                        <a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                      @endif

                    @else

                      @if($p->id != $page->id && in_array($p->id,$btns))
                        <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                      @endif

                    @endif
                 @endforeach
              </div>
         </div>
      </div>
        @endif
      </div>
</form>   

@endsection


