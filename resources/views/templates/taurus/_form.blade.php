<?php

  if(!empty($page->contents)){
    $contents = json_decode($page->contents);
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
  }

  $email_subject = '';
  $vici_info = json_decode($page->vici_info);
  $option = json_decode($page->options);

  if(!empty($option)){
    $email_subject = (isset($option->email_subject)?$option->email_subject:'');
  }

?>
    <div id="page-wrapper"> 
    	<div class="m-t"></div>
        <div class="row">
          <form enctype="multipart/form-data" action="{{url('dashboard/page/edit/'.$page->id)}}" method="post">
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="id" value="{{$page->id}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">  

          	<div class="col-md-12">
  				    <div class="panel panel-default">

              <div class="panel-heading clearfix">
                <div class="col-md-4">
                    <div class="form-group input-group">
                       <span class="input-group-addon">Title</span>
                       <input type="text" class="form-control" name="title" required value="{{(isset($page->title)?$page->title:'')}}" >
                    </div>                        
                </div>

                <div class="col-md-4">
                    <div class="form-group input-group">
                       <span class="input-group-addon">Email Subject</span>
                       <input type="text" class="form-control" name="email_subject" required value="{{$email_subject}}" >
  					
                    </div>                        
                </div>  

                <div class="col-md-4">
                      <button type="submit" class="btn btn-default pull-right">update</button> <a href="{{url('preview/'.$page->campaign_id.'/'.$page->id)}}" target="_tab" class=" preview btn btn-default pull-right">Preview</a>
                      <a href="{{url('dashboard/boards/edit/'.$page->campaign_id)}}" class="preview btn btn-default pull-right">Board Main</a>                        
                </div>      
  			  
          				<div class="col-md-4">
          				  <br/>
                      <div class="form-group input-group">
                        <span class="input-group-addon">Background-color</span>
          					   <input type="color" name="content-2" value="<?= (isset($arr_contents['content-2'])?$arr_contents['content-2']:'#00ffff') ?>" class="form-control" >
                    </div>                        
                </div>		  
              </div>
        
              <div class="panel-body">
                 <div class="col-md-10 col-md-offset-1">
                        @include('flash::message')

                  <div class="panel panel-primary">
                     <div class="panel-heading">
                        Left Block
                     </div>
                     <div class="panel-body">
                        <textarea id="editor" class="form-control" rows="20" height="100px" name="content-l"><?= (isset($arr_contents['content-l'])?$arr_contents['content-l']:'') ?></textarea>
                     </div>
                  </div>

                  <div class="panel panel-primary">
                     <div class="panel-heading">
                       Vici block - <small>vici fields assignment in array</small>
                     </div>
                     <div class="panel-body">
                        <textarea id="editor2" class="form-control" rows="20" height="100px" name="content-r"><?= (isset($arr_contents['content-r'])?$arr_contents['content-r']:'') ?></textarea>
                     </div>
                  </div>

                 </div>
              </div>
  				  </div>

        	 </div>
          </form>
      </div>
  </div>



  <script>

      $( ".add-custom" ).click(function() {
        alert( "Handler for .click() called." );
    });

  </script>