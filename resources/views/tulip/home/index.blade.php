<?php $asset = URL::asset('/'); ?> 
@extends('tulip.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                    <div class="row">
                      <div class="row">
                        <div class="col-md-6"><h3>Submitted Call Report</h3> </div>
                      </div>
                    </div>

                       <div class = "container" style = "margin-left: 10px;">
                            <div class ="row">
                                <h2>Campaign: 
                                    <select id = "selectCampaign">
                                        <option class = "selected" value="Magellan IVR">Magellan IVR</option>
                                    @foreach($campaignData as $key => $cd)
                                        <option disabled value="{{$key}}">{{$cd}}</option>
                                    @endforeach
                                    </select>
                                </h2>
                            </div>
                        </div>

                    <section id = "getRequestData"></section>

                </div>
            </div>
        </div>

    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script>

    $(document).ready(function(){

        // Commented are for Dynamic. Please do not Remove

        // $("#selectCampaign").change(function(){
        //     campaign = $("#selectCampaign").val();
        //     url = "/tulip/generate/";
        //     urlComp = url.concat(campaign);
            
        //     $.ajax({
        //         type: "GET",
        //         url : urlComp,
        //        // data : dataString,
        //         success : function(data){
        //             $(".removeDiv").remove();
        //             $("#getRequestData").append(data);
                    
        //         }  
        //     });
        // });

        campaign = "IVRMagellan";
        url = "/tulip/generate/";
        urlComp = url.concat(campaign);
        
        $.ajax({
            type: "GET",
            url : urlComp,
           // data : dataString,
            success : function(data){
                $(".removeDiv").remove();
                $("#getRequestData").append(data);
                
            }  
        });
    });

</script>

<style>

    .table-container{
        width: 100%;
        overflow-y: auto;
        _overflow: auto;
        margin: 0 0 1em;
    }

    .tdHead{
        background: #2B547E;
        color: white;
        font-weight: bold;
        padding: 6px;
        border: 1px solid #ccc;
        text-align: center;
    }

</style>

@endsection