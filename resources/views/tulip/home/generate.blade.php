
@foreach($callHolder as $key => $call) 

        @foreach($call as $callkey => $callList) 

        <div class = "removeDiv">
            @if($callkey == 0) 
                
                <h3>{{$key}}</h3>
                
            @else

                @if((($callList['created_at']) != $dateHolder) && !empty($callList['id']))
                    <h3>{{$key}}</h3>
                @endif
            @endif

        </div>

            <?php $dateHolder = $callList['created_at']; ?>
        
        <div class = "container removeDiv table-container">

            <table class="table table-bordered ">
                <thead>
                    @foreach($callList['contents'] as $callkey2 => $callList2) 
                        <td class ="tdHead">{{$callkey2}}</td>
                    @endforeach 
                </thead>
                <tbody>
                    <tr>
                        @foreach($callList['contents'] as $callkey2 => $callList2)
                            <td>{{$callList2}}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>   
        @endforeach

    
@endforeach
