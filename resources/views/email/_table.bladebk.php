<table class="o" style=" border-radius: 4px; border-collapse: collapse !important;width: 800px;height: 580px;margin: 10px auto; position:relative; display:block !important;border: 1px solid #ddd;">
    <tr style="height:36px;background-color:#f8f8f8;width:800px;display:block;line-height:30px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:#555;text-indent:8px;    border-bottom: 1px solid #ddd;" >
       <td style=""> Magellan Reporting Service</td>
    </tr>
    <tr style="width:790px; margin:20px 3px;display:block;" class="o">
       <td >
          <table style="border: 1px solid #ddd;width:788px;display: table;border-collapse: collapse !important;">
            <tr >
              @foreach($keys as $key)
              <th style="border: 1px solid #ddd;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;padding: 5px;border-bottom: 3px solid #ddd;">{{$key}}</th>
              @endforeach
            </tr>
            <tr>
              @if(isset($value[0]))
                @foreach($value[0] as $val)
                <td style="border: 1px solid #ddd;">{{$val}}</td>
                @endforeach
              @endif
            </tr>
                                      
          </table>
       </td>
    </tr>
</table>

<?php exit; ?>