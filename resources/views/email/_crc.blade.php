<div class="row">
  <div class = "container">
    <div class="col-xs-9" >
      <span style = "font-size: 22px;">
        {{$label}} &nbsp;&nbsp;&nbsp; <b style = "color: #337ab7;">{{$details}} </b>
      </span>
    </div>
 </div>
</div>

<br>

<div class="bs-example" data-example-id="striped-table">
  <table class="table table-striped">
    <thead >
      <tr >
        <th scope="row">Lead ID</th>
        <th scope="row">Campaign ID</th>
        <th scope="row">Call Date - Time</th>
        <th scope="row">Dispostion</th>
        <th scope="row">Phone Number</th>
        <th scope="row">User</th>
        <th scope="row">Action</th>
      </tr>
      @if(!empty($logs))
        @foreach($logs as $log)
          <tr>
            <td> {{$log->lead_id}} </td>
            <td> {{$log->campaign_id}} </td>
            <td> {{$log->call_date}} </td>
            <td> {{$log->status}} </td>
            <td> {{$log->phone_number}} </td>
            <td> {{$log->user}} </td>
            <td>
              <?php $crcData = [];
              $crcData['lead_id'] = $log->lead_id;
              $crcData['campaign_id'] = $log->campaign_id;
              $crcData['status'] = $log->status;
              ?>
              <a class="btn btn-primary btn-submit edit-submit" data-toggle="modal" href="#myModal" data-uniqueid = "{{$log->uniqueid}}" data-campaign = "{{$log->campaign_id}}" data-lead = "{{$log->lead_id}}" data-table = "vicidial_log" data-server = "{{$conn}}" data-status = "{{$log->status}}"  data-target="#edit-modal">Edit </a>
            </td>
          </tr>
        @endforeach
      @endif

      @if(!empty($logs_closer))
        @foreach($logs_closer as $log_closer)
          <tr>
            <td> {{$log_closer->lead_id}} </td>
            <td> {{$log_closer->campaign_id}} </td>
            <td> {{$log_closer->call_date}} </td>
            <td> {{$log_closer->status}} </td>
            <td> {{$log_closer->phone_number}} </td>
            <td> {{$log_closer->user}} </td>
            <td>
              <?php $crcData = [];
              $crcData['lead_id'] = $log_closer->lead_id;
              $crcData['campaign_id'] = $log_closer->campaign_id;
              $crcData['status'] = $log_closer->status;
              ?>
              <a class="btn btn-primary btn-submit edit-submit" data-toggle="modal" href="#myModal" data-uniqueid = "{{$log_closer->uniqueid}}" data-campaign = "{{$log_closer->campaign_id}}" data-lead = "{{$log_closer->lead_id}}" data-table = "vicidial_closer_log" data-server = "{{$conn}}"  data-status = "{{$log_closer->status}}"  data-target="#edit-modal">Edit </a>
            </td>
          </tr>
        @endforeach
      @endif

      @if(!empty($logs2))
        @foreach($logs2 as $log2)
          <tr>
            <td> {{$log2->lead_id}} </td>
            <td> {{$log2->campaign_id}} </td>
            <td> {{$log2->call_date}} </td>
            <td> {{$log2->status}} </td>
            <td> {{$log2->phone_number}} </td>
            <td> {{$log2->user}} </td>
            <td>
              <?php $crcData = [];
              $crcData['lead_id'] = $log2->lead_id;
              $crcData['campaign_id'] = $log2->campaign_id;
              $crcData['status'] = $log2->status;
              ?>
              <a class="btn btn-primary btn-submit edit-submit" data-toggle="modal" href="#myModal" data-uniqueid = "{{$log2->uniqueid}}" data-campaign = "{{$log2->campaign_id}}" data-lead = "{{$log2->lead_id}}" data-table = "vicidial_log" data-server = "{{$conn2}}" data-status = "{{$log2->status}}"  data-target="#edit-modal">Edit </a>
            </td>
          </tr>
        @endforeach
      @endif

      @if(!empty($logs_closer2))
        @foreach($logs_closer2 as $log_closer2)
          <tr>
            <td> {{$log_closer2->lead_id}} </td>
            <td> {{$log_closer2->campaign_id}} </td>
            <td> {{$log_closer2->call_date}} </td>
            <td> {{$log_closer2->status}} </td>
            <td> {{$log_closer2->phone_number}} </td>
            <td> {{$log_closer2->user}} </td>
            <td>
              <?php $crcData = [];
              $crcData['lead_id'] = $log_closer2->lead_id;
              $crcData['campaign_id'] = $log_closer2->campaign_id;
              $crcData['status'] = $log_closer2->status;
              ?>
              <a class="btn btn-primary btn-submit edit-submit" data-toggle="modal" href="#myModal" data-uniqueid = "{{$log_closer2->uniqueid}}" data-campaign = "{{$log_closer2->campaign_id}}" data-lead = "{{$log_closer2->lead_id}}" data-table = "vicidial_closer2_log" data-server = "{{$conn2}}"  data-status = "{{$log_closer2->status}}"  data-target="#edit-modal">Edit </a>
            </td>
          </tr>
        @endforeach
      @endif
      

    </thead>
    <tbody>
      <tr>
      </tr>
    </tbody>
                      
    </table>

</div>
