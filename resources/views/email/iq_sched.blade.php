<?php

  $fname= !empty($_GET["first_name"]) ?  $_GET["first_name"] : ""; 
  $lname= !empty($_GET["last_name"]) ?  $_GET["last_name"] : ""; 
  $user= !empty($_GET["user"]) ?  $_GET["user"] : ""; 

?>


@extends('templates.scorpio.master')
@section('title', 'Page Title')


@section('custom_head')


@endsection

@section('content')


<form action="{{url('iq_sched_post')}}" method="post">      
  <input type="hidden" name="_token" value="{{ csrf_token() }}">  
  <h1> Schedule Email Invite</h1>

  <br>
 
  <div class="form-group">
    <div class="col-md-3">
       <h3><span class = "required_field">*</span>To:</h3>
    </div>
    <div class="col-md-9">
       <input name="to" type="email" class="feedback-input" placeholder="TO (Email)" required/> 
    </div>
  </div>


  <div class="form-group">
    <div class="col-md-3">
      <h4><span class = "required_field">*</span>Name of Client:</h4>
    </div>
    <div class="col-md-9">
      <input name="name"  value ='{{ucfirst($fname)}}' type="text" class="feedback-input" placeholder="Name of Client" required/>     
    </div>
  </div>


  <div class="form-group">
    <div class="col-md-3">
      <h4>Select CC:</h4>
    </div>
    <div class="col-md-9">
      <select class="form-control" id="sel1" name = "cc">
        <option disabled selected>Select One</option>
        <option value = "devon.Corker@logianalytics.com">devon.Corker@logianalytics.com</option>
        <option value = "brian.zurek@logianalytics.com">brian.zurek@logianalytics.com</option>
        <option value = "peter.barrett@logianalytics.com">peter.barrett@logianalytics.com</option>
        <option value = "matthew.ellis@logianalytics.com">matthew.ellis@logianalytics.com</option>
        <option value = "tyler.dixon@logianalytics.com">tyler.dixon@logianalytics.com</option>
        <option value = "harris.husain@logianalytics.com">harris.husain@logianalytics.com</option>
    </select> 
    </div>
  </div>

  <br>

  <div class="form-group">
    <div class="col-md-3">
      <h3><span class = "required_field">*</span>Date:</h3>
    </div>
    <div class="col-md-9">
      <input name="date" type="text" class="feedback-input" placeholder="Date" />   
    </div>
  </div>

  <input name="fname" type="hidden"  value='{{$fname}}' />   
  <input name="lname" type="hidden" value='{{$lname}}'  />  
  <input name="user" type="hidden" id ="user" value='{{$user}}'  />  


  <div class="form-group">
    <div class="col-md-3">
      <h4><span class = "required_field">*</span>Time of Meeting:</h4>
    </div>
    <div class="col-md-9">
      <input name="time" type="text" class="feedback-input" placeholder="Time of meeting" />  
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
      <h5><span class = "required_field">*</span>Part of the Day:</h5>
    </div>
    <div class="col-md-9">
      <select class="form-control" id="sel1" name = "part_of_day">
        <option disabled selected>Select One</option>
        <option value = "afternoon">Afternoon</option>
        <option value = "morning">Morning</option>
    </select>
    </div>
  </div>

  <br>

  <div class="form-group">
    <div class="col-md-3">
      <h4><span class = "required_field">*</span>Timezone:</h4>
    </div>
    <div class="col-md-9">
       <input name="timezone" type="text" class="feedback-input" value = "EST" placeholder="Timezone" />  
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
      <h4><span class = "required_field">*</span>Phone Number:</h4>
    </div>
    <div class="col-md-9">
       <input name="phone_number" type="number" class="feedback-input" placeholder="Phone Number" />  
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
      <h3>Notes:</h3>
    </div>
    <div class="col-md-9">
        <textarea name="notes" class="feedback-input" placeholder="Notes"></textarea>    
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
    </div>
    <div class="col-md-9">
       <input type="submit" value="SEND TO CLIENT"/> 
    </div>
  
  </div>
  

</form>

<style>

@import url(http://fonts.googleapis.com/css?family=Montserrat:400,700);

h1{

  text-align: center;
  color:gray;
}

body { background:  #e3e3e3; }
form { max-width:90%; margin:50px auto; }

.feedback-input {
  color:black;
  font-family: Helvetica, Arial, sans-serif;
  font-weight:500;
  font-size: 18px;
  border-radius: 5px;
  line-height: 22px;
  background-color: transparent;
  border:2px solid #63D1F4;
  transition: all 0.3s;
  padding: 13px;
  margin-bottom: 15px;
  width:100%;
  box-sizing: border-box;
  outline:0;
}

.required_field{

  color:red;
}

.feedback-input:focus { border:2px solid white; color:black;}

textarea {
  height: 150px;
  line-height: 150%;
  resize:vertical;
}

select{

    background-color: transparent !important ;
    border-style: double !important;
    border-color: #63D1F4 !important;
    margin-bottom: 15px !important;
}

[type="submit"] {
  font-family: 'Montserrat', Arial, Helvetica, sans-serif;
  width: 100%;
  background:#63D1F4;
  border-radius:5px;
  border:0;
  cursor:pointer;
  color:white;
  font-size:24px;
  padding-top:10px;
  padding-bottom:10px;
  transition: all 0.3s;
  margin-top:-4px;
  font-weight:700;
  margin-top: 35px;
}
[type="submit"]:hover { background:white; color:  #63D1F4; }
</style>

@endsection



