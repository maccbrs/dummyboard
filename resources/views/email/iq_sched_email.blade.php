<div style = "font-family: 'Times New Roman', Times, sans-serif;">
Hi {{ucfirst($content['name'])}}, 
<br><br>
Thank you for your time this {{$content['part_of_day']}}, it was a pleasure talking to you. Per our conversation, I have scheduled your Logi Analytics call with {{ucfirst($content['cc_name'])}} for {{$content['date']}} at {{$content['time']}},  {{!empty($content['timezone'])? ucfirst($content['timezone']) : " "}}. We will be calling you on your preferred number at {{!empty($content['phone_number'])? ucfirst($content['phone_number']) : " "}}.
<br><br>
Thank you and I look forward to your accepted invitation.
<br><br>
{{!empty($content['notes'])? ucfirst($content['notes']) : " "}}
<br>

<h4 style = "color:yellow-green">{{!empty($content['fullname'])? ucfirst($content['fullname']) : " "}}</h4>
Business Development Representative <br>

<span style = "font-style:12px;"><span style = "color:#8acc25">E</span> <a href = "" >{{!empty($content['from'])? $content['from'] : " "}}</a><br>
<span style = "color:#8acc25">O</span> (703) 752-9700  <br><br>

<span style = "color:#8acc25">LogiAnalytics.com</span></span>
</div>