<div style = "font-family: 'Times New Roman', Times, sans-serif;">
	Hi {{!empty($content['name']) ? ucfirst($content['name']) : " "}}, 
	<br><br>
	Thanks for your time today. Take a look at these 12 real-world data visualization examples to see how we serve customers.
	<br><br>
	Check out the <a href = "http://lookbook.logianalytics.com/?gnw=cascon_test&utm_medium=email&utm_source=sfdc&utm_campaign=cascon_test" >Interactive Visual Gallery</a> and browse by industry to see more.
	<br><br>
	{{!empty($content['notes'])? ucfirst($content['notes']) : " "}}

	<h4 style = "color:yellow-green">{{!empty($content['fullname'])? ucfirst($content['fullname']) : " "}}</h4>
	Business Development Representative <br>

	<span style = "font-style:12px;"><span style = "color:#8acc25">E</span> <a href = "" >{{!empty($content['from'])? $content['from'] : " "}}</a><br>
	<span style = "color:#8acc25">O</span> (703) 752-9700x7075 <br><br>

	<span style = "color:#8acc25">LogiAnalytics.com</span></span>
</div>
               
