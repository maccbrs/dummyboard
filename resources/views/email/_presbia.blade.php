<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Table Style</title>
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
</head>

<style>

@import url(http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

</style>

<body style = "background-color: #3e94ec;font-family: "Roboto", helvetica, arial, sans-serif;font-size: 16px;font-weight: 400;text-rendering: optimizeLegibility;">

<div class="table-title">

</div>
	<table class="table-fill" style = "display: block;margin: auto;max-width: 600px;padding:5px;width: 100%;   background: transparent;border-radius:3px;border-collapse: collapse;height: auto;margin: auto;max-width: 600px;padding:5px;width: 100%;animation: float 5s infinite;">
		<tbody class="table-hover">

			<tr  style =" border-top: 1px solid #C1C3D1;border-bottom-: 1px solid #C1C3D1;color:#666B85;font-size:16px;font-weight:normal;text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);">
				<th class="text-left" style="  border-top:none; border-top-left-radius:3px;  color:white;;background:#1b1e24;border-bottom:4px solid #9ea7af;border-right: 1px solid #343a45;font-size:23px;font-weight: 100;padding:24px;text-align:left;text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);vertical-align:middle;">Data Field</th>
			
				<th class="text-left" style="border-bottom:none;   border-top-right-radius:3px; border-right:none; color:#D5DDE5;;background:#1b1e24;border-bottom:4px solid #9ea7af;border-right: 1px solid #343a45;font-size:23px;font-weight: 100;padding:24px;text-align:left;text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);vertical-align:middle;">Value</th>
			</tr>

			@foreach($content as $key => $value)
				<tr  style =" border-top: 1px solid #C1C3D1;border-bottom-: 1px solid #C1C3D1;color:#1b1e24;font-size:16px;font-weight:normal;text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);">
					<td style = "  text-align: right;  background:#FFFFFF;padding:20px;text-align:left;vertical-align:middle;font-weight:300;font-size:18px;text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);border-right: 1px solid #C1C3D1;border-bottom: 1px solid #C1C3D1;border-left: 1px solid #C1C3D1;" class="text-left">{{$key}}</td>
					<td style = "  text-align: right;  background:#FFFFFF;padding:20px;text-align:left;vertical-align:middle;font-weight:300;font-size:18px;text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);border-right: 1px solid #C1C3D1;border-bottom: 1px solid #C1C3D1;" class="text-left">{{$value}}</td>
					
				</tr>
			@endforeach
		</tbody>
	</table>
 </body>   

