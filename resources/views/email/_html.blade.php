<!DOCTYPE html>
<html lang="en">

  <head>
    <title>HTML Email Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  </head>

  <body>

    <div style = "border-radius: 7px; width:60%; text-align:center; margin: auto;">

      <div style = "margin:30px; margin-bottom:5px; background-color:black; height: 32%; border-radius: 10px;">
        <div style = "padding:5px;  overflow-y: auto; _overflow: auto; margin: 0 0 1em;">
          <img style ="width:100%;" src="http://magellan-solutions.com/wp-content/uploads/2014/09/logo_on_black.png"  align="absbottom">
          <br><br>
          <img style ="width:100%;" src="http://www.magellan-solutions.com/wp-content/uploads/2014/09/slide1_a.jpg"  align="absbottom"><br><br>      
        </div>
      </div>
   
      <div style = "margin:30px; border-style: solid; margin-top:5px; margin-bottom:5px; border-radius: 10px; padding: 10px;">  
          <br>
          <div style = "width:100%; width: 100%; overflow-y: auto; _overflow: auto; margin: 0 0 1em;">
            <h3>Magellan Reporting Services</h3>
          </div>
          <br><br>
          <div style = "width:100%; overflow-y: auto; _overflow: auto; margin: 0 0 1em;">
            <table class="table table-bordered" style="border: 1px solid #ddd;width:788px;display: table;border-collapse: collapse !important; width:100%;">
                <tr >
                  @foreach($keys as $key)
                  <th style="border: 1px solid #ddd;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;padding: 5px;border-bottom: 3px solid #ddd; text-align:center;">{{$key}}</th>
                  @endforeach
                </tr>
                <tr>
                  @if(isset($value[0]))
                    @foreach($value[0] as $val)
                    <td style="border: 1px solid #ddd;">{{$val}}</td>
                    @endforeach
                  @endif
                </tr>                     
            </table>
          </div>
      </div>

      <div style = "margin:30px; border-style: solid; margin-top:5px; margin-bottom:5px;border-radius: 6px; ">         
        <div style ="background-color:black; border-radius: 2px;">
          <br>
          <span style = "color:white">Copyright Magellan Solutions 2016.</span>
          <br><br>
        </div>
      </div>

    </div>    

  </body>
</html>




