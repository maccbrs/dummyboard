
<!doctype html>
<html >
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
</head>

  <body id="YIELD_MJML" style="">
    <div class="mj-body">
      <div style="margin:0 auto;width:100%;">
        <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;" align="center">
          <tbody> 
            <tr>
              <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;">
                <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;" class="mj-column-per-100" aria-labelledby="mj-column-per-100">
                  <table width="100%">
                    <tbody>
                      <table style="width: 100%; border-collapse: collapse;">
                         @foreach($keys as $key)
                          @foreach($value as $val)
                          <tr>
                            <td width="35%" style="background: #2B547E; color: white; font-weight: bold; padding: 3px; border: 1px solid #ccc; text-align: center;">{{ucfirst(strtolower(str_replace('_', ' ', $key)))}}</th>
                            <td width="65%" style="border: 1px solid #ddd; text-align: center; padding: 3px; border: 3px solid #ccc; text-align: center;">{{$val[$key]}}</td> 
                          </tr>
                           @endforeach 
                          @endforeach
                        </table>
                        <br><br>
                    </tbody>
                  </table>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>



    

