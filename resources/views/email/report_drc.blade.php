@if(isset($board['lob']))
<h4 style="margin-left:30px;">EOD report</h4>
@endif


@if(isset($dispo))
	{!!$dispo!!}

	<br><br>
	<b>Summary of Customer Information. Note that the date and time indicated on the report is based on Philippine Time.</b>
	<br>
@else

	<b>Note that the date and time indicated on the report is based on Sydney Time.</b>
	<br>

@endif

               <!-- content -->
@if(isset($content)) 
	
	{!!$content!!} 

@endif
               
