@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                <span class="mb-custom-title">Vici to Board connector </span>
                <a href="{{route('vct.create')}}" class="btn btn-default pull-right">Add</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('flash::message')
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Vici plug</th>
                                <th>Templar socket</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($connectors as $connector)
                            <tr class="odd gradeX">
                                <td>{{$connector->vici_plug}}</td>
                                <td>{{$templar[$connector->templar_socket]}}</td>
                                <td>
                                    <a href="{{url('dashboard/vct/'.$connector->id.'/delete')}}" class="btn btn-default delete">Delete</a>
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
@endsection 