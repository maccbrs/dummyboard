@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default">
	        <div class="panel-heading">

			<h3>Report Generator</h3>

				<table class='table table-fixed'>

					<form method="post" action="{{route('reporter.blockage')}}" >
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	
						<thead>
							<tr>
								<th><label>Date From: </label><input type='text' name="from" class='form-control input-date date-from' placeholder='Click to select date and time' style='display: inline;'></th>
								<th><label>Date To: </label><input type='text' name="to" class='form-control input-date date-to' placeholder='Click to select date and time' style='display: inline;'></th>
							</tr>					
							<tr>
								<th>
									<label>Blockage</label>
									<select class='form-control select-camps' name="campaigns">
										
										@foreach($campaigns as $c)
										<option value="{{$c['campaigns']}}">{{$c['name']}}</option>
										@endforeach
								    </select>
							    </th>
								<th>
									<button class='btn btn-primary btn-submit'>Generate</button> 
								</th>
							</tr>
						</thead>
					</form>

				</table> 

	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message') 
	            <div class="dataTable_wrapper"> 
	            	{!!(isset($email_tbl)?$email_tbl:'')!!}
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('header-scripts')
	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script> 
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script>
    $('.input-date').datetimepicker({});
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection