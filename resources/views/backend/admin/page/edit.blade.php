 @extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

@section('header-scripts')

<!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('sbadmin/ckeditor/library/grideditor/dist/grideditor.css')}}" /> -->

@endsection

@section('content') 

<?php $include = 'templates.'.$page->template.'._form'; ?>

@include($include)

@endsection  

@section('footer-scripts')

  <script src="{{URL::asset('sbadmin/ckeditor2/ckeditor.js')}}"></script>
  <script src="{{URL::asset('sbadmin/ckeditor2/sample.js')}}"></script>

  <script type="text/javascript">
  
        CKEDITOR.replace( 'editor' );
        CKEDITOR.add
        CKEDITOR.replace( 'editor2' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor3' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor4' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor5' );
        CKEDITOR.add  

        CKEDITOR.on('instanceReady', function(e) {
  		// First time
  		  e.editor.document.getBody().setStyle('background-color', '#ccc');
  		// in case the user switches to source and back
  		  e.editor.on('contentDom', function() {
     		e.editor.document.getBody().setStyle('background-color', '#ccc');
    });
});    
  </script>        		
@endsection