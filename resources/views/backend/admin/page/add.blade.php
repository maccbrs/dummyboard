 @extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper"> 
    	<div class="m-t"></div>
        <div class="row">
        	<div class="col-md-8 col-md-offset-1">
				<div class="panel panel-default">
				   <div class="panel-heading">
				      Add new Page
				   </div>
				   <div class="panel-body">
		  				<div class="input-group">
							@if (count($errors) > 0)
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif	
		  				</div>				   	
				   	<form method="post" action="{{route('page.add.save')}}">
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="campaign_id" value="{{$board->campaign_id}}">
					    <input type="hidden" name="board_id" value="{{$board->id}}">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	

						<div class="form-group">
						   <label>Title</label> 
						   <input class="form-control" type="input" value="{{old('title')}}" name="title" required>
						</div>

						<div class="form-group">
                            <label>Select Template</label>
                            <select class="form-control" name="template">
								<option value="first">Leo (first)</option>
                                <option value="second">Virgo (second)</option>
                                <option value="third">Libra (third)</option>
                                <option value="eCapital">Aries(eCapital)</option>
                                <option value="5star">Sagittarius(5star)</option>
                                <option value="headhunter2">Capricorn (Head Hunter2)</option>
                                <option value="paperchase">Aquarius ( PaperChase )</option>
                                <option value="presbia">Pisces (Presbia)</option>
                                <option value="taurus">Taurus (Vici-Lead Based)</option>
                         <!--        <option value="abcd">ABCD</option> -->
                                <option value="scorpio">Scorpio (Vici-Lead Based)</option>
                                <option value="gemini">Male Gemini(Twin Template - Main)</option>
                                <option value="gemini_apprentice">Female Gemini (Twin Template - Apprentice)</option>
                                <option value="scorpio2">Scorpio2 (Einstein)</option>
                                <option value="customform">Custom Form</option>
                            </select>
                        </div>

						<button type="submit" class="btn btn-default">Save</button>
					</form>
				   </div>
				</div>
        	</div>
        </div>
    </div>
@endsection  