<?php $assets = URL::asset('/').'tokenfield/'; 

$opt = json_decode($board->options);
//pre($opt);
?>

@extends('backend.admin.dashboard.master')
@section('title', 'dashboard')
<?php 
$options = ['onsubmit' => 'Every Submit','daily' => 'Daily Reports'];


 ?>

@section('header-scripts')
    <!-- jQuery UI CSS -->
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <!-- Bootstrap styling for Typeahead -->
    <link href="{{$assets}}dist/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
    <!-- Tokenfield CSS -->
    <link href="{{$assets}}dist/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">
   
@endsection


@section('content')
<div id="page-wrapper">
	<div class="m-t"></div>
	<div class="row">
		<div class="col-md-8 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading clearfix"> 
				Email Settings 
				<a href="{{route('board.edit',$board->campaign_id)}}" target="_tab" class="btn btn-default pull-right clearfix" >Back to Board</a>
				</div> 
				<div class="panel-body">
						@include('flash::message')
						@if (count($errors) > 0)
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							Update failed!
						</div>						
						@foreach ($errors->all() as $error)
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{{$error}}  
						</div>
						@endforeach
						@endif 
						
					<form method="post" action="{{route('backend.admin.board.mail.save',$board->campaign_id)}}">
						<input type="hidden" name="_method" value="POST">
						<input type="hidden" name="campaign_id" value="{{$board->campaign_id}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label>Subject:</label>
							<input class="form-control" type="text"  value="{{(isset($opt->subject)?$opt->subject:'')}}" name="subject">
						</div>	

						<div class="form-group">
							<label>To: <small class="c-red">(accept multiple emails. please use comma (,) to separate not semicolon(;))</small></label>
							<input class="form-control" type="text" id="to" value="{{old('to',(isset($board->to)?emailer_parser($board->to):''))}}" name="to">
						</div>	

                        <div class="form-group">
                           <label>from <small class="c-red">(accept single email only.)</small></label>
                           <input class="form-control" type="text" value="{{old('from',(isset($board->from)?$board->from:''))}}" name="from">
                        </div>


                        <div class="form-group">
                           <label>Email Cc: <small class="c-red">(accept multiple emails. please use comma (,) to separate not semicolon(;))</small></label>
                           <input class="form-control" type="text" id="cc" value="{{old('cc',(isset($board->cc)?emailer_parser($board->cc):''))}}" name="cc">

                        </div> 

                        <div class="form-group">
                           <label>Email bcc: <small class="c-red">(accept multiple emails. please use comma (,) to separate not semicolon(;))</small></label>
                           <input class="form-control" type="text" id="bcc" value="{{old('bcc',(isset($board->bcc)?emailer_parser($board->bcc):''))}}" name="bcc">
                        </div> 

						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@section('footer-scripts')

	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
    <script type="text/javascript" src="{{$assets}}dist/bootstrap-tokenfield.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/scrollspy.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/affix.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/typeahead.bundle.min.js" charset="UTF-8"></script>
    

    <script type="text/javascript">


		$('#to').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

		$('#cc').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

		$('#bcc').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

    </script>


@endsection 