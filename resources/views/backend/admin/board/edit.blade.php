@extends('backend.admin.dashboard.master')
@section('title', 'dashboard')
<?php 
$options = ['onsubmit' => 'Every Submit','daily' => 'Daily Reports'];

//pre($board->toArray());
 ?>
@section('content')
<div id="page-wrapper">
	<div class="m-t"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix"> 
					Edit Board  <a href="{{url('preview/'.$board->campaign_id)}}" target="_new" class="btn btn-default pull-right clearfix" >Preview</a>
					
						<a href="{{route('backend.admin.board.mail',$board->campaign_id)}}" target="_tab" class="btn btn-default pull-right clearfix" >Email Options</a>
					
				</div> 
				<div class="panel-body">
						@include('flash::message')
						@if (count($errors) > 0)
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							Update failed!
						</div>						
						@foreach ($errors->all() as $error)
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{{$error}}  
						</div>
						@endforeach
						@endif 
					<form method="post" action="{{url('dashboard/boards/edit-save/'.$board->campaign_id)}}">
						<input type="hidden" name="_method" value="POST">
						<input type="hidden" name="campaign_id" value="{{$board->campaign_id}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label>Campaign Name</label>  
							<p>{{$board->campaign}}</p>
						</div>
						<div class="form-group">
							<label>LOB</label>
							<input class ="form-control" name="lob" value="{{$board->lob}}" >
							
						</div>	

						<div class="form-group">
                            <label>Set Primary</label>
                            <select class="form-control" name="primary_page">

                            	@if($board->primary_page == '')
                                <option value="">none</option>
                                @endif

                                @foreach($board->pages as $page)
                                	@if($page->id == $board->primary_page)
                                		<option value="{{$page->id}}">{{$page->title}}</option>
                                	@endif
                                @endforeach

                                @foreach($board->pages as $page)
                                	@if($page->id != $board->primary_page)
                                		<option value="{{$page->id}}">{{$page->title}}</option>
                                	@endif
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                           <label>Reporting Options</label>
                           <div id="radio-scroll" class="form-control">
                                @foreach($options as $optk => $optv)

                                <div class="checkbox">
                                   <label>
                                   <input type="checkbox" name="opt[]" <?=(in_array($optk, $reports)?'checked':'')?> value="{{$optk}}" >{{$optv}}
                                   </label>
                                </div>
                                @endforeach
                           </div> 
                        </div> 

                        <div class="form-group">
                           <label>Buttons page</label>
                           <div id="radio-scroll" class="form-control">
                                @foreach($board->pages as $page)

                                <div class="checkbox">
                                   <label>
                                   <input type="checkbox" name="btn[]" <?=(in_array($page->id, $btns)?'checked':'')?> value="{{$page->id}}" >{{$page->title}}
                                   </label>
                                </div>
                                @endforeach
                           </div> 
                        </div> 

						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
			</div>
		</div>
		<div class="m-t"></div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Pages <a href="{{url('dashboard/page/'.$board->campaign_id.'/add')}}" class="btn btn-default pull-right">Add new page</a>
				</div>
				<div class="panel-body">
					<div class="table-responsive table-bordered">
						<table class="table">
							<thead>
								<tr>
									<th>Title</th>
									<th>Template</th>
									<th>Link</th>
									<th>Date created</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								@foreach($board->pages as $page)
									@if($page->status == 1)
									<tr class="my-row">
										<td >{{$page->title}}</td>
										<td >{{$page->template}}</td>
										<td>[link:board={{$page->campaign_id}}:page={{$page->id}}:name=?]</td>
										<td >{{$page->created_at}}</td>
										<td >
											<a href="{{url('dashboard/page/edit/'.$page->id)}}" class="btn btn-default">Edit</a>
											<a href="{{url('dashboard/page/delete/'.$page->id)}}" class="btn btn-default delete">delete</a>
											
										</td>
									</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection