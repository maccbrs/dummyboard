@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

<?php $usertype = Auth::user()->user_type; ?>

@section('content')
    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>Board lists</h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message')
	            <div class="dataTable_wrapper" style="overflow-x:scroll;"> 
	                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                            <th>Campaign</th>
	                            <th>LOB</th>
	                            <th>Email to</th>
	                            <th>Email cc</th>
	                            <th>Email bcc</th>
	                            <th>#</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@foreach($boards as $board)
			                        <tr class="odd gradeX">
			                            <td>

			                            	{{(isset($board['campaign'])?$board['campaign']:'')}}

			                            	@if(!empty($board['connector']))

			                            		<i class="fa fa-link fa-fw"></i>

			                            	@endif


			                            </td>
			                            <td>{{$board['lob']}}</td>
			                            <td>{!!toccbcc_layout($board['to'])!!}</td>
			                            <td>{!!toccbcc_layout($board['cc'])!!}</td>
			                            <td>{!!toccbcc_layout($board['bcc'])!!}</td>
			                            <td>
			                            	<a href="{{url('dashboard/boards/edit/'.$board['campaign_id'])}}" class="btn btn-default">Edit</a>
			                            	<a href="{{url('preview/'.$board['campaign_id'])}}" target="_tab" class="btn btn-default clearfix" >Preview</a>
			                            	<a href="{{url('dashboard/logs/campaign/'.$board['campaign_id'])}}" target="_tab" class="btn btn-default clearfix" >Logs</a>

			                            	@if( $usertype == 'admin')
			                            	<a href="{{url('dashboard/boards/delete/'.$board['campaign_id'])}}" target="_tab" class="btn btn-default clearfix" >Delete</a>
			                            	@endif

			                            </td> 
			                        </tr>
	                        @endforeach
	                    </tbody>  
	                </table>
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection