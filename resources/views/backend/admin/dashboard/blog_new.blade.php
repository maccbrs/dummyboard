@extends('frontend.master')

@section('title', 'Page Title')


@section('content')
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Post New Blog</h4>
						  	</div>
						  	<div class="col-sm-3">
								<p>{{$user->name}}</p>
						  	</div>
					  	</div>
					  </div>
					  <div class="panel-body">

					  	<div class="row">
					  		<div class="col-md-8 col-md-offset-2">
					  				<div class="input-group">
										@if (count($errors) > 0)
										    <div class="alert alert-danger">
										        <ul>
										            @foreach ($errors->all() as $error)
										                <li>{{ $error }}</li>
										            @endforeach
										        </ul>
										    </div>
										@endif	
					  				</div>
								<form action="{{route('blog.put')}}" method="post" class="bs-example mb-custom-form">
								    <input type="hidden" name="_method" value="PUT">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Title</span> 
								   	<input type="text" name="title" value="{{old('title')}}" class="form-control"> 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon">Content</span> 
								   	<textarea name="content"> {{old('content')}} </textarea> 
								   </div>
								   <br> 
									<button type="submit" class="btn btn-default fl-r">Submit</button>							   
								</form>
					  		</div>
					  	</div>

					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection