<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{URL::asset('sbadmin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet"> 
    <!-- MetisMenu CSS -->
    <link href="{{URL::asset('sbadmin/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{URL::asset('sbadmin/dist/css/timeline.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{URL::asset('sbadmin/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{URL::asset('sbadmin/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
      <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('header-scripts')



</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Interface</a>

            </div>

            <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                       <a class="dropdown-toggle" data-toggle="dropdown" href="#" >
                       <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                       </a>
                       <ul class="dropdown-menu dropdown-user">
                          <li><a href="{{route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                          </li>
                       </ul>
                       <!-- /.dropdown-user -->
                    </li>
            </ul>            
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        @if(in_array(Auth::user()->user_type,array('admin','operation','trainer')))
                        
                        <li>
                            <a href="#"><i class="fa fa-th-large fa-fw"></i>Boards<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('board.index')}}">view all</a>
                                </li>                                
                                <li>
                                    <a href="{{route('board.add')}}">Add</a>
                                </li>                            
                            </ul>
                        </li>
                        @endif


                        @if(Auth::user()->user_type == 'admin')
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('users.index')}}">List</a>
                                </li>                                
                                <li>
                                    <a href="{{route('users.add')}}">Add</a>
                                </li>                          
                            </ul>
                        </li> 

                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> connectors<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('vct.index')}}">Board to Vici</a>
                                </li>                                
                                <li>
                                    <a href="{{route('connectors.blockages.index')}}">Blockages</a>
                                </li>                          
                            </ul>
                        </li> 

                        @endif

                        @if(Auth::user()->user_type != 'admin' && in_array(Auth::user()->user_type,array('admin','operation','trainer')) )
                        
                        <li>
                            <a href="#"><i class="fa fa-th-large fa-fw"></i>User Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('users.settings.reporting')}}">Reporting Options</a>
                                </li>                                                          
                            </ul>
                        </li>
                        @endif 

                        @if(in_array(Auth::user()->user_type,array('admin','operation','trainer')))
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i>Logs<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('logs.index')}}">All</a>
                                </li>                                                                                                                         
                            </ul>
                        </li>
                        @endif
                        @if(in_array(Auth::user()->user_type,array('admin','operation','reportanalyst')))
                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i>EOD Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('eod.index')}}">all</a>
                                    <a href="{{route('eod-generator.index')}}">Generator</a>
                                </li>                                                        
                            </ul>
                        </li>                          
                        @endif

                        @if(in_array(Auth::user()->user_type,array('admin','operation')))
                        <li>
                            <a href="{{route('publish.index')}}"><i class="fa fa-rocket fa-fw"></i>Publish</a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i>Emailer function<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('emailer.index')}}">view all</a>
                                    <a href="{{route('emailer.add')}}">add</a>
                                    <a href="{{route('emailer.email_blast')}}">Email Blast</a>
                                </li>                                                                                                                         
                            </ul>

                        </li>
                        @endif

                        @if(in_array(Auth::user()->user_type,array('admin','operation','reportanalyst')))

                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i>Tool<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('tool.index')}}">Change CRC</a>
                                </li>  
                                <li>
                                    <a href="{{route('tool.crc_1010')}}">Change CRC (Lead 5 1010)</a>
                                </li> 
                                @if(in_array(Auth::user()->id,array(4,64,1,15,60,38,103,104)))   
                                    <li>
                                        <a href="{{route('tool.change_data')}}">Change Submitted Data</a>
                                    </li>   
                                @endif                                                                                                                   
                            </ul>
                        </li>                         
                        @endif

                        @if(in_array(Auth::user()->user_type,array('admin','operation','reportanalyst')))
                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i>Reporter<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('reporter.index')}}">index</a>
                                </li>                                                                                                                         
                            </ul>
                        </li> 
                        @endif

                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i>Data Researcher<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @if(in_array(Auth::user()->user_type,array('admin','operation','reportanalyst')))
                                    <li>
                                        <a href="{{route('data_researcher.index')}}">Team Leader</a>
                                    </li>    
                                @endif
                                <li>
                                    <a href="{{route('data_researcher.edit')}}">Update Data</a>
                                </li>                                                                                                                      
                            </ul>
                        </li> 
						
                        @if(in_array(Auth::user()->user_type,array('admin','trainer','operation','reportanalyst')))
						 <li>
                            <a href="#"><i class="fa fa-upload fa-fw"></i>Upload File<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('upload.index')}}">pdf</a>
                                </li>                                                                                                                         
                            </ul>
                        </li>  
                        @endif

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        @yield('content')

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{URL::asset('sbadmin/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>


    <!-- Custom Theme JavaScript -->
    <script src="{{URL::asset('sbadmin/dist/js/sb-admin-2.js')}}"></script>
    @yield('footer-scripts')
</body>

</html>
