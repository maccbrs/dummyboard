@extends('backend.admin.dashboard.master')

@section('title', 'Select server')


@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading">
                      Select a server
                   </div>

                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div> 
                        @include('flash::message')                 
                        <form method="get" action="{{route('vct.create')}}"> 
                        <input type="hidden" name="_method" value="GET">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                        <div class="form-group">
                            <label>Select Server</label>
                            <select class="form-control" name="server">
                                    <option value="server 1">Server 1</option>
                                    <option value="server 2">Server 2</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Bound</label>
                            <select class="form-control" name="bound">
                                    <option value="in">IN</option>
                                    <option value="out">Out</option>
                            </select>
                        </div>


                        <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 