@extends('backend.admin.dashboard.master')

@section('title', 'connectors')

@section('header-scripts')

<style type="text/css">
    .mb-custom-title{
        min-height: 32px;
        display: inline-block;
    }
</style>

@endsection
@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                <span class="mb-custom-title">Vici to Board connector </span>
                <a href="{{URL::previous()}}" class="btn btn-default pull-right">Back</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('flash::message')
                <div class="dataTable_wrapper" style="overflow:scroll;"> 
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Vicidial Plug</th>
                                <th>Templar Socket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td>{{$vici_plug}}</td>
                                <td>{{$templar_socket}}</td>
                            </tr>
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
@endsection  