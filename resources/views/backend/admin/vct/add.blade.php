@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <form method="post" action="{{route('vct.save')}}"> 
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="server" value="{{$server}}">
        <input type="hidden" name="bound" value="{{$bound}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  

        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading">

                        <div class="row">
                            <div class="col-md-4">
                              Server: {{$server}} <br>
                              Bound: {{$bound}}    
                            </div>
                            <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">Title</span>
                                            <input type="text" class="form-control" name="name" >
                                        </div>
                            </div>                            
                            <div class="col-md-2">
                                <button class="btn btn-default pull-right">Submit</button>
                            </div>
                        </div>    

                   </div>

                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div> 
                        @include('flash::message')                 

                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Vicidial Plugs</a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                @foreach($vici as $v)
                                                    @if($v != '')
                                                        <div class="col-md-3">
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="checkbox" name="vici_plug[]" value="{{$v}}">{{$v}}
                                                                </label>
                                                            </div>                                                   
                                                        </div>
                                                    @endif
                                                @endforeach                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Templar Sockets</a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                @foreach($templar as $k => $t)
                                                    @if($t != '')
                                                        <div class="col-md-3">
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="checkbox" name="templar_socket[]" value="{{$k}}">{{$t}}
                                                                </label>
                                                            </div>                                                   
                                                        </div>
                                                    @endif
                                                @endforeach                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>                        
                   </div>
                </div>
            </div>
        </div>

        </form>
    </div>
@endsection 