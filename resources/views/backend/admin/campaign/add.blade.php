@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
    	<div class="m-t"></div>
        <div class="row">
        	<div class="col-md-8 col-md-offset-1">
				<div class="panel panel-default">
				   <div class="panel-heading">
				      Add new campaign
				   </div>
				   <div class="panel-body">
		  				<div class="input-group">
							@if (count($errors) > 0)
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif	
		  				</div>				   	
				   	<form method="post" action="{{route('campaign.add.create')}}">
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	

						<div class="form-group">
						   <label>Campaign Id</label>
						   <input class="form-control" type="input" value="{{old('campaign_id')}}" name="campaign_id">
						</div>

						<div class="form-group">
						   <label>LOB:</label>
						   <input class="form-control" type="input" value="{{old('lob')}}" name="lob">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				   </div>
				</div>
        	</div>
        </div>
    </div>
@endsection  