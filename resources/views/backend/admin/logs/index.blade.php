@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>Logs</h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message')
	            <div class="dataTable_wrapper"> 
	                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                            <th>Campaign Id</th>
	                            <th>User</th>
	                            <th>Activity</th>
	                            <th>Date</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@foreach($logs as $log)
			                        <tr class="odd gradeX">
			                            <td>{{$log['campaign_id']}}</td>
			                            <td>{{$log['users_id']}}</td>
			                            <td>{!!$log['contents']!!}</td>
			                            <td>{{$log['created_at']}}</td>
			                        </tr>
	                        @endforeach
	                    </tbody>  
	                </table>
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection