@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	    	@include('flash::message')
	    	
				<h3 style ="margin-bottom: 0px;">Change Disposition <br>
					
						<span style ="font-size:12px;"><I>*Call Date or Phone Number must be filled up. Select only a disposition to filter the data generated.</I></span>
		        	
		        </h3>
		    
	        	<br>
				<table class='table table-fixed'>
					<form method="post" action="{{route('tool.blockage')}}" >
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	
						<thead>
							<tr>
								<th><label>Call Date: </label><input type='text' name="calldate" class='form-control input-date date-from' placeholder='Click to select date and time' style='display: inline;'></th>	
								<th><label>Phone Number: </label><input type='text' name="phonenumber" class='form-control' placeholder='Phone Number' ></th>
								<th>
									<label>Disposition: </label>
									<select class="form-control" name = "disposition" >
										<option selected="selected"  disabled>Select a Disposition</option>
										@foreach($dispo as $ctr => $disp)
											<option ><?php echo $disp->status ?></option>
										@endforeach
									</select>
								</th>
								<th><button class='btn btn-primary btn-submit'>Generate</button></th>
							</tr>
						</thead>
					</form>
				</table> 
	        </div>
	        
	        <div class="panel-body">
	            <div class="dataTable_wrapper"> 
	            	{!!(isset($email_tbl)?$email_tbl:'')!!}
	            </div>
	        </div>

	        <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title" id="exampleModalLabel">Change CRC</h4>
			      		</div>
			      		<div class="modal-body">
					       	<form id="form-tool" method="post" action="{{route('tool.update')}}"> 
					       		<input type="hidden" name="_method" value="POST">
		                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
		                        <input type="hidden" class="form-control" id="uniqueID"  name = "uniqueid" >
		                        <input type="hidden" class="form-control" id="table"  name = "table" >
		                        <input type="hidden" class="form-control" id="server"  name = "server" >

					          <div class="form-group">
					            <label for="recipient-name" class="control-label">Lead ID:</label>
					            <input type="text" class="form-control" id="leadID" name = "leadID" disabled>
					          </div>

					          <div class="form-group">
					            <label for="message-text" class="control-label">Campaign ID:</label>
					            <input class="form-control" id="CampaignID" name = "campaignID" disabled></input>
					          </div>

					          	<div class="form-group">
					            	<label for="message-text" class="control-label" >Disposition :</label>
						            <select class="form-control" id = "status" name = "disposition">
									  <option selected="selected"  disabled>Select a Disposition</option>
										@foreach($dispo as $ctr => $disp)
											<option ><?php echo $disp->status ?></option>
										@endforeach
									</select>
								</div>
					        
					            <div class="modal-footer">
					                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					                <button type="submit" class="btn btn-primary">Save changes</button>
					            </div>

				        	</form>
				        </div>
				    </div>
				</div>


	    </div>
    </div>
@endsection 

@section('header-scripts')

	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">

@endsection

@section('footer-scripts')

	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script> 
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script>
    $('.input-date').datetimepicker({});
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });


    $('.edit-submit').on('click', function(e) {

    	var $this = $(this);
    	var status =  $( $this ).data("status");
    	var campaign =  $( $this ).data("campaign");
    	var lead =  $( $this ).data("lead");
    	var uniqueid =  $( $this ).data("uniqueid");
    	var table =  $( $this ).data("table");
    	var server =  $( $this ).data("server");

    	$('#table').val(table);
    	$('#CampaignID').val(campaign);
    	$('#leadID').val(lead);
    	$('#uniqueID').val(uniqueid);
    	$('#server').val(server);
    	$('#status').val(status);

    });

    </script>



@endsection