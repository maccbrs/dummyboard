@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	    	@include('flash::message')
	    	
				<h3 style ="margin-bottom: 0px;">Change Submitted Data <br>
					
						<span style ="font-size:12px;"><I>*All changes made using this page has a seperate Logs.</I></span>
		        	
		        </h3>
		    
	        	<br>
				<table class='table table-fixed'>
					<form method="post" action="{{route('tool.change_data')}}" >
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	
						<thead>
							<tr>
								<th><label>Date From: </label><input type='text' name="from" class='form-control input-date date-from' placeholder='Click to select date and time' style='display: inline;'></th>	
								<th><label>Date To: </label><input type='text' name="to" class='form-control input-date date-from' placeholder='Click to select date and time' style='display: inline;'></th>	
								<th>
									<label>Disposition: </label>
									<select class="form-control" name = "campaign" >
										<option selected="selected" disabled>Select a Campaign</option>
										<option value="5879861681241">Prescripto</option>
											<!-- @foreach($campaignlist as $ctr => $camp)
												<option value = '{{$camp->campaign_id}}' >{{$camp->campaign}}</option>
											@endforeach -->
									</select>
								</th>
								<th><button class='btn btn-primary btn-submit'>Generate</button></th>
							</tr>
						</thead>
					</form>
				</table> 
	        </div>


	        
	        <div class="panel-body">
	            <div class="dataTable_wrapper"> 
	            	@if(!empty($campaign_content))

	            	@foreach($campaign_content as $ctr2 => $cont)
	            	<div style="max-width:100%; overflow-x: scroll; white-space: nowrap;">
	            		<table class="table table-striped">
	            			<tr>
	            				<th>Action</th>

			            		@foreach($cont as $ctr22 => $cont2)
			            			@if($ctr22 != 'contents' && $ctr22 != 'request_status')
					            		<th scope="row">{{ucfirst($ctr22)}}</th>				          	
						          	@endif
			            		@endforeach
		            		</tr>
		            		<tr>
		            			
		            			<td scope="row"> <a class="btn btn-primary btn-submit edit-submit" data-toggle="modal" href="#myModal"  data-target="#edit-modal-{{$cont['id']}}">Edit </a></td>	
			            		
			            		@foreach($cont as $ctr22 => $cont2)
				            		@if($ctr22 != 'contents' && $ctr22 != 'request_status')
									    <td scope="row">{{ucfirst($cont2)}}</td>					          	
							        @endif
			            		@endforeach
		            		</tr>
	            		 </table>
	            		</div>

	            		<div id="edit-modal-{{$cont['id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
						        <div class="modal-content">

						            <div class="modal-header">
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        		<h4 class="modal-title" id="exampleModalLabel">Change Content</h4>
						      		</div>

						      		<div class="modal-body">
								       	<form id="form-tool" method="post" action="{{route('tool.update_content')}}"> 
								       		<input type="hidden" name="_method" value="POST">
					                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
					                        <input type="hidden" name="campaign_id" value="{{ $campaign_name }}">  
					                        <input type="hidden" name="to" value="{{ $to }}"> 
					                        <input type="hidden" name="from" value="{{ $from }}">  

					                     
					        		        <div class="panel-body">
									            <div class="dataTable_wrapper"> 

								            		@foreach($cont as $ctr22 => $cont2)

								            			@if($ctr22 == 'contents')

								            				<input type="hidden" name="prev_content" value="{{$cont2}}">  

								            			@endif

								            			@if($ctr22 == 'id')

								            				<input type="hidden" name="id" value="{{$cont2}}">  

								            			@endif

									            		@if($ctr22 != 'contents' &&  $ctr22 != 'id' && $ctr22 != 'campaign_id'&& $ctr22 != 'created_at')
									            			<div class = "row">
															    <div class="form-group">
															    	<div class="col-xs-6">
													            		<label for="recipient-name" class="control-label">{{ucfirst($ctr22)}} </label>
													            	</div>
													            	<div class="col-xs-6">
													            		<input type="text" class="form-control" value = "{{ucfirst($cont2)}}"  name = "content[{{$ctr22}}]" >
													         		</div>
													         	</div>	
													         </div>				          	
												        @endif
								            		@endforeach

									            </div>
									        </div>

								            <div class="modal-footer">
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								                <button type="submit" class="btn btn-primary">Save changes</button>
								            </div>

							        	</form>
							        </div>
							    </div>
							</div>
				    	</div>
	            	@endforeach
	            @endif
	            
	            </div>
	        </div>


   
@endsection 

@section('header-scripts')

	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">

@endsection

@section('footer-scripts')

	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script> 
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script>
    $('.input-date').datetimepicker({});
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });


    $('.edit-submit').on('click', function(e) {

    	var $this = $(this);
    	var status =  $( $this ).data("status");
    	var campaign =  $( $this ).data("campaign");
    	var lead =  $( $this ).data("lead");
    	var uniqueid =  $( $this ).data("uniqueid");
    	var table =  $( $this ).data("table");
    	var server =  $( $this ).data("server");

    	$('#table').val(table);
    	$('#CampaignID').val(campaign);
    	$('#leadID').val(lead);
    	$('#uniqueID').val(uniqueid);
    	$('#server').val(server);
    	$('#status').val(status);

    });

    </script>



@endsection