@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading">
                     Create new blockage
                   </div>
                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div>                  
                    <form method="post" action="{{route('connectors.blockages.save')}}">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                        <div class="form-group">
                           <label>name</label>
                           <input class="form-control" type="input" value="{{old('name')}}" name="name">
                        </div>

                        <div class="form-group">
                           <label>Reporting Options</label>
                           <div id="radio-scroll" class="form-control">

                                @foreach($boards as $board)
                                <div class="checkbox">
                                   <label>
                                   <input type="checkbox" name="campaigns[]" value="{{$board->campaign_id}}" >{{(empty($board->lob)?$board->campaign_id:$board->lob)}}
                                   </label>
                                </div>
                                @endforeach

                           </div> 
                        </div> 
                        
                        <button type="submit" class="btn btn-default">Create</button>
                        
                    </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
@endsection  