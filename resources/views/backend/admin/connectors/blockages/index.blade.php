@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')




@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                <span class="mb-custom-title">Blockage Connectors </span>
                <a href="{{route('connectors.blockages.create')}}" class="btn btn-default pull-right">Add</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('flash::message')
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Campaigns</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($campaigns as $c)
                            <tr class="odd gradeX">
                                <td>{{$c['name']}}</td>
                                <td>{{$c['campaigns']}}</td>
                            </tr>
                            @endforeach
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
@endsection  