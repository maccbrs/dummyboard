@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>EOD Reports - {{(isset($list[0]['date'])?$list[0]['date']:'')}}</h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message')
	            <div class="dataTable_wrapper"> 
	                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                        	<th>Campaign</th>
	                            <th>Subject</th>
	                            <th>Status</th>
	                            <th>#</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($lists as $list)
		                    	<tr>
		                    		<td>{{$list['campaign']}}</td>
		                    		<td>{{$list['subject']}}</td>
		                    		<td>{{($list['status']?'Sent':'No Data')}}</td>	                    			                    		
		                    		<td><a href="{{route('eod.show',$list['id'])}}" class="btn btn-info btn-circle"><i class="fa fa-folder"></i></a></td>
		                    	</tr>
	                    	@endforeach
	                    </tbody>  
	                </table>
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection