@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>EOD Report <a href="{{URL::previous()}}" class="btn btn-info btn-circle pull-right"><i class="fa fa-fast-backward"></i></a></h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message')
	        	<p>To: {{$data['to']}}</p>
	        	<p>From: {{$data['from']}}</p>
	        	<p>Subject: {{$data['subject']}}</p>
	        	<p>Cc: {{$data['cc']}}</p>
	        	<p>Bcc: {{$data['bcc']}}</p>

	        	<div class = "table-container">
	        	{!!$data['content']!!}
	        	</div>
	        </div>
	    </div>
    </div>

    <style>

		.table-container
			{
				width: 100%;
				height: 500px;
				overflow-y: auto;
				_overflow: auto;
				margin: 0 0 1em;
			}

	</style>

@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   

@endsection