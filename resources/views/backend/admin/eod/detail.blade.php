@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>EOD Reports</h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	
	            <div class="dataTable_wrapper"> 
	                <table class="table table-striped table-bordered table-hover" >
	                    <thead>
	                        <tr>
	                        	<th>Unique Id</th>
	                            <th>Call Date</th>
	                            <th>Status</th>
	                            <th>User</th>
	                            <th>Campaign Id</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($result as $list) 
	                    		<tr>
		                    		<td>{{$list->uniqueid}}</td>
		                    		<td>{{$list->call_date}}</td>
		                    		<td>{{$list->status}}</td>
		                    		<td>{{$list->user}}</td>
		                    		<td>{{$list->campaign_id}}</td>
	                    		</tr>
	                    	@endforeach

	                    </tbody> 
	                </table>

	            </div>
	        </div>
	    </div>
    </div>
@endsection 

	