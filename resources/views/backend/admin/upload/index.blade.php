@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default">
	        <div class="panel-heading">

			<h3>Upload File</h3>


<?php	
			if (isset($mode)) {
		
						if ($mode == 'upload')
					{
						?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>{{$msg}}</strong> 
					 </div>
						
						
						
					<?php
		
					}
					else if($mode == 'delete')
					{
						?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>{{$msg}}</strong> 
					</div>
						<?php
					}
			
						
	
	
	
} ?>

  
  
  

			
				<div class="panel-heading clearfix">
					 <a href="{{url('dashboard/upload/create')}}" class="btn btn-default pull-right">Add new pdf</a>
				</div>	
				
             <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                            <th>File Name</th>
	                            <th>Code</th>
	                            <th>Action</th>
	                        </tr>
	                    </thead>
<?php

$files = File::allFiles(public_path('pdf/') );
foreach ($files as $file)
{
	?>	
	 <tr class="odd gradeX">
		<td>
		<form method="post" action="{{route('upload.delete')}}" >
					    <input type="hidden" name="_method" value="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">	
						<input type="hidden" name="filename" value="<?php echo str_replace(public_path('pdf/') . chr(92) ,'',(string)$file) ;?>">	
			<?php			
				  echo '<a href="' . url() . '/pdf/' . basename($file) . '" target="_blank">' . basename($file) ."</a><br/>";		
			?>			
						
		</td>		
		
		<td >
		<textarea rows="4" cols="50"><?php echo '[pdf:name=' . basename($file)  . ']' ;?></textarea>
			
		</td>
	
		
		<td >
		
			<input type="submit" value ="Delete" class="btn btn-danger"/>
			</form>
		</td>

<?php		
		
}



?>	
				
	                    </tbody>  
	                </table>
				
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message') 
	            <div class="dataTable_wrapper"> 
	            	{!!(isset($email_tbl)?$email_tbl:'')!!}
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('header-scripts')
	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script> 
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script>
    $('.input-date').datetimepicker({});
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection