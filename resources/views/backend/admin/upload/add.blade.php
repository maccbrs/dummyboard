 @extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
     <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="panel panel-default clearfix">
            <div class="panel-heading">
                <span class="mb-custom-title">Upload PDF File</span>
            
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            				<div class="row">
								<div class="col-md-10 col-md-offset-1">
										  	   
								   <form enctype="multipart/form-data" action="{{route('upload.create.save')}}" method="POST">
								   <input type="file" name="pdf_file">
								   <input type="hidden" name="_token" value="{{ csrf_token() }}">
								   <input type="submit" class="pull-right btn btn-sm btn-primary" value="Save">
								   </form>
								</div>
							   
							</div>
            </div>
        </div>
    </div>
@endsection  



	
	
	



@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
@endsection 