@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default">
	        <div class="panel-heading">
	        	<h3>Team Leader Page</h3>
	        </div>

	        <div class="panel-body">
	        	@include('flash::message') 
				<table class='table table-fixed'>

					<tr>
						<th> Name </th>
						<th> Dispositions </th>
						<th> Action </th>
					</tr>

					@foreach($researchers as $researcher)
								
						<tr>
							<td>{{$researcher['name']}}</td>
							<td>{{!empty($researcher['dispositions']) ?$researcher['dispositions'] : "" }}</td>
							<td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#assign{{$researcher['id']}}">Assign</button></td>
						</tr>

					    <div id="assign{{$researcher['id']}}" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content">
						    		<form method="post" action="{{route('data_researcher.assignment')}}" >
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Disposition</h4>
						      </div>

						      <div class="modal-body">
						      
									<input type="hidden" name="_method" value="POST">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">	
									<input type="hidden" name="user_type" value="researcher">	

									<input type="hidden" name="id" value="{{$researcher['id']}}">	

									  <label><input type="checkbox" value="LQ" name = "dispo[]">Lead Qualified-</label>
									  <label><input type="checkbox" value="CB" name = "dispo[]">Callback</label>
									  <label><input type="checkbox" value="NI" name = "dispo[]">Not Interested</label>
									  <label><input type="checkbox" value="HU" name = "dispo[]">Hang Up</label>
									  <label><input type="checkbox" value="NA" name = "dispo[]">No Answer</label>
									  <label><input type="checkbox" value="AM" name = "dispo[]">Answering Machine</label>
									  <label><input type="checkbox" value="HUP" name = "dispo[]">Hang Up During Pitch</label>
									  <label><input type="checkbox" value="DMNA" name = "dispo[]">Decision Maker Not Available</label>
									  <label><input type="checkbox" value="WN" name = "dispo[]">Wrong Number</label>
									  <label><input type="checkbox" value="Busy" name = "dispo[]">Busy tone</label>
									  <label><input type="checkbox" value="DNC" name = "dispo[]">Do not call</label>
									  <label><input type="checkbox" value="DN" name = "dispo[]">Disconected No</label>
									  <label><input type="checkbox" value="FM" name = "dispo[]">Fax  Machine</label>
									  <label><input type="checkbox" value="LB" name = "dispo[]">Language Barrier</label>
									  <label><input type="checkbox" value="BC" name = "dispo[]">Bad Connection</label>
									  <label><input type="checkbox" value="EMAIL" name = "dispo[]">Email</label>
									  <label><input type="checkbox" value="FU" name = "dispo[]">For Update</label>
								
						    
						      </div>

						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        <button type="submit" class="btn btn-primary">Assign</button>
						      </div>
						          </form>
						    </div>

						  </div>
						</div>

					@endforeach
					
				</table>
	        </div>
	    </div>
    </div>


@endsection 

@section('header-scripts')
	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script> 
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script>
    $('.input-date').datetimepicker({});
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection