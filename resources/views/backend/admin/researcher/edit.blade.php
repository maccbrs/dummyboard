@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

@section('content')
	{{ Session::get('flash_notification.message') }}

	<script>


        function claim(lid, mid){
            var url = '{{ route('data_researcher.claim') }}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: url + '/' + lid,
                type: 'get',
                success: function(result){
                    alert(result.trim());
                    if(result.trim() == 'claimed'){
                        $('#assign' + mid).modal('hide');
                        $('#claimedModal').modal('show');
					}

                }
            });

        }


	</script>

	<div class="modal fade" id="claimedModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lead Status - CLAIMED</h4>
				</div>
				<div class="modal-body">
					<p>Someone is already working on this lead. Kindly get another one.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<div id="page-wrapper">
		<div class="m-t"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Data Researcher Page</h3>
			</div>


			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#pool" aria-controls="pool" role="tab" data-toggle="tab">Pool</a></li>
				<li role="presentation"><a href="#incomplete" aria-controls="incomplete" role="tab" data-toggle="tab">Incomplete</a></li>
				<li role="presentation"><a href="#complete" aria-controls="complete" role="tab" data-toggle="tab">Completed</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="pool">
					<div class="panel-body">
						@include('flash::message')
						<table class='table table-fixed'>
							<tr>
								<th> Lead ID </th>
								<th> Name </th>
								<th> Company </th>
								<th> Campaign </th>
								<th> User </th>
								<th> Session ID </th>
								<th> Action </th>

							</tr>

							@foreach($pool->getCollection()->all() as $value)

								<tr>
									<td>{{!empty($value['lead_id']) ?$value['lead_id'] : "" }}</td>
									<td>{{!empty($value['content2']->First_and_Last_Name) ? $value['content2']->First_and_Last_Name : "" }}</td>
									<td>{{!empty($value['content2']->Company_Name) ? $value['content2']->Company_Name : "" }}</td>
									<td>{{!empty($value['campaign_id']) ? $value['campaign_id'] : "" }}</td>
									<td>{{!empty($value['user']) ? $value['user'] : "" }}</td>
									<td>{{!empty($value['session_id']) ? $value['session_id'] : "" }}</td>

									<td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#assign{{$value['id']}}" onclick="claim({{ $value['lead_id'] .', '. $value['id']}})">Claim</button></td>
							
							</tr>

								<div id="assign{{$value['id']}}" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<div class="modal-content">
											<form method="post" action="{{route('data_researcher.update')}}" >.

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Update</h4>

												</div>

												<div class="modal-body">
													<div class = "row">

														<input type="hidden" name="_method" value="POST">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">

														<input type="hidden" name="id" value="{{$value['id']}}">
														<input type="hidden" name="updated" value="{{$user_id}}">

														@foreach((array)$value['content2'] as $key2 => $value2)
															@if(!empty($key2))
																<div class="form-group">
																	<div class="col-md-4">
																		<label for="email">{{$key2}}:</label>
																	</div>
																	<div class="col-md-8">
																		<input type="text" class="form-control" name = "content[{{$key2}}]" value = "{{!empty($value2) ? $value2 : "" }}" >
																	</div>
																</div>
															@endif

														@endforeach

													</div>
												</div>

												<br>

												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</form>
										</div>

									</div>
								</div>

							@endforeach


						</table>

						<div class = "text-center">

							{!! $pool->render() !!}

						</div>

					</div>
				</div>

				<div role="tabpanel" class="tab-pane" id="incomplete">
					<div class="panel-body">
						@include('flash::message')
						<table class='table table-fixed'>

							<tr>
								<th> Lead ID </th>
								<th> Name </th>
								<th> Company </th>
								<th> Campaign </th>
								<th> User </th>
								<th> Session ID </th>
								<th> Action </th>

							</tr>

							@foreach($incomplete as $value)


								<tr>
									<td>{{!empty($value['lead_id']) ?$value['lead_id'] : "" }}</td>
									<td>{{!empty($value['content2']->First_and_Last_Name) ? $value['content2']->First_and_Last_Name : "" }}</td>
									<td>{{!empty($value['content2']->Company_Name) ? $value['content2']->Company_Name : "" }}</td>
									<td>{{!empty($value['campaign_id']) ? $value['campaign_id'] : "" }}</td>
									<td>{{!empty($value['user']) ? $value['user'] : "" }}</td>
									<td>{{!empty($value['session_id']) ? $value['session_id'] : "" }}</td>

									<td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#assign{{$value['id']}}">Update</button></td>

								</tr>

								<div id="assign{{$value['id']}}" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<div class="modal-content">
											<form method="post" action="{{route('data_researcher.update')}}" >.

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Update</h4>
												</div>

												<div class="modal-body">
													<div class = "row">

														<input type="hidden" name="_method" value="POST">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">

														<input type="hidden" name="id" value="{{$value['id']}}">
														<input type="hidden" name="updated" value="{{$user_id}}">


														@foreach((array)$value['content2'] as $key2 => $value2)

															@if(!empty($key2))
																<div class="form-group">
																	<div class="col-md-4">
																		<label for="email">{{$key2}}:</label>
																	</div>
																	<div class="col-md-8">
																		<input type="text" class="form-control" name = "content[{{$key2}}]" value = "{{!empty($value2) ? $value2 : "" }}" >
																	</div>
																</div>
															@endif
														@endforeach


													</div>
												</div>

												<br>

												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</form>
										</div>

									</div>
								</div>

							@endforeach

						</table>

					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="complete">
					<div class="panel-body">
						@include('flash::message')
						<table class='table table-fixed'>

							<tr>
								<th> Lead ID </th>
								<th> Name </th>
								<th> Company </th>
								<th> Campaign </th>
								<th> User </th>
								<th> Session ID </th>
								<th> Action </th>

							</tr>


							@foreach($complete as $value)

								<tr>
									<td>{{!empty($value['lead_id']) ?$value['lead_id'] : "" }}</td>
									<td>{{!empty($value['content2']->First_and_Last_Name) ? $value['content2']->First_and_Last_Name : "" }}</td>
									<td>{{!empty($value['content2']->Company_Name) ? $value['content2']->Company_Name : "" }}</td>
									<td>{{!empty($value['campaign_id']) ? $value['campaign_id'] : "" }}</td>
									<td>{{!empty($value['user']) ? $value['user'] : "" }}</td>
									<td>{{!empty($value['session_id']) ? $value['session_id'] : "" }}</td>
									<td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#assign{{$value['id']}}" onclick="claim({{ $value['lead_id']}})">Update</button></td>
								</tr>

								<div id="assign{{$value['id']}}" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<div class="modal-content">
											<form method="post" action="{{route('data_researcher.update')}}" >.

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Update</h4>
												</div>

												<div class="modal-body">
													<div class = "row">

														<input type="hidden" name="_method" value="POST">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">

														<input type="hidden" name="id" value="{{$value['id']}}">
														<input type="hidden" name="updated" value="{{$user_id}}">

														@foreach((array)$value['content2'] as $key2 => $value2)
															@if(!empty($key2))
																<div class="form-group">
																	<div class="col-md-4">
																		<label for="email">{{$key2}}:</label>
																	</div>
																	<div class="col-md-8">
																		<input type="text" class="form-control" name = "content[{{$key2}}]" value = "{{!empty($value2) ? $value2 : "" }}" >
																	</div>
																</div>
															@endif
														@endforeach


													</div>
												</div>

												<br>

												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</form>
										</div>


									</div>
								</div>
							@endforeach
						</table>

						<div class = "text-center">

							{!! $complete->render() !!}

						</div>
					</div>
				</div>
@endsection


@section('header-scripts')
	<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
	<!-- DataTables JavaScript -->
	<script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>
	<script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>
	<script>
        $('.input-date').datetimepicker({});
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });


            $('.modal').on('hidden.bs.modal', function () {
                location.reload();
            })
        });

	</script>

@endsection