<?php $assets = URL::asset('/').'tokenfield/'; 

//pre($opt);
?>
@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')
<?php 
$options = ['onsubmit' => 'Every Submit','daily' => 'Daily Reports'];

?>

@section('header-scripts')
    <!-- jQuery UI CSS -->
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <!-- Bootstrap styling for Typeahead -->
    <link href="{{$assets}}dist/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
    <!-- Tokenfield CSS -->
    <link href="{{$assets}}dist/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">

    <style>
    /*	.append_here{
    		visibility: hidden;
    	}*/
    </style>
   
@endsection
@section('content')
	<meta name="_token" content="{{ csrf_token() }}" />   
	<div id="page-wrapper">
		<div class="m-t"></div>
		<div class="row">

			<div class="panel panel-default">
				<div class="panel-heading clearfix"> 
					Load Emails From Vici
				</div> 
				<div class="panel-body">
				<!-- 	<form method="post" action="{{route('emailer.email_blast_load')}}">
					 -->
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="POST">
						<div class="col-md-2">
							<h4>List ID</h4>
						</div>
						<div class="col-md-8">
							<input class="form-control list_id" type="text"   name="list_id">
						</div>
						<div class="col-md-2">
							<button type ="submit" class="btn btn-primary load_list">Load</button>
						</div>
							
					<!-- </form> -->
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading clearfix"> 
				Email Blast 
				
				</div> 
				<div class="panel-body">
					@include('flash::message')
					<form method="post" action="{{route('emailer.email_blast_send')}}">
						<input type="hidden" name="_method" value="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label>Subject:</label>
							<input class="form-control" type="text"  value="Logi Analytics Interactive Visual Gallery" name="subject">
						</div>	

						<div class="form-group">
							<label>To: <small class="c-red">(accept multiple emails. please use comma (,) to separate not semicolon(;))</small></label>
							<input class="form-control" type="text" id="bcc" value="danilo.hipol@magellan-solutions.com" name="to" required>
						</div>	

                        <div class="form-group">
                           <label>from <small class="c-red">(accept single email only.)</small></label>
                           <input class="form-control" type="text" value="reports@magellan-solutions.com" name="from" readonly>
                        </div>


                        <div class="form-group">
                           <label>Email Cc: <small class="c-red">(accept multiple emails. please use comma (,) to separate )</small></label>
                           <input class="form-control" type="text" id="cc" value="" name="cc" >

                        </div> 

                        <div class="form-group">
                           	<label>Email bcc: <small class="c-red">(accept multiple emails. please use comma (,) to separate )</small></label>
                        
                           	<section class = "append_here">
                           		<input class="form-control bcc" type="text"   >
                           	</section>
                           
                        </div> 
<!-- 
                        <div class="form-group">
                            <div class="panel panel-primary">
                 				<div class="panel-heading">
                    				Body- <small>Email Content</small>
                 				</div>
		                         <div class="panel-body">
		                            <textarea id="editor" class="form-control" rows="20" height="100px" name="body" required></textarea>
		                         </div>
		                      </div>
                			</div>  -->
						<button type="submit" class="btn btn-default">Send</button>
					</form>
				</div>
			</div>
	</div>
</div>


@endsection 

@section('footer-scripts')
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
    <script type="text/javascript" src="{{$assets}}dist/bootstrap-tokenfield.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/scrollspy.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/affix.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/typeahead.bundle.min.js" charset="UTF-8"></script>
    

    <script type="text/javascript">

    	$( ".load_list" ).click(function( event ) {
		//  alert($('.list_id').val());

		   var url = '{{ route("emailer.email_blast_load") }}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

            $.ajax({
                url: url,
                type: 'post',
                data: {'list_id':$('.list_id').val(), '_token': $('input[name=_token]').val()},
                success: function(result){
                   	result = result.toString();
                   	// console.log(result);
                   	alert("Clicking the send button means that you'll be responsible for this function. We suggest to check all the recipients first");
                   	$( ".append_here" ).empty();
					$( ".append_here" ).append( "<input class='form-control bcc' type='text' id='to' value='"+ result +"' name='bcc' >" );
					$( ".hide_this" ).hide();

                }
            });
		  
		});

		$('#to').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

		$('#cc').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

		$('#bcc').tokenfield({
		  autocomplete: {
		    delay: 100
		  },
		  showAutocompleteOnFocus: true
		});

    </script>


	  <script src="{{URL::asset('sbadmin/ckeditor2/ckeditor.js')}}"></script>
	  <script src="{{URL::asset('sbadmin/ckeditor2/sample.js')}}"></script>

	  <script type="text/javascript">
	  
	        CKEDITOR.replace( 'editor' );
	        CKEDITOR.add
	      
	        CKEDITOR.on('instanceReady', function(e) {
	  		// First time
	  		  e.editor.document.getBody().setStyle('background-color', '#ccc');
	  		// in case the user switches to source and back
	  		  e.editor.on('contentDom', function() {
	     		e.editor.document.getBody().setStyle('background-color', '#ccc');
	    });
	});    
	  </script>        		


@endsection