 @extends('backend.admin.dashboard.master')

@section('title', 'emailer')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default"> 
	        <div class="panel-heading">
	            <h5>Email List</h5>
	        </div>

	        <div class="panel-body">
	        	@include('flash::message')
	            <div class="dataTable_wrapper" > 
	                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                        	<th>Name</th>
	                            <th>from</th>
	                            <th>to</th>
	                            <th>cc</th>
	                            <th>bcc</th>
	                            <th>shortcode</th>
	                            <th>#</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($emails as $email)
			                        <tr class="odd gradeX">
			                        	<td>{{$email['name']}}</td>
			                            <td>{{emailer_parser($email['from'])}}</td>
			                            <td>{{emailer_parser($email['to'])}}</td>
			                            <td>{{emailer_parser($email['cc'])}}</td>
			                            <td>{{emailer_parser($email['bcc'])}}</td>
			                            <td>[emailer:{{$email['id']}}:name=?]</td>
			                            <td>
			                            	<a href="{{route('emailer.edit',['emailer_id' => $email['id']])}}" class="btn btn-outline btn-default">Edit</a>
			                            	<a href="{{route('emailer.delete',['emailer_id' => $email['id']])}}" class="delete btn btn-outline btn-default">delete</a>
			                            	<a href="{{route('emailer.send',['emailer_id' => $email['id']])}}" class="send btn btn-outline btn-default">preview</a>
			                            </td> 
			                        </tr>
			                @endforeach
	                    </tbody>  
	                </table>
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });

    $('.delete').on('click',function(){
	    if(!confirm("Are sure you want to delete the email?")){
	    	return false;
	    }
    });

    </script>
    <style>

    .dataTable_wrapper{
    	
    	overflow-x: auto;
    }

    </style>
@endsection