<?php $assets = URL::asset('/').'tokenfield/'; ?>
@extends('backend.admin.dashboard.master')

@section('title', 'emailer | add')

@section('header-scripts')
    <!-- jQuery UI CSS -->
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <!-- Bootstrap styling for Typeahead -->
    <link href="{{$assets}}dist/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
    <!-- Tokenfield CSS -->
    <link href="{{$assets}}dist/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1"> 
                <div class="panel panel-default">
                   <div class="panel-heading">
                      Add new emailer
                   </div>

                   <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif 
                            @include('flash::message')                  
                        <form method="post" action="{{route('emailer.create')}}" id="form-emailer"> 
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">   

                        <div class="form-group">
                           <label>Name</label>
                           <input required class="form-control" type="text" value="{{old('name')}}" name="name">
                        </div>

                        <div class="form-group">
                           <label>from</label>
                           <input required class="form-control" type="text" id="from" value="{{old('from')}}" name="from">
                        </div>

                        <div class="form-group"> 
                           <label>to</label>
                           <input required class="form-control" type="text" id="to" value="{{old('to')}}" name="to">
                        </div>

                        <div class="form-group">
                           <label>cc</label>
                           <textarea class="form-control" id="cc" name="cc" >{{old('cc')}}</textarea>
                        </div>
                        <div class="form-group">
                           <label>bcc</label>
                           <textarea class="form-control" id="bcc" name="bcc" >{{old('bcc')}}</textarea>
                        </div>

                        <div class="form-group">
                           <label>subject</label>
                           <input required class="form-control" type="text"  value="{{old('subject')}}" name="subject">
                        </div>

                        <div class="form-group">
                           <label>content</label>
                           <textarea class="form-control" id="summernote" name="content" rows="5" >{{old('content')}}</textarea>
                        </div>                        

                        <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="{{$assets}}dist/bootstrap-tokenfield.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/scrollspy.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/affix.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{$assets}}docs-assets/js/typeahead.bundle.min.js" charset="UTF-8"></script>
     <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
     <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.js"></script>
    <script type="text/javascript">
$('#from').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#to').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#cc').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#bcc').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});

    $('#summernote').summernote({
      height: 400,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null             // set maximum height of editor
    });
    $('#form-emailer').on('submit',function(){
      var markupStr = $('#summernote').summernote('code');
      var input = $("<input>")
              .attr("type", "hidden")
                     .attr("name", "content").val(markupStr);
      $('#form-emailer').append($(input));
    });

    </script> 
@endsection