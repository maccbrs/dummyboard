@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')



@section('content')

    <div id="page-wrapper">
    	<div class="m-t"></div>
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            <h5>Board lists{{Auth::user()->id}}</h5>
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        	@include('flash::message') 
	            <div class="dataTable_wrapper"> 
	                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                    <thead>
	                        <tr>
	                            <th>Campaign Id</th>
	                            <th>Updates</th>
	                            <th>#</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($boards as $board)
	                    
		                        <tr class="odd gradeX">
		                            <td>{{$board->lob}}</td>
		                            <td class="max-h-scroll"> {!! mb_view_contents($board->contents) !!}</td>
		                            <td>
		                            	<a href="{{url('dashboard/publish/'.$board->campaign_id.'/save')}}" class="btn btn-default">publish</a>
		                            	<a href="{{url('preview/'.$board->campaign_id)}}" target="_tab" class="btn btn-default clearfix" >Preview</a>
		                            </td>
		                        </tr>

	                        @endforeach
	                    </tbody>  
	                </table>
	            </div>
	        </div>
	    </div>
    </div>
@endsection 

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection