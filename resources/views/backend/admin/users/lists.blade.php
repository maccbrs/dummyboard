@extends('backend.admin.dashboard.master')

@section('title', 'users')


@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5>Users</h5>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('flash::message')
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr class="odd gradeX">
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    {!!view('backend.admin.users.resetPass-modal', ['id' => $user->id])!!}
                                    <a href="{{url('dashboard/user/edit/'.$user->id)}}" class="btn btn-default">Edit</a>
                                    <a href="{{url('dashboard/user/delete/'.$user->id)}}" class="btn btn-default delete">Delete</a>
                                    <a href="{{url('dashboard/logs/user/'.$user->id)}}" class="btn btn-default">logs</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection 

@section('footer-scripts')
    <link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });

        $('.delete').on('click',function(){

            if(!confirm("Are you sure you want to delete this user?")){
                return false;
            }
            
        });        
    });

    </script>
@endsection 