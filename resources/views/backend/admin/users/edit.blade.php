@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')

<?php 
$user_types = ['admin' => 'Admin','trainer' => 'Trainer','operation' => 'Operation' ,'report analyst' => 'reportanalyst' ];
$camps = ['test' => 'Test','test1' => 'Test 1','test2' => 'Test 2','test3' => 'Test 3','test4' => 'Test 4','test5' => 'Test 5','test6' => 'Test 6',];

$campaigns_raw = $user->campaigns->toArray();
$campaigns = [];
foreach ($campaigns_raw as $crv) {
  $campaigns[] = $crv['campaign_id'];
}

?>

@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading clearfix">
                      edit user <a href="{{url('dashboard/user/delete/'.$user->id)}}" class="btn btn-default delete pull-right">Delete</a>
                   </div>

                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div>                  
                        <form method="post" action="{{url('dashboard/user/edit/'.$user->id)}}"> 
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  



                        <div class="form-group">
                           <label>Name</label> 
                           <input class="form-control" type="input" value="{{old('name',$user->name)}}" name="name">
                        </div>

                        <div class="form-group">
                           <label>Email</label>
                           <input class="form-control" type="email" value="{{old('email',$user->email)}}" name="email">
                        </div>

                        <div class="form-group">
                            <label>User type</label>
                            <select class="form-control" name="user_type">

                                @foreach($user_types as $k => $v)

                                    @if($k == $user->user_type)
                                        <option value="{{$k}}">{{$v}}</option>
                                    @endif
                                @endforeach
                                @foreach($user_types as $k => $v)
                                    @if($k != $user->user_type)
                                        <option value="{{$k}}">{{$v}}</option>
                                    @endif
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                           <label>Assign Campaign</label>
                           <div id="radio-scroll" class="form-control">

                                @foreach($boards as $boardv)
                                <div class="checkbox">
                                   <label>
                                      <input type="checkbox" name="campaign[]" {{(in_array($boardv->campaign_id,$campaigns)?"checked":"")}} value="{{$boardv->campaign_id}}" >{{$boardv->lob}}
                                   </label>
                                </div>

                                @endforeach

                           </div> 

                        </div>


                        <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
<script type="text/javascript">
$('.delete').on('click',function(){

    if(!confirm("Are you sure you want to delete this user?")){
        return false;
    }
    
});
</script>
@endsection