@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading">
                      Add new user
                   </div>

                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div>                  
                        <form method="post" action="{{route('users.create')}}"> 
                          <input type="hidden" name="_method" value="POST">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">  



                          <div class="form-group">
                             <label>Name</label>
                             <input class="form-control" type="input" value="{{old('name')}}" name="name">
                          </div>

                          <div class="form-group">
                             <label>Email</label>
                             <input class="form-control" type="email" value="{{old('email')}}" name="email">
                          </div>

                          <div class="form-group">
                              <label>User type</label>
                              <select class="form-control" name="user_type">
                                  <option value="admin">admin</option>
                                  <option value="trainer">trainer</option>
                                  <option value="operation">operation</option>
                                  <option value="reportanalyst">report analyst</option>
                                  <option value="user">user</option>
                              </select>
                          </div>

                          <div class="form-group">
                             <label>Password</label>
                             <input class="form-control" type="password" value="{{old('password')}}" name="password">
                          </div>

                          <div class="form-group">
                             <label>Repeat Password</label>
                             <input class="form-control" type="password" value="" name="repeat_password">
                          </div>

                          <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 