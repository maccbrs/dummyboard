@extends('backend.admin.dashboard.master')

@section('title', 'dashboard')
<?php 

//get all campaign assigned to?

 ?>

@section('content')

    <div id="page-wrapper">
        <div class="m-t"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                   <div class="panel-heading">
                      Email Reporting Options
                   </div>

                   <div class="panel-body">
                        <div class="input-group">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger"> 
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                        </div>                  
                        <form method="post" action="{{route('users.settings.reporting.post')}}"> 
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                        <div class="form-group">
                           <label>Reporting Options</label>
                           <div id="radio-scroll" class="form-control">

                                @foreach($options as $optk => $optv) 

                                    <div class="form-group">
                                       <label>{{$board_name[$optk]}}</label>

                                      @if(isset($sets[$optk]->reports))
                                        @foreach($sets[$optk]->reports as $v)
                                           <label class="checkbox-inline">
                                            <input type="checkbox" @if($optv) {{(in_array($v,$optv)?"checked":"")}} @endif name="opt[{{$optk}}][]" value="{{$v}}">{{$v}}
                                           </label>
                                        @endforeach
                                      @endif

                                    </div>

                                @endforeach

                           </div> 
                        </div> 



                        <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection 