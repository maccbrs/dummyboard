<!-- Modal -->
<a class="btn btn-default" data-toggle="modal" data-target=".resetpass{{$id}}">Reset Password</a>
<div class="modal fade resetpass{{$id}}" id="resetpass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reset Password</h5>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('users.resetPassword') }}">
          <div class="row">
            <div class="col-md-12">
              {{csrf_field()}}
              <input type="hidden" name="id" value="{{$id}}">
              <input type="text" name="password" class="form-control" required>
            </div>
          </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
         </form>
      </div>
    </div>
  </div>
</div>