@extends('frontend.master')

@section('title', 'Tabular Board')


@section('content')

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
           
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
	  
	    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

	  	<script>
	  $(document).ready(function() {
    	$('#example').DataTable(
        {
				
			dom: 'lBfrtip',
	    	lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
	        pageLength: 25, 
	        responsive: true,
	        autoWidth: false,
	        scrollX: true,
	        scrollY: '600px',
	        scrollCollapse: true,
	        buttons: [
            'excelHtml5'
         
       		 ]

	    	});
	} );
	  </script>

	  <style type="text/css">
	  p.uppercase {
					text-transform: uppercase;
					font-weight: bold;
				  }
	  </style>


<body style="background-color: #383838">

<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-12 ">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <!--<div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Register</h4>					  		
						  	</div>
						  	<div class="col-sm-3 txt-a-r">
						  				<a class="btn btn-default" href="{{route('home')}}">Home</a>
										<a class="btn btn-default" href="{{route('login')}}">Login</a> 
						  	</div>
					  	</div>
					  </div> -->
					  <div class="panel-body">

					  	<div class="row">
									<div class="col-md-12">
									<table class="display table-striped" id="example">
									
									@if($campaign == "59b96bfe41291")								
									  <thead class="thead-inverse">
										<tr>
										  <th>#</th>
										  <th>First Name</th>
										  <th>Last Name</th>
										  <th>Email Address</th>
										  <th>Country</th>
										  <th>Issue Description</th>
										  <th>Serial Number</th>
										  <th>Type of Device</th> 
										  <th>Notes</th>
										  <th>Status</th>
										  <th>Agent</th>

										  
										  <th>Campaign</th>
										  <th>Date</th>
										</tr>
									  </thead>
									  <tbody>
									  
									   @foreach($campaigndatas as $campaigndata)
										<tr>
										  <th scope="row">{{$campaigndata->id}}</th>
										  <td>
										  	{{!empty($help->fetchfname($campaigndata->contents))?$help->fetchfname($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchlname($campaigndata->contents))?$help->fetchlname($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchemailforVr($campaigndata->contents))?$help->fetchemailforVr($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchcountry2($campaigndata->contents))?$help->fetchcountry2($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchidesc($campaigndata->contents))?$help->fetchidesc($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  {{!empty($help->fetchsn($campaigndata->contents))?$help->fetchsn($campaigndata->contents) : 'N/A'}} 
										  </td> 
										  <td>
										  {{!empty($help->fetchdevicetype($campaigndata->contents))?$help->fetchdevicetype($campaigndata->contents) : 'N/A'}} 
										  </td>
										  <td>
										  	{{!empty($help->fetchnotes($campaigndata->contents))?$help->fetchnotes($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchstatus($campaigndata->contents))?$help->fetchstatus($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchagent($campaigndata->contents))?$help->fetchagent($campaigndata->contents) : 'N/A'}}
										  </td>										  
										  <td>{{$campaigndata->lob}}</td>
										  <td>{{$campaigndata->created_at}}</td>				
										</tr>
										@endforeach
									  </tbody>
									  @endif
									 

									  @if($campaign == "5879861681241")	

									  <thead class="thead-inverse">
										<tr>
										  <th>DID Number</th>
										  <th>Date(MM/DD/YYY)</th>
										  <th>First and Last Name</th>
										  <!-- <th>Phone Number</th> -->
										  <th>Shipping Address</th>
										  <th>City</th>
										  <th>State</th>
										  <th>Zipcode</th>
										  <th>Country</th>
										  <th>Email Address</th>
										  <th>Ship Method</th>
										  <th>Quantity/Product</th>
										  <th>Price</th>
										  <th>Billing Address</th>
										  <th>Representative's Name</th>
										</tr>
									  </thead>
									  <tbody>
									  
									   @foreach($campaigndatas as $campaigndata)					
										<tr>
										  <td>
										  	{{!empty($help->fetchdid($campaigndata->contents))?$help->fetchdid($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchdate($campaigndata->contents))?$help->fetchdate($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchfal($campaigndata->contents))?$help->fetchfal($campaigndata->contents) : 'N/A'}}
										  </td>
										  <!-- <td>
										  	{{!empty($help->fetchpnumber($campaigndata->contents))?$help->fetchpnumber($campaigndata->contents) : 'N/A'}}
										  </td> -->
										  <td>
										  	{{!empty($help->fetchsaddress($campaigndata->contents))?$help->fetchsaddress($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchcity($campaigndata->contents))?$help->fetchcity($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchstate($campaigndata->contents))?$help->fetchstate($campaigndata->contents) : 'N/A'}}
										  </td>
										  <td>
										  	{{!empty($help->fetchzip($campaigndata->contents))?$help->fetchzip($campaigndata->contents) : 'N/A'}}
										  </td>		
										  <td>
										  	{{!empty($help->fetchcountry($campaigndata->contents))?$help->fetchcountry($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchemail($campaigndata->contents))?$help->fetchemail($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchsmethod($campaigndata->contents))?$help->fetchsmethod($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchqp($campaigndata->contents))?$help->fetchqp($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchprice($campaigndata->contents))?$help->fetchprice($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchbaddress($campaigndata->contents))?$help->fetchbaddress($campaigndata->contents) : 'N/A'}}
										  </td>	
										  <td>
										  	{{!empty($help->fetchrname($campaigndata->contents))?$help->fetchrname($campaigndata->contents) : 'N/A'}}
										  </td>									  			
										</tr>
										@endforeach
									  </tbody>
									  @endif

									  @if($campaign == '5b58dc9911d9e')

									   <thead class="thead-inverse">
										<tr>
										 <th>Start Time</th>
										 <th>Form to be Submitted</th>
										 <th>Agent Name</th>
										 <th>Video ID</th>
										 <th>Issue Type</th>
										 <th>Issue Specific</th>
										 <th>Date</th>
										</tr>
									  </thead>

									  <tbody>
									  	@foreach($campaigndatas as $campaigndata)
									  	<tr>
									  		<td>{{!empty($help->fetchstarttime($campaigndata->contents))?$help->fetchstarttime($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{!empty($help->fetchform($campaigndata->contents))?$help->fetchform($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{!empty($help->fetchagentname($campaigndata->contents))?$help->fetchagentname($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{!empty($help->fetchvideo($campaigndata->contents))?$help->fetchvideo($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{!empty($help->fetchissuetype($campaigndata->contents))?$help->fetchissuetype($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{!empty($help->fetchissuespecifics($campaigndata->contents))?$help->fetchissuespecifics($campaigndata->contents) : 'N/A'}}</td>
									  		<td>{{$campaigndata->created_at}}</td>
									  	</tr>
									  	@endforeach
									  </tbody>

									  @endif

									  @if(!in_array($campaign, $custom))
									  <thead class="thead-inverse">
										<tr>
										  <th>#</th>
										  <th>Details</th>
										  <th>Campaign</th>
										  <th>Date</th>								  									  
										</tr>
									  </thead>

									  <tbody>
									   @foreach($campaigndatas as $campaigndata)
										<tr>
										   <th scope="row">{{$campaigndata->id}}</th>
										  <td >
										  
										  <?php  //{{$CampaignDatalist->contents}}
										  $pieces = explode(",", $campaigndata->contents); 
										  $pieces1 = preg_replace('/"([^"]+)"\s*:\s*/', '<b>$1</b>: ', $pieces);
										  $sym = array('"', '{', '}');
										  $pieces2 = str_replace($sym, ' ', $pieces1);
										  foreach($pieces2 as $var1){
											  echo ($var1 . "<br/>");									  											  
										  }
										  
										  //echo $pieces[2];//

										  ?>
										  </td>
										  <td>{{$campaigndata->lob}}</td>
										  <td>{{$campaigndata->created_at}}</td>											 
										</tr>
										@endforeach
									  </tbody>
									  @endif
									</table>
									</div>
					  	</div>
					  </div>
					</div>
			</div>
		</div>
	</div>
</div>

</body>
@endsection