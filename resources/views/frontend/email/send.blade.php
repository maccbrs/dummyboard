
@extends('frontend.email.master')

@section('title', 'dashboard')

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{URL::asset('sbadmin/ckeditor/library/grideditor/dist/grideditor.css')}}" /> 
@endsection

@section('content') 

<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
  <div class="panel panel-primary">

     <div class="panel-heading">
 			@include('flash::message')
			<ul>
				<li>From: {{emailer_parser($emails->from)}}</li>
				<li>To: {{emailer_parser($emails->to)}}</li>
				<li>Cc: {{emailer_parser($emails->cc)}}</li>
				<li>Bcc: {{emailer_parser($emails->bcc)}}</li>
				<li>Subject: {{$emails->subject}}</li>
			</ul>    
     </div>

     <div class="panel-body">

		<form action="{{route('emailer.sent',['emailer_id' => $emails->id])}}" method="post" class="bs-example mb-custom-form">
		    <input type="hidden" name="_method" value="POST">
		    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <input type="hidden" name="referer" value="{{(URL::previous() != ''?URL::previous(): 'none')}}">


        <textarea id="editor" class="form-control" rows="20" height="100px" name="content">{!!old('content',$emails->content)!!}</textarea>

			<br> 
			<button type="submit" class="btn btn-default fl-r">Send</button>	
					    
		  </div>
						   
		</form>


     </div>
  </div>
</div>
</div>
</div>



@endsection  

@section('footer-scripts')
        <script src="{{URL::asset('sbadmin/ckeditor2/ckeditor.js')}}"></script>
        <script src="{{URL::asset('sbadmin/ckeditor2/sample.js')}}"></script>

        <script type="text/javascript">
              CKEDITOR.replace( 'editor' );
              CKEDITOR.add
              CKEDITOR.replace( 'editor2' );
              CKEDITOR.add  
              CKEDITOR.replace( 'editor3' );
              CKEDITOR.add  
              CKEDITOR.replace( 'editor4' );
              CKEDITOR.add  
              CKEDITOR.replace( 'editor5' );
              CKEDITOR.add  

              CKEDITOR.on('instanceReady', function(e) {
		    		// First time
		    		  e.editor.document.getBody().setStyle('background-color', '#ccc');
		    		// in case the user switches to source and back
		    		  e.editor.on('contentDom', function() {
		       		e.editor.document.getBody().setStyle('background-color', '#ccc');
		      });
});    
        </script>        		
@endsection