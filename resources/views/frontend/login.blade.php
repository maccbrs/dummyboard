@extends('frontend.master')

@section('title', 'Login')

@section('content')
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Login</h4>
						  	</div>
					  	</div>
					  </div>
					  <div class="panel-body">

					  	<div class="row">
					  		<div class="col-md-8 col-md-offset-2">
					  				<div class="input-group">
										@if (count($errors) > 0)
										    <div class="alert alert-danger">
										        <ul>
										            @foreach ($errors->all() as $error)
										                <li>{{ $error }}</li>
										            @endforeach
										        </ul>
										    </div>
										@endif	
					  				</div>

								<form action="{{route('login.post')}}" method="post" class="bs-example mb-custom-form">
								    <input type="hidden" name="_method" value="POST">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
								    @if (session('alert'))
    								<div class="alert alert-danger">
       								{{ session('alert') }}
    								</div>
									@endif
								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Email</span> 
								   	<input type="text" name="email" class="form-control" value="{{old('email')}}" > 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon">Password</span> 
								   	<input type="password" name="password" class="form-control" > 
								   </div>
								   <br> 
									<button type="submit" class="btn btn-default fl-r">Submit</button>							   
								</form>

					

					  		</div>
					  	</div>

					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection