@extends('frontend.master')

@section('title', 'Page Title')


@section('content')
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>The Blogsite</h4>
						  	</div>
						  	<div class="col-sm-3 txt-a-r">
										<a class="btn btn-default">Login</a> 
										<a class="btn btn-default">Register</a> 
						  	</div>
					  	</div>
					  </div>
					  <div class="panel-body">
					    <p>Where everyone matter</p>
					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection

<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Register</h4>
						  	</div>
						  	<div class="col-sm-3 txt-a-r">
										<a class="btn btn-default">Login</a> 
						  	</div>
					  	</div>
					  </div>
					  <div class="panel-body">

					  	<div class="row">
					  		<div class="col-md-8 col-md-offset-2">
								<form class="bs-example mb-custom-form">

								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Username</span> 
								   	<input type="text" class="form-control" placeholder="Username" > 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Email</span> 
								   	<input type="text" class="form-control" placeholder="email" > 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Password</span> 
								   	<input type="password" class="form-control" placeholder="password" > 
								   </div>
								   <br> 
									<button type="submit" class="btn btn-default fl-r">Submit</button>							   
								</form>
					  		</div>
					  	</div>

					  </div>
					</div>
			</div>
		</div>
	</div>
</div>