@extends('frontend.master')

@section('title', 'Page Title')


@section('content')
<body style="background-color:#808080"></body>
<div class="navbar navbar-default navbar-fixed-top">
	<div class="row">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('dashboard')}}">Interface</a>
        </div>
        <center>
            <div class="navbar-collapse collapse" id="navbar-main">
                <ul class="nav navbar-nav" role="tablist">
                    <li class="active"><a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="true">Dummy Boards</a>
                    </li>
                    <li><a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Tabular</a>
                    </li>
                </ul>


                <form action="{{route('login.post')}}" method="post" class="navbar-form navbar-right" role="search">
                	<input type="hidden" name="_method" value="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">   
                    
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{old('email')}}" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                    

<!--                     @if($auth = Auth::user())
                    <ul class="nav navbar-right">
                    <a href="{{route('logout')}}"><button class="btn btn-danger">Logout</button></a>
            		</ul>
            		@endif -->
                    
                </form>
            </div>
        </center>
    </div>
</div>
</div>

<br>

<div class="tab-content">
<div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Dummy Boards 
						  		
						  		</h4> 
						  	</div>
						  	<!-- <div class="col-sm-3">
						  		<a href="{{url('login')}}" class="btn btn-default pull-right">login</a>
						  	</div> -->
					  	</div>
					  </div>
					  <div class="panel-body">
				            <div class="dataTable_wrapper"> 
				                <table class="table table-striped table-bordered table-hover" id="boards">
				                    <thead>
				                        <tr>
				                            <th>Campaign Name</th>
				                            <th>Campaign Id</th>
				                            <th>#</th>
				                        </tr>
				                    </thead>
				                    <tbody>

				                    	@foreach($boards as $board)

				                        <tr class="odd gradeX">
				                            <td style="width: 500px">{{$board->lob}}</td>
				                            <td style="width: 145px">{{$board->campaign_id}}</td>
				                            <td><a href="{{url('live/'.$board->campaign_id.'?lead_id=preview')}}" class="btn btn-default" target="_target">View</a></td>
				                        </tr>

				                        @endforeach 

				                    </tbody>  
				                </table>
				            </div>
					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="tab-pane" id="two" role="tabpanel" aria-expanded="false">
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Tabular Boards</h4> 
						  	</div>
						  	<!-- <div class="col-sm-3">
						  		<a href="{{url('login')}}" class="btn btn-default pull-right">login</a>
						  	</div> -->
					  	</div>
					  </div>
					  <div class="panel-body">
				            <div class="dataTable_wrapper">	
				          
				                <table class="table table-striped table-bordered table-hover" id="tabular">
				                    <thead>
				                        <tr>
				                            <th>Campaign Name</th>
				                            <th>Campaign Id</th>
				                            <th>#</th>
				                        </tr>
				                    </thead>
				                    <tbody>

				                    	@foreach($boards as $board)

				                        <tr class="odd gradeX">
				                            <td style="width: 500px">{{$board->lob}}</td>
				                            <td style="width: 145px">{{$board->campaign_id}}</td>
				                            <td><a href="{{url('tabularboard?id='.$board->campaign_id)}}" class="btn btn-default" target="_target">View</a></td>
				                        </tr>

				                        @endforeach 

				                    </tbody>  
				                </table>
				               
				            </div>
					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@endsection

@section('footer-scripts')
	<link href="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- DataTables JavaScript -->
    <script src="{{URL::asset('sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>   
    <script>
    $(document).ready(function() {
        $('#boards').DataTable({
                responsive: true
        });

        $('#tabular').DataTable({
                responsive: true
        });
    });
    </script>
@endsection
