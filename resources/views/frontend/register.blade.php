@extends('frontend.master')

@section('title', 'Register')


@section('content')
<div id="main-container">
	<div class="container">
		<div class="row first-row">
			<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
					  <!-- Default panel contents -->
					  <div class="panel-heading">
					  	<div class="row">
						  	<div class="col-sm-9">
						  		<h4>Register</h4>					  		
						  	</div>
						  	<div class="col-sm-3 txt-a-r">
						  				<a class="btn btn-default" href="{{route('home')}}">Home</a>
										<a class="btn btn-default" href="{{route('login')}}">Login</a> 
						  	</div>
					  	</div>
					  </div>
					  <div class="panel-body">

					  	<div class="row">
					  		<div class="col-md-8 col-md-offset-2">
					  				<div class="input-group">
										@if (count($errors) > 0)
										    <div class="alert alert-danger">
										        <ul>
										            @foreach ($errors->all() as $error)
										                <li>{{ $error }}</li>
										            @endforeach
										        </ul>
										    </div>
										@endif	
					  				</div>
								<form action="{{route('register.post')}}" method="post" class="bs-example mb-custom-form">
								    <input type="hidden" name="_method" value="POST">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
								   <div class="input-group"> 
								   	<span class="input-group-addon" id="basic-addon1">Name</span> 
								   	<input type="text" name="name" value="{{old('name')}}" class="form-control"> 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon">Email</span> 
								   	<input type="text" class="form-control" name="email" value="{{old('email')}}" > 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon">Password</span> 
								   	<input type="password" name="password" value="" class="form-control" > 
								   </div>
								   <br> 
								   <div class="input-group"> 
								   	<span class="input-group-addon">Password again</span> 
								   	<input type="password" class="form-control" name="password_confirmation"> 
								   </div>								   
								   
								   <br> 
									<button type="submit" class="btn btn-default fl-r">Submit</button>							   
								</form>
					  		</div>
					  	</div>

					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection